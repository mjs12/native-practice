import {Draw, UserType} from "./outlet.model";

export const adminDraw = [
  {value: Draw.DRAW1, label: '11-am'},
  {value: Draw.DRAW2, label: '4-pm'},
  {value: Draw.DRAW3, label: '9-pm'}
];


export class ReportSummary{
  constructor(
    public totalSales: number,
    public totalCancelledSales: number,
    public totalHits: number,
    public gross: number
  ){ }
}

export class FormUser {
  constructor(
    public lastname: string,
    public firstname: string,
    public userType: UserType,
    public branchId: string,
    public outletId: string,
    public username?: string
  ){}
}

export class FormSummary {
  constructor(
    public from: any,
    public to: any,
    public draw: Draw,
    public branch: string,
    public outlet: string
  ){}
}
