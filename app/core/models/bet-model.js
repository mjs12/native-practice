"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var outlet_model_1 = require("./outlet.model");
exports.codes = [
    { value: outlet_model_1.Draw.DRAW1, label: '11-am' },
    { value: outlet_model_1.Draw.DRAW2, label: '4-pm' },
    { value: outlet_model_1.Draw.DRAW3, label: '9-pm' }
];
exports.orders = [
    { value: outlet_model_1.Raffle.S3, label: 'Swertres' },
    { value: outlet_model_1.Raffle.S3R, label: 'Rambolito' }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmV0LW1vZGVsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYmV0LW1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsK0NBQTRDO0FBRS9CLFFBQUEsS0FBSyxHQUFHO0lBQ25CLEVBQUMsS0FBSyxFQUFFLG1CQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUM7SUFDbkMsRUFBQyxLQUFLLEVBQUUsbUJBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBQztJQUNsQyxFQUFDLEtBQUssRUFBRSxtQkFBSSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFDO0NBQ25DLENBQUM7QUFFVyxRQUFBLE1BQU0sR0FBSTtJQUNyQixFQUFDLEtBQUssRUFBRSxxQkFBTSxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFDO0lBQ3JDLEVBQUMsS0FBSyxFQUFFLHFCQUFNLENBQUMsR0FBRyxFQUFHLEtBQUssRUFBRSxXQUFXLEVBQUM7Q0FDekMsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RHJhdywgUmFmZmxlfSBmcm9tIFwiLi9vdXRsZXQubW9kZWxcIjtcblxuZXhwb3J0IGNvbnN0IGNvZGVzID0gW1xuICB7dmFsdWU6IERyYXcuRFJBVzEsIGxhYmVsOiAnMTEtYW0nfSxcbiAge3ZhbHVlOiBEcmF3LkRSQVcyLCBsYWJlbDogJzQtcG0nfSxcbiAge3ZhbHVlOiBEcmF3LkRSQVczLCBsYWJlbDogJzktcG0nfVxuXTtcblxuZXhwb3J0IGNvbnN0IG9yZGVycyA9ICBbXG4gIHt2YWx1ZTogUmFmZmxlLlMzLCBsYWJlbDogJ1N3ZXJ0cmVzJ30sXG4gIHt2YWx1ZTogUmFmZmxlLlMzUiAsIGxhYmVsOiAnUmFtYm9saXRvJ31cbl07XG4iXX0=