"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var document_model_1 = require("./database/core/document.model");
var date_helper_1 = require("../services/helpers/date-helper");
var Draw;
(function (Draw) {
    Draw["ALL"] = "ALL";
    Draw["DRAW1"] = "d1";
    Draw["DRAW2"] = "d2";
    Draw["DRAW3"] = "d3";
})(Draw = exports.Draw || (exports.Draw = {}));
var Raffle;
(function (Raffle) {
    Raffle["S3"] = "s3";
    Raffle["S3R"] = "s3r";
})(Raffle = exports.Raffle || (exports.Raffle = {}));
var UserType;
(function (UserType) {
    UserType["ADMIN"] = "admin";
    UserType["BRANCH"] = "branch";
    UserType["OUTLET"] = "outlet";
    UserType["ENCODER"] = "encoder";
})(UserType = exports.UserType || (exports.UserType = {}));
// helper class for Settings
var RaffleLimit = /** @class */ (function () {
    // if from/to combination is negative, then it will apply to all possible combinations
    function RaffleLimit(raffle, limit, from, to) {
        this.raffle = raffle;
        this.limit = limit;
        this.from = from;
        this.to = to;
        if (to) {
            from = from < to ? from : to;
            to = to > from ? to : from;
        }
    }
    return RaffleLimit;
}());
exports.RaffleLimit = RaffleLimit;
var RafflePrize = /** @class */ (function () {
    function RafflePrize(raffle, unitPrize) {
        this.raffle = raffle;
        this.unitPrize = unitPrize;
    }
    return RafflePrize;
}());
exports.RafflePrize = RafflePrize;
// helper for transaction (stored in a transaction)
var Bet = /** @class */ (function () {
    function Bet(raffle, combination, amount, unitPrize) {
        this.raffle = raffle;
        this.combination = combination;
        this.amount = amount;
        this.unitPrize = unitPrize;
        if (!unitPrize) {
            this.unitPrize = 600; // win per peso for s3 and s3r
        }
    }
    return Bet;
}());
exports.Bet = Bet;
// helper for SummaryTrace (stored in SummaryTrace)
var SummaryTraceDetail = /** @class */ (function () {
    function SummaryTraceDetail(betId, amount) {
        this.betId = betId;
        this.amount = amount;
    }
    return SummaryTraceDetail;
}());
exports.SummaryTraceDetail = SummaryTraceDetail;
// helper class for Summary and MainSummary; contains transaction and bets
var SummaryTrace = /** @class */ (function () {
    function SummaryTrace(transactionId, details) {
        this.transactionId = transactionId;
        this.details = details;
        this.cancelled = false;
    }
    return SummaryTrace;
}());
exports.SummaryTrace = SummaryTrace;
var ReportDetail = /** @class */ (function () {
    function ReportDetail(transactionId, raffle, combination, amount) {
        this.transactionId = transactionId;
        this.raffle = raffle;
        this.combination = combination;
        this.amount = amount;
    }
    return ReportDetail;
}());
exports.ReportDetail = ReportDetail;
// The following are classes stored in the database.
var User = /** @class */ (function (_super) {
    __extends(User, _super);
    function User(lastname, firstname, userType, branchId, outletId) {
        var _this = _super.call(this, document_model_1.SystemType.USER) || this;
        _this.lastname = lastname;
        _this.firstname = firstname;
        _this.userType = userType;
        _this.branchId = branchId;
        _this.outletId = outletId;
        _this.username = _this._systemHeader.documentId.split('-', 1)[0];
        return _this;
    }
    return User;
}(document_model_1.Document));
exports.User = User;
var Settings = /** @class */ (function (_super) {
    __extends(Settings, _super);
    function Settings() {
        var _this = _super.call(this, document_model_1.SystemType.SETTINGS) || this;
        _this.limits = [];
        _this.prizes = [];
        return _this;
    }
    return Settings;
}(document_model_1.Document));
exports.Settings = Settings;
var Branch = /** @class */ (function (_super) {
    __extends(Branch, _super);
    function Branch(name) {
        var _this = _super.call(this, document_model_1.SystemType.BRANCH) || this;
        _this.name = name;
        _this.limits = [];
        _this.prizes = [];
        return _this;
    }
    return Branch;
}(document_model_1.Document));
exports.Branch = Branch;
var Outlet = /** @class */ (function (_super) {
    __extends(Outlet, _super);
    function Outlet(branchId, name) {
        var _this = _super.call(this, document_model_1.SystemType.OUTLET) || this;
        _this.branchId = branchId;
        _this.name = name;
        _this.limits = [];
        _this.prizes = [];
        return _this;
    }
    return Outlet;
}(document_model_1.Document));
exports.Outlet = Outlet;
var Transaction = /** @class */ (function (_super) {
    __extends(Transaction, _super);
    function Transaction(draw, bets) {
        var _this = _super.call(this, document_model_1.SystemType.TRANSACTION) || this;
        _this.draw = draw;
        _this.bets = bets;
        _this.cancelled = false;
        _this.date = date_helper_1.dateNoTime(new Date()).getTime();
        _this.transactionId = _this._systemHeader.documentId.split('-', 1)[0];
        // update total
        _this.total = 0;
        for (var i = 0; i < _this.bets.length; i++) {
            var bet = _this.bets[i];
            bet.id = _this.transactionId + '-' + i;
            _this.total += bet.amount;
        }
        return _this;
    }
    return Transaction;
}(document_model_1.Document));
exports.Transaction = Transaction;
var Summary = /** @class */ (function (_super) {
    __extends(Summary, _super);
    function Summary(date, draw, raffle, combination, unitPrize) {
        var _this = _super.call(this, document_model_1.SystemType.SUMMARY) || this;
        _this.date = date;
        _this.draw = draw;
        _this.raffle = raffle;
        _this.combination = combination;
        _this.unitPrize = unitPrize;
        _this.trace = [];
        _this.amount = 0;
        _this.win = false;
        // unique identifier for this summary is the combination and raffle
        _this.combinationId = Summary.combinationId(date, draw, raffle, combination);
        if (!unitPrize) {
            _this.unitPrize = 600; // win per peso for s3 and s3r
        }
        return _this;
    }
    Summary.combinationId = function (date, draw, raffle, combination) {
        return date + '-' + draw + '-' + raffle + '-' + combination;
    };
    return Summary;
}(document_model_1.Document));
exports.Summary = Summary;
var ReportSales = /** @class */ (function () {
    function ReportSales(total) {
        this.total = total;
        this.details = [];
    }
    return ReportSales;
}());
exports.ReportSales = ReportSales;
var ReportHits = /** @class */ (function () {
    function ReportHits(total) {
        this.total = total;
        this.details = [];
    }
    return ReportHits;
}());
exports.ReportHits = ReportHits;
var Report = /** @class */ (function (_super) {
    __extends(Report, _super);
    function Report(date, draw, outlet, sales, cancelled, hits) {
        var _this = _super.call(this, document_model_1.SystemType.OUTLET_REPORT) || this;
        _this.date = date;
        _this.draw = draw;
        _this.sales = sales;
        _this.cancelled = cancelled;
        _this.hits = hits;
        _this.branchId = outlet.branchId;
        _this.outletId = outlet._systemHeader.documentId;
        _this.gross = sales.total - cancelled.total - hits.total;
        return _this;
    }
    return Report;
}(document_model_1.Document));
exports.Report = Report;
var WinningNumber = /** @class */ (function (_super) {
    __extends(WinningNumber, _super);
    function WinningNumber(date, draw, raffle, combination) {
        var _this = _super.call(this, document_model_1.SystemType.WINNING_NUMBER) || this;
        _this.draw = draw;
        _this.raffle = raffle;
        _this.combination = combination;
        _this.date = date_helper_1.dateNoTime(date).getTime();
        _this._systemHeader.documentId = _this.date + '-' + _this.draw + '-' + _this.raffle;
        return _this;
    }
    return WinningNumber;
}(document_model_1.Document));
exports.WinningNumber = WinningNumber;
function createMockData() {
    // insert settings
    var settings = new Settings();
    settings.limits = [new RaffleLimit(Raffle.S3, 100, 0, 999)]; // limit for all outlets for s3 is 100 per combination
    // insert branches and outlets
    var delSur = new Branch('Davao del Sur');
    var ds1 = new Outlet(delSur._systemHeader.documentId, 'Digos 1');
    var ds2 = new Outlet(delSur._systemHeader.documentId, 'Digos 2');
    var davOcc = new Branch('Davao Occidental');
    var oc1 = new Outlet(davOcc._systemHeader.documentId, 'Malita 1');
    var oc2 = new Outlet(davOcc._systemHeader.documentId, 'Malita 2');
    // insert users
    // default super user
    var admin = new User('[DEFAULT-ADMIN]', '', UserType.ADMIN, null, null);
    // del sur users
    var delSurManager = new User('ALBOROTO', 'JOSE', UserType.BRANCH, delSur._systemHeader.documentId, null);
    var ds1Clerk = new User('RIZAL', 'JOSE', UserType.OUTLET, delSur._systemHeader.documentId, ds1._systemHeader.documentId);
    var ds2Clerk = new User('MAGDARO', 'FELONA', UserType.OUTLET, delSur._systemHeader.documentId, ds2._systemHeader.documentId);
    // occidental users
    var occidentalManager = new User('HASMIN', 'ADELLE', UserType.BRANCH, davOcc._systemHeader.documentId, null);
    var oc1Clerk = new User('ALPARATA', 'JOHN', UserType.OUTLET, davOcc._systemHeader.documentId, oc1._systemHeader.documentId);
    var oc2Clerk = new User('MAGDALENA', 'MARIA', UserType.OUTLET, davOcc._systemHeader.documentId, oc2._systemHeader.documentId);
    var date = date_helper_1.dateNoTime(new Date()).getTime();
    // insert sample reports
    var ds1Report = new Report(date, Draw.DRAW1, ds1, new ReportSales(15000), new ReportSales(0), new ReportHits(600));
    var ds2Report = new Report(date, Draw.DRAW1, ds2, new ReportSales(10000), new ReportSales(20), new ReportHits(0));
    var oc1Report = new Report(date, Draw.DRAW1, oc1, new ReportSales(9000), new ReportSales(0), new ReportHits(6000));
    var oc2Report = new Report(date, Draw.DRAW1, oc2, new ReportSales(20000), new ReportSales(0), new ReportHits(1200));
    return [
        settings,
        delSur, ds1, ds2, davOcc, oc1, oc2,
        admin, delSurManager, ds1Clerk, ds2Clerk, occidentalManager, oc1Clerk, oc2Clerk,
    ];
}
exports.createMockData = createMockData;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3V0bGV0Lm1vZGVsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsib3V0bGV0Lm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsaUVBQW9FO0FBR3BFLCtEQUEyRDtBQUUzRCxJQUFZLElBS1g7QUFMRCxXQUFZLElBQUk7SUFDZCxtQkFBVyxDQUFBO0lBQ1gsb0JBQVksQ0FBQTtJQUNaLG9CQUFZLENBQUE7SUFDWixvQkFBWSxDQUFBO0FBQ2QsQ0FBQyxFQUxXLElBQUksR0FBSixZQUFJLEtBQUosWUFBSSxRQUtmO0FBRUQsSUFBWSxNQUdYO0FBSEQsV0FBWSxNQUFNO0lBQ2hCLG1CQUFTLENBQUE7SUFDVCxxQkFBVyxDQUFBO0FBQ2IsQ0FBQyxFQUhXLE1BQU0sR0FBTixjQUFNLEtBQU4sY0FBTSxRQUdqQjtBQUVELElBQVksUUFLWDtBQUxELFdBQVksUUFBUTtJQUNsQiwyQkFBZSxDQUFBO0lBQ2YsNkJBQWlCLENBQUE7SUFDakIsNkJBQWlCLENBQUE7SUFDakIsK0JBQW1CLENBQUE7QUFDckIsQ0FBQyxFQUxXLFFBQVEsR0FBUixnQkFBUSxLQUFSLGdCQUFRLFFBS25CO0FBRUQsNEJBQTRCO0FBQzVCO0lBQ0Usc0ZBQXNGO0lBQ3RGLHFCQUFtQixNQUFjLEVBQVMsS0FBYSxFQUFTLElBQVksRUFBUyxFQUFXO1FBQTdFLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBUyxVQUFLLEdBQUwsS0FBSyxDQUFRO1FBQVMsU0FBSSxHQUFKLElBQUksQ0FBUTtRQUFTLE9BQUUsR0FBRixFQUFFLENBQVM7UUFDOUYsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNQLElBQUksR0FBRyxJQUFJLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUM3QixFQUFFLEdBQUcsRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDN0IsQ0FBQztJQUNILENBQUM7SUFDSCxrQkFBQztBQUFELENBQUMsQUFSRCxJQVFDO0FBUlksa0NBQVc7QUFVeEI7SUFDRSxxQkFBbUIsTUFBYyxFQUFTLFNBQWlCO1FBQXhDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBUyxjQUFTLEdBQVQsU0FBUyxDQUFRO0lBQUcsQ0FBQztJQUNqRSxrQkFBQztBQUFELENBQUMsQUFGRCxJQUVDO0FBRlksa0NBQVc7QUFJeEIsbURBQW1EO0FBQ25EO0lBSUUsYUFDUyxNQUFjLEVBQ2QsV0FBbUIsRUFDbkIsTUFBYyxFQUNkLFNBQWtCO1FBSGxCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxnQkFBVyxHQUFYLFdBQVcsQ0FBUTtRQUNuQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsY0FBUyxHQUFULFNBQVMsQ0FBUztRQUd6QixFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDZixJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxDQUFDLDhCQUE4QjtRQUN0RCxDQUFDO0lBRUgsQ0FBQztJQUVILFVBQUM7QUFBRCxDQUFDLEFBakJELElBaUJDO0FBakJZLGtCQUFHO0FBbUJoQixtREFBbUQ7QUFDbkQ7SUFDRSw0QkFBb0IsS0FBYSxFQUFTLE1BQWM7UUFBcEMsVUFBSyxHQUFMLEtBQUssQ0FBUTtRQUFTLFdBQU0sR0FBTixNQUFNLENBQVE7SUFBRyxDQUFDO0lBQzlELHlCQUFDO0FBQUQsQ0FBQyxBQUZELElBRUM7QUFGWSxnREFBa0I7QUFJL0IsMEVBQTBFO0FBQzFFO0lBRUUsc0JBQW1CLGFBQXFCLEVBQVMsT0FBNkI7UUFBM0Qsa0JBQWEsR0FBYixhQUFhLENBQVE7UUFBUyxZQUFPLEdBQVAsT0FBTyxDQUFzQjtRQUR2RSxjQUFTLEdBQUcsS0FBSyxDQUFDO0lBQ3dELENBQUM7SUFDcEYsbUJBQUM7QUFBRCxDQUFDLEFBSEQsSUFHQztBQUhZLG9DQUFZO0FBS3pCO0lBQ0Usc0JBQW1CLGFBQXFCLEVBQVMsTUFBYyxFQUFTLFdBQW1CLEVBQVMsTUFBYztRQUEvRixrQkFBYSxHQUFiLGFBQWEsQ0FBUTtRQUFTLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBUyxnQkFBVyxHQUFYLFdBQVcsQ0FBUTtRQUFTLFdBQU0sR0FBTixNQUFNLENBQVE7SUFBRSxDQUFDO0lBQ3ZILG1CQUFDO0FBQUQsQ0FBQyxBQUZELElBRUM7QUFGWSxvQ0FBWTtBQUl6QixvREFBb0Q7QUFFcEQ7SUFBMEIsd0JBQVE7SUFLOUIsY0FFTyxRQUFnQixFQUNoQixTQUFpQixFQUNqQixRQUFrQixFQUNsQixRQUFnQixFQUNoQixRQUFnQjtRQU52QixZQVFBLGtCQUFNLDJCQUFVLENBQUMsSUFBSSxDQUFDLFNBR3ZCO1FBVFEsY0FBUSxHQUFSLFFBQVEsQ0FBUTtRQUNoQixlQUFTLEdBQVQsU0FBUyxDQUFRO1FBQ2pCLGNBQVEsR0FBUixRQUFRLENBQVU7UUFDbEIsY0FBUSxHQUFSLFFBQVEsQ0FBUTtRQUNoQixjQUFRLEdBQVIsUUFBUSxDQUFRO1FBR3ZCLEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7SUFFakUsQ0FBQztJQUVILFdBQUM7QUFBRCxDQUFDLEFBbEJELENBQTBCLHlCQUFRLEdBa0JqQztBQWxCWSxvQkFBSTtBQW9CakI7SUFBOEIsNEJBQVE7SUFLcEM7UUFBQSxZQUNFLGtCQUFNLDJCQUFVLENBQUMsUUFBUSxDQUFDLFNBQzNCO1FBTE0sWUFBTSxHQUFrQixFQUFFLENBQUM7UUFDM0IsWUFBTSxHQUFrQixFQUFFLENBQUM7O0lBSWxDLENBQUM7SUFFSCxlQUFDO0FBQUQsQ0FBQyxBQVRELENBQThCLHlCQUFRLEdBU3JDO0FBVFksNEJBQVE7QUFXckI7SUFBNEIsMEJBQVE7SUFLbEMsZ0JBQW1CLElBQUk7UUFBdkIsWUFDRSxrQkFBTSwyQkFBVSxDQUFDLE1BQU0sQ0FBQyxTQUN6QjtRQUZrQixVQUFJLEdBQUosSUFBSSxDQUFBO1FBSGhCLFlBQU0sR0FBa0IsRUFBRSxDQUFDO1FBQzNCLFlBQU0sR0FBa0IsRUFBRSxDQUFDOztJQUlsQyxDQUFDO0lBRUgsYUFBQztBQUFELENBQUMsQUFURCxDQUE0Qix5QkFBUSxHQVNuQztBQVRZLHdCQUFNO0FBV25CO0lBQTRCLDBCQUFRO0lBS2xDLGdCQUFtQixRQUFnQixFQUFTLElBQUk7UUFBaEQsWUFDRSxrQkFBTSwyQkFBVSxDQUFDLE1BQU0sQ0FBQyxTQUN6QjtRQUZrQixjQUFRLEdBQVIsUUFBUSxDQUFRO1FBQVMsVUFBSSxHQUFKLElBQUksQ0FBQTtRQUh6QyxZQUFNLEdBQWtCLEVBQUUsQ0FBQztRQUMzQixZQUFNLEdBQWtCLEVBQUUsQ0FBQzs7SUFJbEMsQ0FBQztJQUVILGFBQUM7QUFBRCxDQUFDLEFBVEQsQ0FBNEIseUJBQVEsR0FTbkM7QUFUWSx3QkFBTTtBQVduQjtJQUFpQywrQkFBUTtJQU92QyxxQkFBbUIsSUFBVSxFQUFTLElBQVc7UUFBakQsWUFFRSxrQkFBTSwyQkFBVSxDQUFDLFdBQVcsQ0FBQyxTQWE5QjtRQWZrQixVQUFJLEdBQUosSUFBSSxDQUFNO1FBQVMsVUFBSSxHQUFKLElBQUksQ0FBTztRQUYxQyxlQUFTLEdBQUcsS0FBSyxDQUFDO1FBTXZCLEtBQUksQ0FBQyxJQUFJLEdBQUcsd0JBQVUsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDN0MsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRXBFLGVBQWU7UUFDZixLQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNmLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUMxQyxJQUFNLEdBQUcsR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLEdBQUcsQ0FBQyxFQUFFLEdBQUcsS0FBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQ3RDLEtBQUksQ0FBQyxLQUFLLElBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUMzQixDQUFDOztJQUVILENBQUM7SUFFSCxrQkFBQztBQUFELENBQUMsQUF4QkQsQ0FBaUMseUJBQVEsR0F3QnhDO0FBeEJZLGtDQUFXO0FBMEJ4QjtJQUE2QiwyQkFBUTtJQU9uQyxpQkFBbUIsSUFBWSxFQUFTLElBQVUsRUFBUyxNQUFjLEVBQVMsV0FBbUIsRUFBUyxTQUFrQjtRQUFoSSxZQUVFLGtCQUFNLDJCQUFVLENBQUMsT0FBTyxDQUFDLFNBUTFCO1FBVmtCLFVBQUksR0FBSixJQUFJLENBQVE7UUFBUyxVQUFJLEdBQUosSUFBSSxDQUFNO1FBQVMsWUFBTSxHQUFOLE1BQU0sQ0FBUTtRQUFTLGlCQUFXLEdBQVgsV0FBVyxDQUFRO1FBQVMsZUFBUyxHQUFULFNBQVMsQ0FBUztRQUp6SCxXQUFLLEdBQW1CLEVBQUUsQ0FBQztRQUMzQixZQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ1gsU0FBRyxHQUFHLEtBQUssQ0FBQztRQUtqQixtRUFBbUU7UUFDbkUsS0FBSSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBRTVFLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNmLEtBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDLENBQUMsOEJBQThCO1FBQ3RELENBQUM7O0lBRUgsQ0FBQztJQUVhLHFCQUFhLEdBQTNCLFVBQTRCLElBQVksRUFBRSxJQUFVLEVBQUUsTUFBYyxFQUFFLFdBQW1CO1FBQ3ZGLE1BQU0sQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLElBQUksR0FBRyxHQUFHLEdBQUcsTUFBTSxHQUFHLEdBQUcsR0FBRyxXQUFXLENBQUM7SUFDOUQsQ0FBQztJQUNILGNBQUM7QUFBRCxDQUFDLEFBdEJELENBQTZCLHlCQUFRLEdBc0JwQztBQXRCWSwwQkFBTztBQXdCcEI7SUFJRSxxQkFBbUIsS0FBYTtRQUFiLFVBQUssR0FBTCxLQUFLLENBQVE7UUFGekIsWUFBTyxHQUFtQixFQUFFLENBQUM7SUFJcEMsQ0FBQztJQUVILGtCQUFDO0FBQUQsQ0FBQyxBQVJELElBUUM7QUFSWSxrQ0FBVztBQVV4QjtJQUlFLG9CQUFtQixLQUFhO1FBQWIsVUFBSyxHQUFMLEtBQUssQ0FBUTtRQUZ6QixZQUFPLEdBQW1CLEVBQUUsQ0FBQztJQUlwQyxDQUFDO0lBRUgsaUJBQUM7QUFBRCxDQUFDLEFBUkQsSUFRQztBQVJZLGdDQUFVO0FBVXZCO0lBQTRCLDBCQUFRO0lBT2xDLGdCQUFtQixJQUFZLEVBQVMsSUFBVSxFQUFFLE1BQWMsRUFDL0MsS0FBa0IsRUFBUyxTQUFzQixFQUFTLElBQWdCO1FBRDdGLFlBR0Usa0JBQU0sMkJBQVUsQ0FBQyxhQUFhLENBQUMsU0FNaEM7UUFUa0IsVUFBSSxHQUFKLElBQUksQ0FBUTtRQUFTLFVBQUksR0FBSixJQUFJLENBQU07UUFDL0IsV0FBSyxHQUFMLEtBQUssQ0FBYTtRQUFTLGVBQVMsR0FBVCxTQUFTLENBQWE7UUFBUyxVQUFJLEdBQUosSUFBSSxDQUFZO1FBSTNGLEtBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNoQyxLQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDO1FBQ2hELEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7O0lBRTFELENBQUM7SUFDSCxhQUFDO0FBQUQsQ0FBQyxBQWpCRCxDQUE0Qix5QkFBUSxHQWlCbkM7QUFqQlksd0JBQU07QUFtQm5CO0lBQW1DLGlDQUFRO0lBSXpDLHVCQUFZLElBQVUsRUFBUyxJQUFVLEVBQVMsTUFBYyxFQUFTLFdBQW1CO1FBQTVGLFlBRUUsa0JBQU0sMkJBQVUsQ0FBQyxjQUFjLENBQUMsU0FLakM7UUFQOEIsVUFBSSxHQUFKLElBQUksQ0FBTTtRQUFTLFlBQU0sR0FBTixNQUFNLENBQVE7UUFBUyxpQkFBVyxHQUFYLFdBQVcsQ0FBUTtRQUkxRixLQUFJLENBQUMsSUFBSSxHQUFHLHdCQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDdkMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsS0FBSSxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQzs7SUFFbEYsQ0FBQztJQUNILG9CQUFDO0FBQUQsQ0FBQyxBQVpELENBQW1DLHlCQUFRLEdBWTFDO0FBWlksc0NBQWE7QUFjMUI7SUFDRSxrQkFBa0I7SUFFbEIsSUFBTSxRQUFRLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztJQUNoQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxXQUFXLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxzREFBc0Q7SUFHbkgsOEJBQThCO0lBQzlCLElBQU0sTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQzNDLElBQU0sR0FBRyxHQUFJLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQ3BFLElBQU0sR0FBRyxHQUFJLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBRXBFLElBQU0sTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDOUMsSUFBTSxHQUFHLEdBQUksSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDckUsSUFBTSxHQUFHLEdBQUksSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFFckUsZUFBZTtJQUVmLHFCQUFxQjtJQUNyQixJQUFNLEtBQUssR0FBRyxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLEVBQUUsUUFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFFMUUsZ0JBQWdCO0lBQ2hCLElBQU0sYUFBYSxHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUUsUUFBUSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMzRyxJQUFNLFFBQVEsR0FBRyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMzSCxJQUFNLFFBQVEsR0FBRyxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLFFBQVEsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUUvSCxtQkFBbUI7SUFDbkIsSUFBTSxpQkFBaUIsR0FBRyxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLFFBQVEsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDL0csSUFBTSxRQUFRLEdBQUcsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDOUgsSUFBTSxRQUFRLEdBQUcsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7SUFHaEksSUFBTSxJQUFJLEdBQUcsd0JBQVUsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7SUFFOUMsd0JBQXdCO0lBQ3hCLElBQU0sU0FBUyxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBRSxDQUFDO0lBQ3RILElBQU0sU0FBUyxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBRSxDQUFDO0lBRXJILElBQU0sU0FBUyxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBRSxDQUFDO0lBQ3RILElBQU0sU0FBUyxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBRSxDQUFDO0lBR3ZILE1BQU0sQ0FBQztRQUNMLFFBQVE7UUFDUixNQUFNLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLEdBQUc7UUFDbEMsS0FBSyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLGlCQUFpQixFQUFFLFFBQVEsRUFBRSxRQUFRO0tBRWhGLENBQUM7QUFFSixDQUFDO0FBakRELHdDQWlEQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RG9jdW1lbnQsIFN5c3RlbVR5cGV9IGZyb20gJy4vZGF0YWJhc2UvY29yZS9kb2N1bWVudC5tb2RlbCc7XG5cbmltcG9ydCB7cmFtYm9saXRvfSBmcm9tICcuLi9zZXJ2aWNlcy9oZWxwZXJzL251bWJlci1oZWxwZXInO1xuaW1wb3J0IHtkYXRlTm9UaW1lfSBmcm9tICcuLi9zZXJ2aWNlcy9oZWxwZXJzL2RhdGUtaGVscGVyJztcblxuZXhwb3J0IGVudW0gRHJhdyB7XG4gIEFMTCA9ICdBTEwnLFxuICBEUkFXMSA9ICdkMScsXG4gIERSQVcyID0gJ2QyJyxcbiAgRFJBVzMgPSAnZDMnXG59XG5cbmV4cG9ydCBlbnVtIFJhZmZsZSB7XG4gIFMzID0gJ3MzJyxcbiAgUzNSID0gJ3Mzcidcbn1cblxuZXhwb3J0IGVudW0gVXNlclR5cGUge1xuICBBRE1JTiA9ICdhZG1pbicsXG4gIEJSQU5DSCA9ICdicmFuY2gnLFxuICBPVVRMRVQgPSAnb3V0bGV0JyxcbiAgRU5DT0RFUiA9ICdlbmNvZGVyJ1xufVxuXG4vLyBoZWxwZXIgY2xhc3MgZm9yIFNldHRpbmdzXG5leHBvcnQgY2xhc3MgUmFmZmxlTGltaXQge1xuICAvLyBpZiBmcm9tL3RvIGNvbWJpbmF0aW9uIGlzIG5lZ2F0aXZlLCB0aGVuIGl0IHdpbGwgYXBwbHkgdG8gYWxsIHBvc3NpYmxlIGNvbWJpbmF0aW9uc1xuICBjb25zdHJ1Y3RvcihwdWJsaWMgcmFmZmxlOiBSYWZmbGUsIHB1YmxpYyBsaW1pdDogbnVtYmVyLCBwdWJsaWMgZnJvbTogbnVtYmVyLCBwdWJsaWMgdG8/OiBudW1iZXIpIHtcbiAgICBpZiAodG8pIHtcbiAgICAgIGZyb20gPSBmcm9tIDwgdG8gPyBmcm9tIDogdG87XG4gICAgICB0byA9IHRvID4gZnJvbSA/IHRvIDogZnJvbTtcbiAgICB9XG4gIH1cbn1cblxuZXhwb3J0IGNsYXNzIFJhZmZsZVByaXplIHtcbiAgY29uc3RydWN0b3IocHVibGljIHJhZmZsZTogUmFmZmxlLCBwdWJsaWMgdW5pdFByaXplOiBudW1iZXIpIHt9XG59XG5cbi8vIGhlbHBlciBmb3IgdHJhbnNhY3Rpb24gKHN0b3JlZCBpbiBhIHRyYW5zYWN0aW9uKVxuZXhwb3J0IGNsYXNzIEJldCB7XG5cbiAgcHVibGljIGlkOiBzdHJpbmc7IC8vIHRoZSBpbmRleCBvZiB0aGlzIGJldCB3aGVuIHN0b3JlZCBpbnNpZGUgdGhlIHRyYW5zYWN0aW9uLmJldHMgYXJyYXlcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgcmFmZmxlOiBSYWZmbGUsXG4gICAgcHVibGljIGNvbWJpbmF0aW9uOiBudW1iZXIsXG4gICAgcHVibGljIGFtb3VudDogbnVtYmVyLFxuICAgIHB1YmxpYyB1bml0UHJpemU/OiBudW1iZXJcbiAgKSB7XG5cbiAgICBpZiAoIXVuaXRQcml6ZSkge1xuICAgICAgdGhpcy51bml0UHJpemUgPSA2MDA7IC8vIHdpbiBwZXIgcGVzbyBmb3IgczMgYW5kIHMzclxuICAgIH1cblxuICB9XG5cbn1cblxuLy8gaGVscGVyIGZvciBTdW1tYXJ5VHJhY2UgKHN0b3JlZCBpbiBTdW1tYXJ5VHJhY2UpXG5leHBvcnQgY2xhc3MgU3VtbWFyeVRyYWNlRGV0YWlsIHtcbiAgY29uc3RydWN0b3IgKHB1YmxpYyBiZXRJZDogc3RyaW5nLCBwdWJsaWMgYW1vdW50OiBudW1iZXIpIHt9XG59XG5cbi8vIGhlbHBlciBjbGFzcyBmb3IgU3VtbWFyeSBhbmQgTWFpblN1bW1hcnk7IGNvbnRhaW5zIHRyYW5zYWN0aW9uIGFuZCBiZXRzXG5leHBvcnQgY2xhc3MgU3VtbWFyeVRyYWNlIHtcbiAgcHVibGljIGNhbmNlbGxlZCA9IGZhbHNlO1xuICBjb25zdHJ1Y3RvcihwdWJsaWMgdHJhbnNhY3Rpb25JZDogc3RyaW5nLCBwdWJsaWMgZGV0YWlsczogU3VtbWFyeVRyYWNlRGV0YWlsW10pIHt9XG59XG5cbmV4cG9ydCBjbGFzcyBSZXBvcnREZXRhaWwge1xuICBjb25zdHJ1Y3RvcihwdWJsaWMgdHJhbnNhY3Rpb25JZDogc3RyaW5nLCBwdWJsaWMgcmFmZmxlOiBSYWZmbGUsIHB1YmxpYyBjb21iaW5hdGlvbjogbnVtYmVyLCBwdWJsaWMgYW1vdW50OiBudW1iZXIpe31cbn1cblxuLy8gVGhlIGZvbGxvd2luZyBhcmUgY2xhc3NlcyBzdG9yZWQgaW4gdGhlIGRhdGFiYXNlLlxuXG5leHBvcnQgY2xhc3MgVXNlciBleHRlbmRzIERvY3VtZW50IHtcblxuICBwdWJsaWMgcGFzc3dvcmQ6IHN0cmluZzsgLy8gZW5jcnlwdGVkIHBhc3N3b3JkXG4gIHB1YmxpYyB1c2VybmFtZTogc3RyaW5nOyAvLyBnZW5lcmF0ZWRcblxuICAgIGNvbnN0cnVjdG9yXG4gIChcbiAgICBwdWJsaWMgbGFzdG5hbWU6IHN0cmluZyxcbiAgICBwdWJsaWMgZmlyc3RuYW1lOiBzdHJpbmcsXG4gICAgcHVibGljIHVzZXJUeXBlOiBVc2VyVHlwZSxcbiAgICBwdWJsaWMgYnJhbmNoSWQ6IHN0cmluZyxcbiAgICBwdWJsaWMgb3V0bGV0SWQ6IHN0cmluZ1xuICApIHtcbiAgICBzdXBlcihTeXN0ZW1UeXBlLlVTRVIpOyAvLyBjYWxsIERvY3VtZW50IGNvbnN0cnVjdG9yXG4gICAgdGhpcy51c2VybmFtZSA9IHRoaXMuX3N5c3RlbUhlYWRlci5kb2N1bWVudElkLnNwbGl0KCctJywgMSlbMF07XG5cbiAgfVxuXG59XG5cbmV4cG9ydCBjbGFzcyBTZXR0aW5ncyBleHRlbmRzIERvY3VtZW50IHtcblxuICBwdWJsaWMgbGltaXRzOiBSYWZmbGVMaW1pdFtdID0gW107XG4gIHB1YmxpYyBwcml6ZXM6IFJhZmZsZVByaXplW10gPSBbXTtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcihTeXN0ZW1UeXBlLlNFVFRJTkdTKTtcbiAgfVxuXG59XG5cbmV4cG9ydCBjbGFzcyBCcmFuY2ggZXh0ZW5kcyBEb2N1bWVudCB7XG5cbiAgcHVibGljIGxpbWl0czogUmFmZmxlTGltaXRbXSA9IFtdO1xuICBwdWJsaWMgcHJpemVzOiBSYWZmbGVQcml6ZVtdID0gW107XG5cbiAgY29uc3RydWN0b3IocHVibGljIG5hbWUpIHtcbiAgICBzdXBlcihTeXN0ZW1UeXBlLkJSQU5DSCk7XG4gIH1cblxufVxuXG5leHBvcnQgY2xhc3MgT3V0bGV0IGV4dGVuZHMgRG9jdW1lbnQge1xuXG4gIHB1YmxpYyBsaW1pdHM6IFJhZmZsZUxpbWl0W10gPSBbXTtcbiAgcHVibGljIHByaXplczogUmFmZmxlUHJpemVbXSA9IFtdO1xuXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBicmFuY2hJZDogc3RyaW5nLCBwdWJsaWMgbmFtZSkge1xuICAgIHN1cGVyKFN5c3RlbVR5cGUuT1VUTEVUKTtcbiAgfVxuXG59XG5cbmV4cG9ydCBjbGFzcyBUcmFuc2FjdGlvbiBleHRlbmRzIERvY3VtZW50IHtcblxuICBwdWJsaWMgZGF0ZTogbnVtYmVyO1xuICBwdWJsaWMgdHJhbnNhY3Rpb25JZDogc3RyaW5nO1xuICBwdWJsaWMgdG90YWw6IG51bWJlcjtcbiAgcHVibGljIGNhbmNlbGxlZCA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBkcmF3OiBEcmF3LCBwdWJsaWMgYmV0czogQmV0W10pIHtcblxuICAgIHN1cGVyKFN5c3RlbVR5cGUuVFJBTlNBQ1RJT04pOyAvLyBjYWxsIERvY3VtZW50IGNvbnN0cnVjdG9yXG5cbiAgICB0aGlzLmRhdGUgPSBkYXRlTm9UaW1lKG5ldyBEYXRlKCkpLmdldFRpbWUoKTtcbiAgICB0aGlzLnRyYW5zYWN0aW9uSWQgPSB0aGlzLl9zeXN0ZW1IZWFkZXIuZG9jdW1lbnRJZC5zcGxpdCgnLScsIDEpWzBdO1xuXG4gICAgLy8gdXBkYXRlIHRvdGFsXG4gICAgdGhpcy50b3RhbCA9IDA7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmJldHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGNvbnN0IGJldCA9IHRoaXMuYmV0c1tpXTtcbiAgICAgIGJldC5pZCA9IHRoaXMudHJhbnNhY3Rpb25JZCArICctJyArIGk7XG4gICAgICB0aGlzLnRvdGFsICs9IGJldC5hbW91bnQ7XG4gICAgfVxuXG4gIH1cblxufVxuXG5leHBvcnQgY2xhc3MgU3VtbWFyeSBleHRlbmRzIERvY3VtZW50IHtcblxuICBwdWJsaWMgY29tYmluYXRpb25JZDogc3RyaW5nO1xuICBwdWJsaWMgdHJhY2U6IFN1bW1hcnlUcmFjZVtdID0gW107XG4gIHB1YmxpYyBhbW91bnQgPSAwO1xuICBwdWJsaWMgd2luID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IocHVibGljIGRhdGU6IG51bWJlciwgcHVibGljIGRyYXc6IERyYXcsIHB1YmxpYyByYWZmbGU6IFJhZmZsZSwgcHVibGljIGNvbWJpbmF0aW9uOiBudW1iZXIsIHB1YmxpYyB1bml0UHJpemU/OiBudW1iZXIpIHtcblxuICAgIHN1cGVyKFN5c3RlbVR5cGUuU1VNTUFSWSk7XG4gICAgLy8gdW5pcXVlIGlkZW50aWZpZXIgZm9yIHRoaXMgc3VtbWFyeSBpcyB0aGUgY29tYmluYXRpb24gYW5kIHJhZmZsZVxuICAgIHRoaXMuY29tYmluYXRpb25JZCA9IFN1bW1hcnkuY29tYmluYXRpb25JZChkYXRlLCBkcmF3LCByYWZmbGUsIGNvbWJpbmF0aW9uKTtcblxuICAgIGlmICghdW5pdFByaXplKSB7XG4gICAgICB0aGlzLnVuaXRQcml6ZSA9IDYwMDsgLy8gd2luIHBlciBwZXNvIGZvciBzMyBhbmQgczNyXG4gICAgfVxuXG4gIH1cblxuICBwdWJsaWMgc3RhdGljIGNvbWJpbmF0aW9uSWQoZGF0ZTogbnVtYmVyLCBkcmF3OiBEcmF3LCByYWZmbGU6IFJhZmZsZSwgY29tYmluYXRpb246IG51bWJlcikge1xuICAgIHJldHVybiBkYXRlICsgJy0nICsgZHJhdyArICctJyArIHJhZmZsZSArICctJyArIGNvbWJpbmF0aW9uO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBSZXBvcnRTYWxlcyB7XG5cbiAgcHVibGljIGRldGFpbHM6IFJlcG9ydERldGFpbFtdID0gW107XG5cbiAgY29uc3RydWN0b3IocHVibGljIHRvdGFsOiBudW1iZXIpIHtcblxuICB9XG5cbn1cblxuZXhwb3J0IGNsYXNzIFJlcG9ydEhpdHMge1xuXG4gIHB1YmxpYyBkZXRhaWxzOiBSZXBvcnREZXRhaWxbXSA9IFtdO1xuXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyB0b3RhbDogbnVtYmVyKSB7XG5cbiAgfVxuXG59XG5cbmV4cG9ydCBjbGFzcyBSZXBvcnQgZXh0ZW5kcyBEb2N1bWVudCB7XG5cbiAgcHVibGljIGJyYW5jaElkOiBzdHJpbmc7XG4gIHB1YmxpYyBvdXRsZXRJZDogc3RyaW5nO1xuXG4gIHB1YmxpYyBncm9zczogbnVtYmVyO1xuXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBkYXRlOiBudW1iZXIsIHB1YmxpYyBkcmF3OiBEcmF3LCBvdXRsZXQ6IE91dGxldCxcbiAgICAgICAgICAgICAgcHVibGljIHNhbGVzOiBSZXBvcnRTYWxlcywgcHVibGljIGNhbmNlbGxlZDogUmVwb3J0U2FsZXMsIHB1YmxpYyBoaXRzOiBSZXBvcnRIaXRzKSB7XG5cbiAgICBzdXBlcihTeXN0ZW1UeXBlLk9VVExFVF9SRVBPUlQpO1xuXG4gICAgdGhpcy5icmFuY2hJZCA9IG91dGxldC5icmFuY2hJZDtcbiAgICB0aGlzLm91dGxldElkID0gb3V0bGV0Ll9zeXN0ZW1IZWFkZXIuZG9jdW1lbnRJZDtcbiAgICB0aGlzLmdyb3NzID0gc2FsZXMudG90YWwgLSBjYW5jZWxsZWQudG90YWwgLSBoaXRzLnRvdGFsO1xuXG4gIH1cbn1cblxuZXhwb3J0IGNsYXNzIFdpbm5pbmdOdW1iZXIgZXh0ZW5kcyBEb2N1bWVudCB7XG5cbiAgcHVibGljIGRhdGU6IG51bWJlcjtcblxuICBjb25zdHJ1Y3RvcihkYXRlOiBEYXRlLCBwdWJsaWMgZHJhdzogRHJhdywgcHVibGljIHJhZmZsZTogUmFmZmxlLCBwdWJsaWMgY29tYmluYXRpb246IG51bWJlciApIHtcblxuICAgIHN1cGVyKFN5c3RlbVR5cGUuV0lOTklOR19OVU1CRVIpO1xuXG4gICAgdGhpcy5kYXRlID0gZGF0ZU5vVGltZShkYXRlKS5nZXRUaW1lKCk7XG4gICAgdGhpcy5fc3lzdGVtSGVhZGVyLmRvY3VtZW50SWQgPSB0aGlzLmRhdGUgKyAnLScgKyB0aGlzLmRyYXcgKyAnLScgKyB0aGlzLnJhZmZsZTtcblxuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVNb2NrRGF0YSgpIHtcbiAgLy8gaW5zZXJ0IHNldHRpbmdzXG5cbiAgY29uc3Qgc2V0dGluZ3MgPSBuZXcgU2V0dGluZ3MoKTtcbiAgc2V0dGluZ3MubGltaXRzID0gW25ldyBSYWZmbGVMaW1pdChSYWZmbGUuUzMsIDEwMCwgMCwgOTk5KV07IC8vIGxpbWl0IGZvciBhbGwgb3V0bGV0cyBmb3IgczMgaXMgMTAwIHBlciBjb21iaW5hdGlvblxuXG5cbiAgLy8gaW5zZXJ0IGJyYW5jaGVzIGFuZCBvdXRsZXRzXG4gIGNvbnN0IGRlbFN1ciA9IG5ldyBCcmFuY2goJ0RhdmFvIGRlbCBTdXInKTtcbiAgY29uc3QgZHMxICA9IG5ldyBPdXRsZXQoZGVsU3VyLl9zeXN0ZW1IZWFkZXIuZG9jdW1lbnRJZCwgJ0RpZ29zIDEnKTtcbiAgY29uc3QgZHMyICA9IG5ldyBPdXRsZXQoZGVsU3VyLl9zeXN0ZW1IZWFkZXIuZG9jdW1lbnRJZCwgJ0RpZ29zIDInKTtcblxuICBjb25zdCBkYXZPY2MgPSBuZXcgQnJhbmNoKCdEYXZhbyBPY2NpZGVudGFsJyk7XG4gIGNvbnN0IG9jMSAgPSBuZXcgT3V0bGV0KGRhdk9jYy5fc3lzdGVtSGVhZGVyLmRvY3VtZW50SWQsICdNYWxpdGEgMScpO1xuICBjb25zdCBvYzIgID0gbmV3IE91dGxldChkYXZPY2MuX3N5c3RlbUhlYWRlci5kb2N1bWVudElkLCAnTWFsaXRhIDInKTtcblxuICAvLyBpbnNlcnQgdXNlcnNcblxuICAvLyBkZWZhdWx0IHN1cGVyIHVzZXJcbiAgY29uc3QgYWRtaW4gPSBuZXcgVXNlcignW0RFRkFVTFQtQURNSU5dJywgJycsIFVzZXJUeXBlLkFETUlOLCBudWxsLCBudWxsKTtcblxuICAvLyBkZWwgc3VyIHVzZXJzXG4gIGNvbnN0IGRlbFN1ck1hbmFnZXIgPSBuZXcgVXNlcignQUxCT1JPVE8nLCAnSk9TRScsIFVzZXJUeXBlLkJSQU5DSCwgZGVsU3VyLl9zeXN0ZW1IZWFkZXIuZG9jdW1lbnRJZCwgbnVsbCk7XG4gIGNvbnN0IGRzMUNsZXJrID0gbmV3IFVzZXIoJ1JJWkFMJywgJ0pPU0UnLCBVc2VyVHlwZS5PVVRMRVQsIGRlbFN1ci5fc3lzdGVtSGVhZGVyLmRvY3VtZW50SWQsIGRzMS5fc3lzdGVtSGVhZGVyLmRvY3VtZW50SWQpO1xuICBjb25zdCBkczJDbGVyayA9IG5ldyBVc2VyKCdNQUdEQVJPJywgJ0ZFTE9OQScsIFVzZXJUeXBlLk9VVExFVCwgZGVsU3VyLl9zeXN0ZW1IZWFkZXIuZG9jdW1lbnRJZCwgZHMyLl9zeXN0ZW1IZWFkZXIuZG9jdW1lbnRJZCk7XG5cbiAgLy8gb2NjaWRlbnRhbCB1c2Vyc1xuICBjb25zdCBvY2NpZGVudGFsTWFuYWdlciA9IG5ldyBVc2VyKCdIQVNNSU4nLCAnQURFTExFJywgVXNlclR5cGUuQlJBTkNILCBkYXZPY2MuX3N5c3RlbUhlYWRlci5kb2N1bWVudElkLCBudWxsKTtcbiAgY29uc3Qgb2MxQ2xlcmsgPSBuZXcgVXNlcignQUxQQVJBVEEnLCAnSk9ITicsIFVzZXJUeXBlLk9VVExFVCwgZGF2T2NjLl9zeXN0ZW1IZWFkZXIuZG9jdW1lbnRJZCwgb2MxLl9zeXN0ZW1IZWFkZXIuZG9jdW1lbnRJZCk7XG4gIGNvbnN0IG9jMkNsZXJrID0gbmV3IFVzZXIoJ01BR0RBTEVOQScsICdNQVJJQScsIFVzZXJUeXBlLk9VVExFVCwgZGF2T2NjLl9zeXN0ZW1IZWFkZXIuZG9jdW1lbnRJZCwgb2MyLl9zeXN0ZW1IZWFkZXIuZG9jdW1lbnRJZCk7XG5cblxuICBjb25zdCBkYXRlID0gZGF0ZU5vVGltZShuZXcgRGF0ZSgpKS5nZXRUaW1lKCk7XG5cbiAgLy8gaW5zZXJ0IHNhbXBsZSByZXBvcnRzXG4gIGNvbnN0IGRzMVJlcG9ydCA9IG5ldyBSZXBvcnQoZGF0ZSwgRHJhdy5EUkFXMSwgZHMxLCBuZXcgUmVwb3J0U2FsZXMoMTUwMDApLCBuZXcgUmVwb3J0U2FsZXMoMCksIG5ldyBSZXBvcnRIaXRzKDYwMCkgKTtcbiAgY29uc3QgZHMyUmVwb3J0ID0gbmV3IFJlcG9ydChkYXRlLCBEcmF3LkRSQVcxLCBkczIsIG5ldyBSZXBvcnRTYWxlcygxMDAwMCksIG5ldyBSZXBvcnRTYWxlcygyMCksIG5ldyBSZXBvcnRIaXRzKDApICk7XG5cbiAgY29uc3Qgb2MxUmVwb3J0ID0gbmV3IFJlcG9ydChkYXRlLCBEcmF3LkRSQVcxLCBvYzEsIG5ldyBSZXBvcnRTYWxlcyg5MDAwKSwgbmV3IFJlcG9ydFNhbGVzKDApLCBuZXcgUmVwb3J0SGl0cyg2MDAwKSApO1xuICBjb25zdCBvYzJSZXBvcnQgPSBuZXcgUmVwb3J0KGRhdGUsIERyYXcuRFJBVzEsIG9jMiwgbmV3IFJlcG9ydFNhbGVzKDIwMDAwKSwgbmV3IFJlcG9ydFNhbGVzKDApLCBuZXcgUmVwb3J0SGl0cygxMjAwKSApO1xuXG5cbiAgcmV0dXJuIFtcbiAgICBzZXR0aW5ncyxcbiAgICBkZWxTdXIsIGRzMSwgZHMyLCBkYXZPY2MsIG9jMSwgb2MyLFxuICAgIGFkbWluLCBkZWxTdXJNYW5hZ2VyLCBkczFDbGVyaywgZHMyQ2xlcmssIG9jY2lkZW50YWxNYW5hZ2VyLCBvYzFDbGVyaywgb2MyQ2xlcmssXG4gICAgLy8gZHMxUmVwb3J0LCBkczJSZXBvcnQsIG9jMVJlcG9ydCwgb2MyUmVwb3J0XG4gIF07XG5cbn1cbiJdfQ==