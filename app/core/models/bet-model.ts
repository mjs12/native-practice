import {Draw, Raffle} from "./outlet.model";

export const codes = [
  {value: Draw.DRAW1, label: '11-am'},
  {value: Draw.DRAW2, label: '4-pm'},
  {value: Draw.DRAW3, label: '9-pm'}
];

export const orders =  [
  {value: Raffle.S3, label: 'Swertres'},
  {value: Raffle.S3R , label: 'Rambolito'}
];
