"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var outlet_model_1 = require("./outlet.model");
exports.adminDraw = [
    { value: outlet_model_1.Draw.DRAW1, label: '11-am' },
    { value: outlet_model_1.Draw.DRAW2, label: '4-pm' },
    { value: outlet_model_1.Draw.DRAW3, label: '9-pm' }
];
var ReportSummary = /** @class */ (function () {
    function ReportSummary(totalSales, totalCancelledSales, totalHits, gross) {
        this.totalSales = totalSales;
        this.totalCancelledSales = totalCancelledSales;
        this.totalHits = totalHits;
        this.gross = gross;
    }
    return ReportSummary;
}());
exports.ReportSummary = ReportSummary;
var FormUser = /** @class */ (function () {
    function FormUser(lastname, firstname, userType, branchId, outletId, username) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.userType = userType;
        this.branchId = branchId;
        this.outletId = outletId;
        this.username = username;
    }
    return FormUser;
}());
exports.FormUser = FormUser;
var FormSummary = /** @class */ (function () {
    function FormSummary(from, to, draw, branch, outlet) {
        this.from = from;
        this.to = to;
        this.draw = draw;
        this.branch = branch;
        this.outlet = outlet;
    }
    return FormSummary;
}());
exports.FormSummary = FormSummary;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRtaW4tbW9kZWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhZG1pbi1tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLCtDQUE4QztBQUVqQyxRQUFBLFNBQVMsR0FBRztJQUN2QixFQUFDLEtBQUssRUFBRSxtQkFBSSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFDO0lBQ25DLEVBQUMsS0FBSyxFQUFFLG1CQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUM7SUFDbEMsRUFBQyxLQUFLLEVBQUUsbUJBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBQztDQUNuQyxDQUFDO0FBR0Y7SUFDRSx1QkFDUyxVQUFrQixFQUNsQixtQkFBMkIsRUFDM0IsU0FBaUIsRUFDakIsS0FBYTtRQUhiLGVBQVUsR0FBVixVQUFVLENBQVE7UUFDbEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFRO1FBQzNCLGNBQVMsR0FBVCxTQUFTLENBQVE7UUFDakIsVUFBSyxHQUFMLEtBQUssQ0FBUTtJQUNuQixDQUFDO0lBQ04sb0JBQUM7QUFBRCxDQUFDLEFBUEQsSUFPQztBQVBZLHNDQUFhO0FBUzFCO0lBQ0Usa0JBQ1MsUUFBZ0IsRUFDaEIsU0FBaUIsRUFDakIsUUFBa0IsRUFDbEIsUUFBZ0IsRUFDaEIsUUFBZ0IsRUFDaEIsUUFBaUI7UUFMakIsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUNoQixjQUFTLEdBQVQsU0FBUyxDQUFRO1FBQ2pCLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDbEIsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUNoQixhQUFRLEdBQVIsUUFBUSxDQUFRO1FBQ2hCLGFBQVEsR0FBUixRQUFRLENBQVM7SUFDeEIsQ0FBQztJQUNMLGVBQUM7QUFBRCxDQUFDLEFBVEQsSUFTQztBQVRZLDRCQUFRO0FBV3JCO0lBQ0UscUJBQ1MsSUFBUyxFQUNULEVBQU8sRUFDUCxJQUFVLEVBQ1YsTUFBYyxFQUNkLE1BQWM7UUFKZCxTQUFJLEdBQUosSUFBSSxDQUFLO1FBQ1QsT0FBRSxHQUFGLEVBQUUsQ0FBSztRQUNQLFNBQUksR0FBSixJQUFJLENBQU07UUFDVixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsV0FBTSxHQUFOLE1BQU0sQ0FBUTtJQUNyQixDQUFDO0lBQ0wsa0JBQUM7QUFBRCxDQUFDLEFBUkQsSUFRQztBQVJZLGtDQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtEcmF3LCBVc2VyVHlwZX0gZnJvbSBcIi4vb3V0bGV0Lm1vZGVsXCI7XG5cbmV4cG9ydCBjb25zdCBhZG1pbkRyYXcgPSBbXG4gIHt2YWx1ZTogRHJhdy5EUkFXMSwgbGFiZWw6ICcxMS1hbSd9LFxuICB7dmFsdWU6IERyYXcuRFJBVzIsIGxhYmVsOiAnNC1wbSd9LFxuICB7dmFsdWU6IERyYXcuRFJBVzMsIGxhYmVsOiAnOS1wbSd9XG5dO1xuXG5cbmV4cG9ydCBjbGFzcyBSZXBvcnRTdW1tYXJ5e1xuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgdG90YWxTYWxlczogbnVtYmVyLFxuICAgIHB1YmxpYyB0b3RhbENhbmNlbGxlZFNhbGVzOiBudW1iZXIsXG4gICAgcHVibGljIHRvdGFsSGl0czogbnVtYmVyLFxuICAgIHB1YmxpYyBncm9zczogbnVtYmVyXG4gICl7IH1cbn1cblxuZXhwb3J0IGNsYXNzIEZvcm1Vc2VyIHtcbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIGxhc3RuYW1lOiBzdHJpbmcsXG4gICAgcHVibGljIGZpcnN0bmFtZTogc3RyaW5nLFxuICAgIHB1YmxpYyB1c2VyVHlwZTogVXNlclR5cGUsXG4gICAgcHVibGljIGJyYW5jaElkOiBzdHJpbmcsXG4gICAgcHVibGljIG91dGxldElkOiBzdHJpbmcsXG4gICAgcHVibGljIHVzZXJuYW1lPzogc3RyaW5nXG4gICl7fVxufVxuXG5leHBvcnQgY2xhc3MgRm9ybVN1bW1hcnkge1xuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgZnJvbTogYW55LFxuICAgIHB1YmxpYyB0bzogYW55LFxuICAgIHB1YmxpYyBkcmF3OiBEcmF3LFxuICAgIHB1YmxpYyBicmFuY2g6IHN0cmluZyxcbiAgICBwdWJsaWMgb3V0bGV0OiBzdHJpbmdcbiAgKXt9XG59XG4iXX0=