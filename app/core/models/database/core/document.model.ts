// import { v4 as uuid } from 'uuid';
import {OFFLINE_CONSTANTS} from "~/core/services/helpers/contants";
import {getUUID} from "nativescript-uuid";

export enum SystemType {
  TRANSACTION = 'transaction',
  BET = 'bet',
  BRANCH = 'branch',
  OUTLET = 'outlet',
  USER = 'user',
  SETTINGS = 'settings',
  SUMMARY = 'summary',
  OUTLET_REPORT = 'outletReport',
  WINNING_NUMBER = 'winningNumber'
}

export class Document {

  public _systemHeader: SystemHeader;

  constructor(systemType: SystemType) {

    if (!systemType) {
      throw new Error('No document type specified.');
    }

    this._systemHeader = new SystemHeader(systemType);

  }

}

class SystemHeader {

  public outletId: string;
  public branchId: string;
  public createdDate: number;
  public serverCreatedDate: number;
  public createdBy: string;

  public documentId: string;
  public versionId: string;
  public previousVersionId: string;
  public currentVersion: boolean;
  public deleted: boolean;

  constructor(public type: SystemType) {

    this.documentId = getUUID();
    this.versionId = getUUID();
    this.currentVersion = true;
    this.previousVersionId = null; // unmodified
    this.deleted = false;

    this.createdDate = Date.now();

    this.outletId = localStorage.getItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.OUTLET_ID);
    this.branchId = localStorage.getItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.BRANCH_ID);
    this.createdBy = localStorage.getItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.USER_ID);

  }

}
