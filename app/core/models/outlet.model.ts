import {Document, SystemType} from './database/core/document.model';

import {rambolito} from '../services/helpers/number-helper';
import {dateNoTime} from '../services/helpers/date-helper';

export enum Draw {
  ALL = 'ALL',
  DRAW1 = 'd1',
  DRAW2 = 'd2',
  DRAW3 = 'd3'
}

export enum Raffle {
  S3 = 's3',
  S3R = 's3r'
}

export enum UserType {
  ADMIN = 'admin',
  BRANCH = 'branch',
  OUTLET = 'outlet',
  ENCODER = 'encoder'
}

// helper class for Settings
export class RaffleLimit {
  // if from/to combination is negative, then it will apply to all possible combinations
  constructor(public raffle: Raffle, public limit: number, public from: number, public to?: number) {
    if (to) {
      from = from < to ? from : to;
      to = to > from ? to : from;
    }
  }
}

export class RafflePrize {
  constructor(public raffle: Raffle, public unitPrize: number) {}
}

// helper for transaction (stored in a transaction)
export class Bet {

  public id: string; // the index of this bet when stored inside the transaction.bets array

  constructor(
    public raffle: Raffle,
    public combination: number,
    public amount: number,
    public unitPrize?: number
  ) {

    if (!unitPrize) {
      this.unitPrize = 600; // win per peso for s3 and s3r
    }

  }

}

// helper for SummaryTrace (stored in SummaryTrace)
export class SummaryTraceDetail {
  constructor (public betId: string, public amount: number) {}
}

// helper class for Summary and MainSummary; contains transaction and bets
export class SummaryTrace {
  public cancelled = false;
  constructor(public transactionId: string, public details: SummaryTraceDetail[]) {}
}

export class ReportDetail {
  constructor(public transactionId: string, public raffle: Raffle, public combination: number, public amount: number){}
}

// The following are classes stored in the database.

export class User extends Document {

  public password: string; // encrypted password
  public username: string; // generated

    constructor
  (
    public lastname: string,
    public firstname: string,
    public userType: UserType,
    public branchId: string,
    public outletId: string
  ) {
    super(SystemType.USER); // call Document constructor
    this.username = this._systemHeader.documentId.split('-', 1)[0];

  }

}

export class Settings extends Document {

  public limits: RaffleLimit[] = [];
  public prizes: RafflePrize[] = [];

  constructor() {
    super(SystemType.SETTINGS);
  }

}

export class Branch extends Document {

  public limits: RaffleLimit[] = [];
  public prizes: RafflePrize[] = [];

  constructor(public name) {
    super(SystemType.BRANCH);
  }

}

export class Outlet extends Document {

  public limits: RaffleLimit[] = [];
  public prizes: RafflePrize[] = [];

  constructor(public branchId: string, public name) {
    super(SystemType.OUTLET);
  }

}

export class Transaction extends Document {

  public date: number;
  public transactionId: string;
  public total: number;
  public cancelled = false;

  constructor(public draw: Draw, public bets: Bet[]) {

    super(SystemType.TRANSACTION); // call Document constructor

    this.date = dateNoTime(new Date()).getTime();
    this.transactionId = this._systemHeader.documentId.split('-', 1)[0];

    // update total
    this.total = 0;
    for (let i = 0; i < this.bets.length; i++) {
      const bet = this.bets[i];
      bet.id = this.transactionId + '-' + i;
      this.total += bet.amount;
    }

  }

}

export class Summary extends Document {

  public combinationId: string;
  public trace: SummaryTrace[] = [];
  public amount = 0;
  public win = false;

  constructor(public date: number, public draw: Draw, public raffle: Raffle, public combination: number, public unitPrize?: number) {

    super(SystemType.SUMMARY);
    // unique identifier for this summary is the combination and raffle
    this.combinationId = Summary.combinationId(date, draw, raffle, combination);

    if (!unitPrize) {
      this.unitPrize = 600; // win per peso for s3 and s3r
    }

  }

  public static combinationId(date: number, draw: Draw, raffle: Raffle, combination: number) {
    return date + '-' + draw + '-' + raffle + '-' + combination;
  }
}

export class ReportSales {

  public details: ReportDetail[] = [];

  constructor(public total: number) {

  }

}

export class ReportHits {

  public details: ReportDetail[] = [];

  constructor(public total: number) {

  }

}

export class Report extends Document {

  public branchId: string;
  public outletId: string;

  public gross: number;

  constructor(public date: number, public draw: Draw, outlet: Outlet,
              public sales: ReportSales, public cancelled: ReportSales, public hits: ReportHits) {

    super(SystemType.OUTLET_REPORT);

    this.branchId = outlet.branchId;
    this.outletId = outlet._systemHeader.documentId;
    this.gross = sales.total - cancelled.total - hits.total;

  }
}

export class WinningNumber extends Document {

  public date: number;

  constructor(date: Date, public draw: Draw, public raffle: Raffle, public combination: number ) {

    super(SystemType.WINNING_NUMBER);

    this.date = dateNoTime(date).getTime();
    this._systemHeader.documentId = this.date + '-' + this.draw + '-' + this.raffle;

  }
}

export function createMockData() {
  // insert settings

  const settings = new Settings();
  settings.limits = [new RaffleLimit(Raffle.S3, 100, 0, 999)]; // limit for all outlets for s3 is 100 per combination


  // insert branches and outlets
  const delSur = new Branch('Davao del Sur');
  const ds1  = new Outlet(delSur._systemHeader.documentId, 'Digos 1');
  const ds2  = new Outlet(delSur._systemHeader.documentId, 'Digos 2');

  const davOcc = new Branch('Davao Occidental');
  const oc1  = new Outlet(davOcc._systemHeader.documentId, 'Malita 1');
  const oc2  = new Outlet(davOcc._systemHeader.documentId, 'Malita 2');

  // insert users

  // default super user
  const admin = new User('[DEFAULT-ADMIN]', '', UserType.ADMIN, null, null);

  // del sur users
  const delSurManager = new User('ALBOROTO', 'JOSE', UserType.BRANCH, delSur._systemHeader.documentId, null);
  const ds1Clerk = new User('RIZAL', 'JOSE', UserType.OUTLET, delSur._systemHeader.documentId, ds1._systemHeader.documentId);
  const ds2Clerk = new User('MAGDARO', 'FELONA', UserType.OUTLET, delSur._systemHeader.documentId, ds2._systemHeader.documentId);

  // occidental users
  const occidentalManager = new User('HASMIN', 'ADELLE', UserType.BRANCH, davOcc._systemHeader.documentId, null);
  const oc1Clerk = new User('ALPARATA', 'JOHN', UserType.OUTLET, davOcc._systemHeader.documentId, oc1._systemHeader.documentId);
  const oc2Clerk = new User('MAGDALENA', 'MARIA', UserType.OUTLET, davOcc._systemHeader.documentId, oc2._systemHeader.documentId);


  const date = dateNoTime(new Date()).getTime();

  // insert sample reports
  const ds1Report = new Report(date, Draw.DRAW1, ds1, new ReportSales(15000), new ReportSales(0), new ReportHits(600) );
  const ds2Report = new Report(date, Draw.DRAW1, ds2, new ReportSales(10000), new ReportSales(20), new ReportHits(0) );

  const oc1Report = new Report(date, Draw.DRAW1, oc1, new ReportSales(9000), new ReportSales(0), new ReportHits(6000) );
  const oc2Report = new Report(date, Draw.DRAW1, oc2, new ReportSales(20000), new ReportSales(0), new ReportHits(1200) );


  return [
    settings,
    delSur, ds1, ds2, davOcc, oc1, oc2,
    admin, delSurManager, ds1Clerk, ds2Clerk, occidentalManager, oc1Clerk, oc2Clerk,
    // ds1Report, ds2Report, oc1Report, oc2Report
  ];

}
