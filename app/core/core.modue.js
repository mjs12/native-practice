"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var http_1 = require("@angular/common/http");
var forms_1 = require("@angular/forms");
var router_1 = require("nativescript-angular/router");
var http_2 = require("nativescript-angular/http");
var http_client_1 = require("nativescript-angular/http-client");
var forms_2 = require("nativescript-angular/forms");
var api_service_1 = require("~/core/services/api.service");
var auth_guard_1 = require("~/core/services/auth.guard");
var CoreModule = /** @class */ (function () {
    function CoreModule() {
    }
    CoreModule_1 = CoreModule;
    CoreModule.forRoot = function () {
        return {
            ngModule: CoreModule_1,
            providers: [
                api_service_1.ApiService,
                auth_guard_1.AuthGuard
            ]
        };
    };
    CoreModule = CoreModule_1 = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                http_1.HttpClientModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                http_2.NativeScriptHttpModule,
                http_client_1.NativeScriptHttpClientModule,
                forms_2.NativeScriptFormsModule,
                router_1.NativeScriptRouterModule,
            ],
            exports: [
                http_1.HttpClientModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                http_2.NativeScriptHttpModule,
                http_client_1.NativeScriptHttpClientModule,
                forms_2.NativeScriptFormsModule,
                router_1.NativeScriptRouterModule
            ],
            declarations: []
        })
    ], CoreModule);
    return CoreModule;
    var CoreModule_1;
}());
exports.CoreModule = CoreModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5tb2R1ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNvcmUubW9kdWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBNEQ7QUFDNUQsMENBQTZDO0FBQzdDLDZDQUFzRDtBQUN0RCx3Q0FBZ0U7QUFDaEUsc0RBQXFFO0FBQ3JFLGtEQUFpRTtBQUNqRSxnRUFBOEU7QUFDOUUsb0RBQW1FO0FBQ25FLDJEQUF1RDtBQUN2RCx5REFBcUQ7QUF3QnJEO0lBQUE7SUFVQSxDQUFDO21CQVZZLFVBQVU7SUFDWixrQkFBTyxHQUFkO1FBQ0ksTUFBTSxDQUFDO1lBQ0gsUUFBUSxFQUFFLFlBQVU7WUFDcEIsU0FBUyxFQUFFO2dCQUNQLHdCQUFVO2dCQUNWLHNCQUFTO2FBQ1o7U0FDSixDQUFDO0lBQ04sQ0FBQztJQVRRLFVBQVU7UUF2QnRCLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRTtnQkFDTCxxQkFBWTtnQkFDWix1QkFBZ0I7Z0JBQ2hCLG1CQUFXO2dCQUNYLDJCQUFtQjtnQkFDbkIsNkJBQXNCO2dCQUN0QiwwQ0FBNEI7Z0JBQzVCLCtCQUF1QjtnQkFDdkIsaUNBQXdCO2FBQzNCO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLHVCQUFnQjtnQkFDaEIsbUJBQVc7Z0JBQ1gsMkJBQW1CO2dCQUNuQiw2QkFBc0I7Z0JBQ3RCLDBDQUE0QjtnQkFDNUIsK0JBQXVCO2dCQUN2QixpQ0FBd0I7YUFDM0I7WUFDRCxZQUFZLEVBQUUsRUFDYjtTQUNKLENBQUM7T0FDVyxVQUFVLENBVXRCO0lBQUQsaUJBQUM7O0NBQUEsQUFWRCxJQVVDO0FBVlksZ0NBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge01vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtIdHRwQ2xpZW50TW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQge0Zvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge05hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZX0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHtOYXRpdmVTY3JpcHRIdHRwTW9kdWxlfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvaHR0cFwiO1xuaW1wb3J0IHtOYXRpdmVTY3JpcHRIdHRwQ2xpZW50TW9kdWxlfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvaHR0cC1jbGllbnRcIjtcbmltcG9ydCB7TmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGV9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHtBcGlTZXJ2aWNlfSBmcm9tIFwifi9jb3JlL3NlcnZpY2VzL2FwaS5zZXJ2aWNlXCI7XG5pbXBvcnQge0F1dGhHdWFyZH0gZnJvbSBcIn4vY29yZS9zZXJ2aWNlcy9hdXRoLmd1YXJkXCI7XG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxuICAgICAgICBIdHRwQ2xpZW50TW9kdWxlLFxuICAgICAgICBGb3Jtc01vZHVsZSxcbiAgICAgICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcbiAgICAgICAgTmF0aXZlU2NyaXB0SHR0cE1vZHVsZSxcbiAgICAgICAgTmF0aXZlU2NyaXB0SHR0cENsaWVudE1vZHVsZSxcbiAgICAgICAgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUsXG4gICAgICAgIE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSxcbiAgICBdLFxuICAgIGV4cG9ydHM6IFtcbiAgICAgICAgSHR0cENsaWVudE1vZHVsZSxcbiAgICAgICAgRm9ybXNNb2R1bGUsXG4gICAgICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXG4gICAgICAgIE5hdGl2ZVNjcmlwdEh0dHBNb2R1bGUsXG4gICAgICAgIE5hdGl2ZVNjcmlwdEh0dHBDbGllbnRNb2R1bGUsXG4gICAgICAgIE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlLFxuICAgICAgICBOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGVcbiAgICBdLFxuICAgIGRlY2xhcmF0aW9uczogW1xuICAgIF1cbn0pXG5leHBvcnQgY2xhc3MgQ29yZU1vZHVsZSB7XG4gICAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBuZ01vZHVsZTogQ29yZU1vZHVsZSxcbiAgICAgICAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICAgICAgICAgIEFwaVNlcnZpY2UsXG4gICAgICAgICAgICAgICAgQXV0aEd1YXJkXG4gICAgICAgICAgICBdXG4gICAgICAgIH07XG4gICAgfVxufVxuIl19