
// https://stackoverflow.com/questions/1267283/how-can-i-pad-a-value-with-leading-zeros
// adds zero to number according to padlen
// var fu = paddy(14, 5); // 00014
// var bar = paddy(2, 4, '#'); // ###2
export function paddy(num, padlen?, padchar?) {
  const pad_char = typeof padchar !== 'undefined' ? padchar : '0';
  padlen = padlen ? padlen : 3;
  const pad = new Array(1 + padlen).join(pad_char);
  return (pad + num).slice(-pad.length); // returns a string
}


export function rambolito(number) {

  const c: string = paddy(number, 3); // combination

  // Possible combos for 3 digit: 012, 021, 102, 120, 201, 210

  if (c.length !== 3) {
    throw new Error('Rambolito does not apply with combination: ' + c);
  }

  const permutation: Set<string> = new Set([
    '' + c[0] + c[1] + c[2],
    '' + c[0] + c[2] + c[1],
    '' + c[1] + c[0] + c[2],
    '' + c[1] + c[2] + c[0],
    '' + c[2] + c[0] + c[1],
    '' + c[2] + c[1] + c[0]
  ]);

  return permutation;

}

export function rambolitoInt(combination){
  return (Array.from(rambolito(combination)).map(val => {return parseInt(val, 10)}));
}
