
// accepts date object
import {Raffle} from "../../models/outlet.model";
import {rambolito} from "./number-helper";

export function displayPrize(raffle: Raffle, combination: number, amount: number, unitPrize: number){
  if(raffle === Raffle.S3R){
    return (amount / Array.from(rambolito(combination)).length) * unitPrize;
  }
  return amount * unitPrize;
}
