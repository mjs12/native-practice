"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// accepts date object
function dateNoTime(timestamp) {
    // returns new Date object
    var result = new Date(timestamp);
    result.setHours(0, 0, 0, 0);
    return result;
}
exports.dateNoTime = dateNoTime;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1oZWxwZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkYXRlLWhlbHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHNCQUFzQjtBQUN0QixvQkFBMkIsU0FBUztJQUNsQywwQkFBMEI7SUFDMUIsSUFBTSxNQUFNLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDbkMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUM1QixNQUFNLENBQUMsTUFBTSxDQUFDO0FBQ2hCLENBQUM7QUFMRCxnQ0FLQyIsInNvdXJjZXNDb250ZW50IjpbIlxuLy8gYWNjZXB0cyBkYXRlIG9iamVjdFxuZXhwb3J0IGZ1bmN0aW9uIGRhdGVOb1RpbWUodGltZXN0YW1wKTogRGF0ZSB7XG4gIC8vIHJldHVybnMgbmV3IERhdGUgb2JqZWN0XG4gIGNvbnN0IHJlc3VsdCA9IG5ldyBEYXRlKHRpbWVzdGFtcCk7XG4gIHJlc3VsdC5zZXRIb3VycygwLCAwLCAwLCAwKTtcbiAgcmV0dXJuIHJlc3VsdDtcbn1cbiJdfQ==