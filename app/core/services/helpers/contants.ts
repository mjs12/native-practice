export const OFFLINE_CONSTANTS = {

  IDB_NAME_PREFIX : 'outl',
  IDB_VERSION : 1, // upgrading the version will delete the current database

  TABLES : {
    TRANSACTION : 'transaction',
    SUMMARY  : 'summary'
  },

  INDEXES : {
    COMBINATION_ID : 'combinationId',
    COMBINATION : 'combination',
    DOCUMENT_ID : 'documentId',
    TRANSACTION_ID : 'transactionId'
  },

  LOCAL_STORAGE : {
    ACTIVE_DRAW : 'activeDraw', // btoa('activeDraw')
    OUTLET_ID : 'outletId',
    BRANCH_ID : 'branchId',
    USER_ID : 'userId',
    USER_INFO: 'userInfo'
  }

};
