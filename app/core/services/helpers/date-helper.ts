
// accepts date object
export function dateNoTime(timestamp): Date {
  // returns new Date object
  const result = new Date(timestamp);
  result.setHours(0, 0, 0, 0);
  return result;
}
