import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {ApiService} from "./api.service";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(public apiService: ApiService, public router: Router){}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    const userTypes = next.data.userTypes;
    const loggedInUser = this.apiService.getLoggedUser();
    const allowed = loggedInUser && userTypes.indexOf(loggedInUser.userType)!= -1;

    if(!allowed){
      this.router.navigate(['login']);
    }

    return allowed;
  }

}
