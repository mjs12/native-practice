// <reference path="../../../../node_modules/rxjs/add/operator/catch.d.ts"/>
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/catch';
import {
  Transaction, Draw, Settings, User, Branch, Outlet, createMockData, WinningNumber, Bet, Raffle
} from '../models/outlet.model';
import {OfflineAPIService} from './offline/offline.api.service';
import {Observable} from 'rxjs/Observable';
import {OFFLINE_CONSTANTS} from "./helpers/contants";
import {SystemType} from "../models/database/core/document.model";
import "rxjs/add/operator/finally";
import {dateNoTime} from "./helpers/date-helper";
import {Router} from "@angular/router";

const API_URL = "https://dev.smiles89.com/api/v1";

@Injectable()
export class ApiService {

  constructor(private httpClient: HttpClient, private router: Router) {}

  resetPassword(userId, newPassword){
    return this.httpClient.post<User>(`${API_URL}/update/password`, {userId, newPassword});
  }

  login(username, password): Observable<User> {
    return this.httpClient.post<User>(`${API_URL}/login`, {username, password})
  }

  getLoggedUser(){
    return OfflineAPIService.getLoggedUser();
  }

  _login(user:User){
    return OfflineAPIService.login(user);
  }

  async logout() {
      await OfflineAPIService.logout();
      await this.router.navigate(['login']);
      return;
  }

  getSettings(): Observable<Settings> {
    // will always return only a single Settings object
    return this.httpClient.get<Settings>(`${API_URL}/get/settings`);
  }

  searchUser(keyword?: string): Observable<User[]> {
    // if userId is null then it will return all users
    return this.httpClient.post<User[]>(`${API_URL}/search/user`, {keyword})
  }

  getBranch(branchId: string): Observable<Branch[]> {
    // if branch is null then it will return all branch
    return this.httpClient.post<Branch[]>(`${API_URL}/get/branch`, {branchId});
  }

  getOutlet(branchId: string, outletId: string): Observable<Outlet[]> {
    // if branch is null then it will return all branch
    return this.httpClient.post<Outlet[]>(`${API_URL}/get/outlet`, {branchId, outletId});
  }

  getReport(dateFrom: Date, dateTo: Date, draw: Draw, branchId?: string, outletId?: string):Observable<any> {
    // if branchId and outletId is empty then it will return all reports
    return this.httpClient
      .post(`${API_URL}/get/report`, {dateFrom, dateTo, draw, branchId, outletId})
  }

  update(documentId: string, updates){
    return this.httpClient.post(`${API_URL}/update`, { documentId, updates })
  }

  insertDocument(document: any): Observable<any>{ // can be any type of type branch, user, outlet

    switch (document._systemHeader.type){
      case SystemType.USER:
      case SystemType.BRANCH:
      case SystemType.OUTLET:
        break;
      default:
        throw new Error('Inserting an invalid document type');
    }

    return this.httpClient.post<any>(`${API_URL}/insert`, { documents: [document] });
  }

  setWinningNumber(winningNumber: WinningNumber){
    return this.httpClient.post(`${API_URL}/set/winningNumber`, { winningNumber });
  }

  getWinningNumbers(date: Date, draw: string|Draw): Observable<WinningNumber[]> {
    return this.httpClient.post<WinningNumber[]>(`${API_URL}/get/winningNumbers`, { date, draw })
  }


  insertDrawDocuments(documents){
    return this.httpClient.post(`${API_URL}/insert`, { documents: documents });
  }

  addMockData() { // will insert data to database

    const mocks = createMockData();
    console.log(JSON.stringify(mocks));
    this.httpClient.post(`${API_URL}/insert`, { documents :  mocks })
      .subscribe((data) => {
        console.log(data);
      }, (err) => {
        console.log(err);
      });

  }

  checkLimit(draw: Draw, betting: Bet, bets: Bet[]){
    return OfflineAPIService.checkNewBet(draw, betting, bets);
  }

  addTransaction(transaction: Transaction): Promise<any> {
    return OfflineAPIService.addTransaction(transaction);
  }

  searchSummary(date: Date, draw: Draw, key?) { //key can be transaction number or combination number
    return OfflineAPIService.searchSummary(date, draw, key);
  }

  async closeDraw(draw: Draw, date?: Date) {
    return await OfflineAPIService.closeDraw(draw, date);
  }

  submitDraw(closeResult){
    return this.httpClient.post(`${API_URL}/insert`, { documents: closeResult.data.documents });
  }

  cancelTransaction(draw: Draw, transactionId: string) {
    return OfflineAPIService.cancelTransaction(draw, transactionId);
  }

  getTransaction(date: Date, draw: Draw, transactionId: string): Promise<Transaction[]>{ //empty transactionId will return all transactions

    if (!draw) {
      throw new Error('No draw specified.');
    }

    const dateMillis = dateNoTime(date).getTime();

    return OfflineAPIService.getTransaction(dateMillis, draw, transactionId);
  }
}
