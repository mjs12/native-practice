"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var api_service_1 = require("./api.service");
var AuthGuard = /** @class */ (function () {
    function AuthGuard(apiService, router) {
        this.apiService = apiService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        var userTypes = next.data.userTypes;
        var loggedInUser = this.apiService.getLoggedUser();
        var allowed = loggedInUser && userTypes.indexOf(loggedInUser.userType) != -1;
        if (!allowed) {
            this.router.navigate(['login']);
        }
        return allowed;
    };
    AuthGuard = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [api_service_1.ApiService, router_1.Router])
    ], AuthGuard);
    return AuthGuard;
}());
exports.AuthGuard = AuthGuard;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5ndWFyZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImF1dGguZ3VhcmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsMENBQWlHO0FBQ2pHLDZDQUF5QztBQUd6QztJQUVFLG1CQUFtQixVQUFzQixFQUFTLE1BQWM7UUFBN0MsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUFTLFdBQU0sR0FBTixNQUFNLENBQVE7SUFBRSxDQUFDO0lBRW5FLCtCQUFXLEdBQVgsVUFBWSxJQUE0QixFQUFFLEtBQTBCO1FBRWxFLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ3RDLElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckQsSUFBTSxPQUFPLEdBQUcsWUFBWSxJQUFJLFNBQVMsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFHLENBQUMsQ0FBQyxDQUFDO1FBRTlFLEVBQUUsQ0FBQSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUEsQ0FBQztZQUNYLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztRQUNsQyxDQUFDO1FBRUQsTUFBTSxDQUFDLE9BQU8sQ0FBQztJQUNqQixDQUFDO0lBZlUsU0FBUztRQURyQixpQkFBVSxFQUFFO3lDQUdvQix3QkFBVSxFQUFpQixlQUFNO09BRnJELFNBQVMsQ0FpQnJCO0lBQUQsZ0JBQUM7Q0FBQSxBQWpCRCxJQWlCQztBQWpCWSw4QkFBUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Q2FuQWN0aXZhdGUsIEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIFJvdXRlclN0YXRlU25hcHNob3QsIFJvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7QXBpU2VydmljZX0gZnJvbSBcIi4vYXBpLnNlcnZpY2VcIjtcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEF1dGhHdWFyZCBpbXBsZW1lbnRzIENhbkFjdGl2YXRlIHtcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgYXBpU2VydmljZTogQXBpU2VydmljZSwgcHVibGljIHJvdXRlcjogUm91dGVyKXt9XG5cbiAgY2FuQWN0aXZhdGUobmV4dDogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpOiBib29sZWFuIHtcblxuICAgIGNvbnN0IHVzZXJUeXBlcyA9IG5leHQuZGF0YS51c2VyVHlwZXM7XG4gICAgY29uc3QgbG9nZ2VkSW5Vc2VyID0gdGhpcy5hcGlTZXJ2aWNlLmdldExvZ2dlZFVzZXIoKTtcbiAgICBjb25zdCBhbGxvd2VkID0gbG9nZ2VkSW5Vc2VyICYmIHVzZXJUeXBlcy5pbmRleE9mKGxvZ2dlZEluVXNlci51c2VyVHlwZSkhPSAtMTtcblxuICAgIGlmKCFhbGxvd2VkKXtcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnbG9naW4nXSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGFsbG93ZWQ7XG4gIH1cblxufVxuIl19