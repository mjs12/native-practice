"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// <reference path="../../../../node_modules/rxjs/add/operator/catch.d.ts"/>
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
require("rxjs/add/operator/catch");
var outlet_model_1 = require("../models/outlet.model");
var offline_api_service_1 = require("./offline/offline.api.service");
var document_model_1 = require("../models/database/core/document.model");
require("rxjs/add/operator/finally");
var date_helper_1 = require("./helpers/date-helper");
var router_1 = require("@angular/router");
var API_URL = "https://dev.smiles89.com/api/v1";
var ApiService = /** @class */ (function () {
    function ApiService(httpClient, router) {
        this.httpClient = httpClient;
        this.router = router;
    }
    ApiService.prototype.resetPassword = function (userId, newPassword) {
        return this.httpClient.post(API_URL + "/update/password", { userId: userId, newPassword: newPassword });
    };
    ApiService.prototype.login = function (username, password) {
        return this.httpClient.post(API_URL + "/login", { username: username, password: password });
    };
    ApiService.prototype.getLoggedUser = function () {
        return offline_api_service_1.OfflineAPIService.getLoggedUser();
    };
    ApiService.prototype._login = function (user) {
        return offline_api_service_1.OfflineAPIService.login(user);
    };
    ApiService.prototype.logout = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, offline_api_service_1.OfflineAPIService.logout()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.router.navigate(['login'])];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ApiService.prototype.getSettings = function () {
        // will always return only a single Settings object
        return this.httpClient.get(API_URL + "/get/settings");
    };
    ApiService.prototype.searchUser = function (keyword) {
        // if userId is null then it will return all users
        return this.httpClient.post(API_URL + "/search/user", { keyword: keyword });
    };
    ApiService.prototype.getBranch = function (branchId) {
        // if branch is null then it will return all branch
        return this.httpClient.post(API_URL + "/get/branch", { branchId: branchId });
    };
    ApiService.prototype.getOutlet = function (branchId, outletId) {
        // if branch is null then it will return all branch
        return this.httpClient.post(API_URL + "/get/outlet", { branchId: branchId, outletId: outletId });
    };
    ApiService.prototype.getReport = function (dateFrom, dateTo, draw, branchId, outletId) {
        // if branchId and outletId is empty then it will return all reports
        return this.httpClient
            .post(API_URL + "/get/report", { dateFrom: dateFrom, dateTo: dateTo, draw: draw, branchId: branchId, outletId: outletId });
    };
    ApiService.prototype.update = function (documentId, updates) {
        return this.httpClient.post(API_URL + "/update", { documentId: documentId, updates: updates });
    };
    ApiService.prototype.insertDocument = function (document) {
        switch (document._systemHeader.type) {
            case document_model_1.SystemType.USER:
            case document_model_1.SystemType.BRANCH:
            case document_model_1.SystemType.OUTLET:
                break;
            default:
                throw new Error('Inserting an invalid document type');
        }
        return this.httpClient.post(API_URL + "/insert", { documents: [document] });
    };
    ApiService.prototype.setWinningNumber = function (winningNumber) {
        return this.httpClient.post(API_URL + "/set/winningNumber", { winningNumber: winningNumber });
    };
    ApiService.prototype.getWinningNumbers = function (date, draw) {
        return this.httpClient.post(API_URL + "/get/winningNumbers", { date: date, draw: draw });
    };
    ApiService.prototype.insertDrawDocuments = function (documents) {
        return this.httpClient.post(API_URL + "/insert", { documents: documents });
    };
    ApiService.prototype.addMockData = function () {
        var mocks = outlet_model_1.createMockData();
        console.log(JSON.stringify(mocks));
        this.httpClient.post(API_URL + "/insert", { documents: mocks })
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.checkLimit = function (draw, betting, bets) {
        return offline_api_service_1.OfflineAPIService.checkNewBet(draw, betting, bets);
    };
    ApiService.prototype.addTransaction = function (transaction) {
        return offline_api_service_1.OfflineAPIService.addTransaction(transaction);
    };
    ApiService.prototype.searchSummary = function (date, draw, key) {
        return offline_api_service_1.OfflineAPIService.searchSummary(date, draw, key);
    };
    ApiService.prototype.closeDraw = function (draw, date) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, offline_api_service_1.OfflineAPIService.closeDraw(draw, date)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ApiService.prototype.submitDraw = function (closeResult) {
        return this.httpClient.post(API_URL + "/insert", { documents: closeResult.data.documents });
    };
    ApiService.prototype.cancelTransaction = function (draw, transactionId) {
        return offline_api_service_1.OfflineAPIService.cancelTransaction(draw, transactionId);
    };
    ApiService.prototype.getTransaction = function (date, draw, transactionId) {
        if (!draw) {
            throw new Error('No draw specified.');
        }
        var dateMillis = date_helper_1.dateNoTime(date).getTime();
        return offline_api_service_1.OfflineAPIService.getTransaction(dateMillis, draw, transactionId);
    };
    ApiService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, router_1.Router])
    ], ApiService);
    return ApiService;
}());
exports.ApiService = ApiService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBpLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhcGkuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDRFQUE0RTtBQUM1RSxzQ0FBMkM7QUFDM0MsNkNBQWdEO0FBQ2hELG1DQUFpQztBQUNqQyx1REFFZ0M7QUFDaEMscUVBQWdFO0FBR2hFLHlFQUFrRTtBQUNsRSxxQ0FBbUM7QUFDbkMscURBQWlEO0FBQ2pELDBDQUF1QztBQUV2QyxJQUFNLE9BQU8sR0FBRyxpQ0FBaUMsQ0FBQztBQUdsRDtJQUVFLG9CQUFvQixVQUFzQixFQUFVLE1BQWM7UUFBOUMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUFVLFdBQU0sR0FBTixNQUFNLENBQVE7SUFBRyxDQUFDO0lBRXRFLGtDQUFhLEdBQWIsVUFBYyxNQUFNLEVBQUUsV0FBVztRQUMvQixNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQVUsT0FBTyxxQkFBa0IsRUFBRSxFQUFDLE1BQU0sUUFBQSxFQUFFLFdBQVcsYUFBQSxFQUFDLENBQUMsQ0FBQztJQUN6RixDQUFDO0lBRUQsMEJBQUssR0FBTCxVQUFNLFFBQVEsRUFBRSxRQUFRO1FBQ3RCLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBVSxPQUFPLFdBQVEsRUFBRSxFQUFDLFFBQVEsVUFBQSxFQUFFLFFBQVEsVUFBQSxFQUFDLENBQUMsQ0FBQTtJQUM3RSxDQUFDO0lBRUQsa0NBQWEsR0FBYjtRQUNFLE1BQU0sQ0FBQyx1Q0FBaUIsQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUMzQyxDQUFDO0lBRUQsMkJBQU0sR0FBTixVQUFPLElBQVM7UUFDZCxNQUFNLENBQUMsdUNBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFSywyQkFBTSxHQUFaOzs7OzRCQUNJLHFCQUFNLHVDQUFpQixDQUFDLE1BQU0sRUFBRSxFQUFBOzt3QkFBaEMsU0FBZ0MsQ0FBQzt3QkFDakMscUJBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFBOzt3QkFBckMsU0FBcUMsQ0FBQzt3QkFDdEMsc0JBQU87Ozs7S0FDVjtJQUVELGdDQUFXLEdBQVg7UUFDRSxtREFBbUQ7UUFDbkQsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFjLE9BQU8sa0JBQWUsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFRCwrQkFBVSxHQUFWLFVBQVcsT0FBZ0I7UUFDekIsa0RBQWtEO1FBQ2xELE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBWSxPQUFPLGlCQUFjLEVBQUUsRUFBQyxPQUFPLFNBQUEsRUFBQyxDQUFDLENBQUE7SUFDMUUsQ0FBQztJQUVELDhCQUFTLEdBQVQsVUFBVSxRQUFnQjtRQUN4QixtREFBbUQ7UUFDbkQsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFjLE9BQU8sZ0JBQWEsRUFBRSxFQUFDLFFBQVEsVUFBQSxFQUFDLENBQUMsQ0FBQztJQUM3RSxDQUFDO0lBRUQsOEJBQVMsR0FBVCxVQUFVLFFBQWdCLEVBQUUsUUFBZ0I7UUFDMUMsbURBQW1EO1FBQ25ELE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBYyxPQUFPLGdCQUFhLEVBQUUsRUFBQyxRQUFRLFVBQUEsRUFBRSxRQUFRLFVBQUEsRUFBQyxDQUFDLENBQUM7SUFDdkYsQ0FBQztJQUVELDhCQUFTLEdBQVQsVUFBVSxRQUFjLEVBQUUsTUFBWSxFQUFFLElBQVUsRUFBRSxRQUFpQixFQUFFLFFBQWlCO1FBQ3RGLG9FQUFvRTtRQUNwRSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVU7YUFDbkIsSUFBSSxDQUFJLE9BQU8sZ0JBQWEsRUFBRSxFQUFDLFFBQVEsVUFBQSxFQUFFLE1BQU0sUUFBQSxFQUFFLElBQUksTUFBQSxFQUFFLFFBQVEsVUFBQSxFQUFFLFFBQVEsVUFBQSxFQUFDLENBQUMsQ0FBQTtJQUNoRixDQUFDO0lBRUQsMkJBQU0sR0FBTixVQUFPLFVBQWtCLEVBQUUsT0FBTztRQUNoQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUksT0FBTyxZQUFTLEVBQUUsRUFBRSxVQUFVLFlBQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUE7SUFDM0UsQ0FBQztJQUVELG1DQUFjLEdBQWQsVUFBZSxRQUFhO1FBRTFCLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUEsQ0FBQztZQUNuQyxLQUFLLDJCQUFVLENBQUMsSUFBSSxDQUFDO1lBQ3JCLEtBQUssMkJBQVUsQ0FBQyxNQUFNLENBQUM7WUFDdkIsS0FBSywyQkFBVSxDQUFDLE1BQU07Z0JBQ3BCLEtBQUssQ0FBQztZQUNSO2dCQUNFLE1BQU0sSUFBSSxLQUFLLENBQUMsb0NBQW9DLENBQUMsQ0FBQztRQUMxRCxDQUFDO1FBRUQsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFTLE9BQU8sWUFBUyxFQUFFLEVBQUUsU0FBUyxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ25GLENBQUM7SUFFRCxxQ0FBZ0IsR0FBaEIsVUFBaUIsYUFBNEI7UUFDM0MsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFJLE9BQU8sdUJBQW9CLEVBQUUsRUFBRSxhQUFhLGVBQUEsRUFBRSxDQUFDLENBQUM7SUFDakYsQ0FBQztJQUVELHNDQUFpQixHQUFqQixVQUFrQixJQUFVLEVBQUUsSUFBaUI7UUFDN0MsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFxQixPQUFPLHdCQUFxQixFQUFFLEVBQUUsSUFBSSxNQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFBO0lBQy9GLENBQUM7SUFHRCx3Q0FBbUIsR0FBbkIsVUFBb0IsU0FBUztRQUMzQixNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUksT0FBTyxZQUFTLEVBQUUsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUM3RSxDQUFDO0lBRUQsZ0NBQVcsR0FBWDtRQUVFLElBQU0sS0FBSyxHQUFHLDZCQUFjLEVBQUUsQ0FBQztRQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBSSxPQUFPLFlBQVMsRUFBRSxFQUFFLFNBQVMsRUFBSSxLQUFLLEVBQUUsQ0FBQzthQUM5RCxTQUFTLENBQUMsVUFBQyxJQUFJO1lBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQixDQUFDLEVBQUUsVUFBQyxHQUFHO1lBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNuQixDQUFDLENBQUMsQ0FBQztJQUVQLENBQUM7SUFFRCwrQkFBVSxHQUFWLFVBQVcsSUFBVSxFQUFFLE9BQVksRUFBRSxJQUFXO1FBQzlDLE1BQU0sQ0FBQyx1Q0FBaUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM1RCxDQUFDO0lBRUQsbUNBQWMsR0FBZCxVQUFlLFdBQXdCO1FBQ3JDLE1BQU0sQ0FBQyx1Q0FBaUIsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVELGtDQUFhLEdBQWIsVUFBYyxJQUFVLEVBQUUsSUFBVSxFQUFFLEdBQUk7UUFDeEMsTUFBTSxDQUFDLHVDQUFpQixDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFFSyw4QkFBUyxHQUFmLFVBQWdCLElBQVUsRUFBRSxJQUFXOzs7OzRCQUM5QixxQkFBTSx1Q0FBaUIsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFBOzRCQUFwRCxzQkFBTyxTQUE2QyxFQUFDOzs7O0tBQ3REO0lBRUQsK0JBQVUsR0FBVixVQUFXLFdBQVc7UUFDcEIsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFJLE9BQU8sWUFBUyxFQUFFLEVBQUUsU0FBUyxFQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQztJQUM5RixDQUFDO0lBRUQsc0NBQWlCLEdBQWpCLFVBQWtCLElBQVUsRUFBRSxhQUFxQjtRQUNqRCxNQUFNLENBQUMsdUNBQWlCLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFRCxtQ0FBYyxHQUFkLFVBQWUsSUFBVSxFQUFFLElBQVUsRUFBRSxhQUFxQjtRQUUxRCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDVixNQUFNLElBQUksS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDeEMsQ0FBQztRQUVELElBQU0sVUFBVSxHQUFHLHdCQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7UUFFOUMsTUFBTSxDQUFDLHVDQUFpQixDQUFDLGNBQWMsQ0FBQyxVQUFVLEVBQUUsSUFBSSxFQUFFLGFBQWEsQ0FBQyxDQUFDO0lBQzNFLENBQUM7SUFqSVUsVUFBVTtRQUR0QixpQkFBVSxFQUFFO3lDQUdxQixpQkFBVSxFQUFrQixlQUFNO09BRnZELFVBQVUsQ0FrSXRCO0lBQUQsaUJBQUM7Q0FBQSxBQWxJRCxJQWtJQztBQWxJWSxnQ0FBVSIsInNvdXJjZXNDb250ZW50IjpbIi8vIDxyZWZlcmVuY2UgcGF0aD1cIi4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9yeGpzL2FkZC9vcGVyYXRvci9jYXRjaC5kLnRzXCIvPlxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtIdHRwQ2xpZW50fSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL2NhdGNoJztcbmltcG9ydCB7XG4gIFRyYW5zYWN0aW9uLCBEcmF3LCBTZXR0aW5ncywgVXNlciwgQnJhbmNoLCBPdXRsZXQsIGNyZWF0ZU1vY2tEYXRhLCBXaW5uaW5nTnVtYmVyLCBCZXQsIFJhZmZsZVxufSBmcm9tICcuLi9tb2RlbHMvb3V0bGV0Lm1vZGVsJztcbmltcG9ydCB7T2ZmbGluZUFQSVNlcnZpY2V9IGZyb20gJy4vb2ZmbGluZS9vZmZsaW5lLmFwaS5zZXJ2aWNlJztcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcy9PYnNlcnZhYmxlJztcbmltcG9ydCB7T0ZGTElORV9DT05TVEFOVFN9IGZyb20gXCIuL2hlbHBlcnMvY29udGFudHNcIjtcbmltcG9ydCB7U3lzdGVtVHlwZX0gZnJvbSBcIi4uL21vZGVscy9kYXRhYmFzZS9jb3JlL2RvY3VtZW50Lm1vZGVsXCI7XG5pbXBvcnQgXCJyeGpzL2FkZC9vcGVyYXRvci9maW5hbGx5XCI7XG5pbXBvcnQge2RhdGVOb1RpbWV9IGZyb20gXCIuL2hlbHBlcnMvZGF0ZS1oZWxwZXJcIjtcbmltcG9ydCB7Um91dGVyfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5cbmNvbnN0IEFQSV9VUkwgPSBcImh0dHBzOi8vZGV2LnNtaWxlczg5LmNvbS9hcGkvdjFcIjtcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEFwaVNlcnZpY2Uge1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cENsaWVudDogSHR0cENsaWVudCwgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge31cblxuICByZXNldFBhc3N3b3JkKHVzZXJJZCwgbmV3UGFzc3dvcmQpe1xuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdDxVc2VyPihgJHtBUElfVVJMfS91cGRhdGUvcGFzc3dvcmRgLCB7dXNlcklkLCBuZXdQYXNzd29yZH0pO1xuICB9XG5cbiAgbG9naW4odXNlcm5hbWUsIHBhc3N3b3JkKTogT2JzZXJ2YWJsZTxVc2VyPiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0PFVzZXI+KGAke0FQSV9VUkx9L2xvZ2luYCwge3VzZXJuYW1lLCBwYXNzd29yZH0pXG4gIH1cblxuICBnZXRMb2dnZWRVc2VyKCl7XG4gICAgcmV0dXJuIE9mZmxpbmVBUElTZXJ2aWNlLmdldExvZ2dlZFVzZXIoKTtcbiAgfVxuXG4gIF9sb2dpbih1c2VyOlVzZXIpe1xuICAgIHJldHVybiBPZmZsaW5lQVBJU2VydmljZS5sb2dpbih1c2VyKTtcbiAgfVxuXG4gIGFzeW5jIGxvZ291dCgpIHtcbiAgICAgIGF3YWl0IE9mZmxpbmVBUElTZXJ2aWNlLmxvZ291dCgpO1xuICAgICAgYXdhaXQgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWydsb2dpbiddKTtcbiAgICAgIHJldHVybjtcbiAgfVxuXG4gIGdldFNldHRpbmdzKCk6IE9ic2VydmFibGU8U2V0dGluZ3M+IHtcbiAgICAvLyB3aWxsIGFsd2F5cyByZXR1cm4gb25seSBhIHNpbmdsZSBTZXR0aW5ncyBvYmplY3RcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxTZXR0aW5ncz4oYCR7QVBJX1VSTH0vZ2V0L3NldHRpbmdzYCk7XG4gIH1cblxuICBzZWFyY2hVc2VyKGtleXdvcmQ/OiBzdHJpbmcpOiBPYnNlcnZhYmxlPFVzZXJbXT4ge1xuICAgIC8vIGlmIHVzZXJJZCBpcyBudWxsIHRoZW4gaXQgd2lsbCByZXR1cm4gYWxsIHVzZXJzXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0PFVzZXJbXT4oYCR7QVBJX1VSTH0vc2VhcmNoL3VzZXJgLCB7a2V5d29yZH0pXG4gIH1cblxuICBnZXRCcmFuY2goYnJhbmNoSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8QnJhbmNoW10+IHtcbiAgICAvLyBpZiBicmFuY2ggaXMgbnVsbCB0aGVuIGl0IHdpbGwgcmV0dXJuIGFsbCBicmFuY2hcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3Q8QnJhbmNoW10+KGAke0FQSV9VUkx9L2dldC9icmFuY2hgLCB7YnJhbmNoSWR9KTtcbiAgfVxuXG4gIGdldE91dGxldChicmFuY2hJZDogc3RyaW5nLCBvdXRsZXRJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxPdXRsZXRbXT4ge1xuICAgIC8vIGlmIGJyYW5jaCBpcyBudWxsIHRoZW4gaXQgd2lsbCByZXR1cm4gYWxsIGJyYW5jaFxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdDxPdXRsZXRbXT4oYCR7QVBJX1VSTH0vZ2V0L291dGxldGAsIHticmFuY2hJZCwgb3V0bGV0SWR9KTtcbiAgfVxuXG4gIGdldFJlcG9ydChkYXRlRnJvbTogRGF0ZSwgZGF0ZVRvOiBEYXRlLCBkcmF3OiBEcmF3LCBicmFuY2hJZD86IHN0cmluZywgb3V0bGV0SWQ/OiBzdHJpbmcpOk9ic2VydmFibGU8YW55PiB7XG4gICAgLy8gaWYgYnJhbmNoSWQgYW5kIG91dGxldElkIGlzIGVtcHR5IHRoZW4gaXQgd2lsbCByZXR1cm4gYWxsIHJlcG9ydHNcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50XG4gICAgICAucG9zdChgJHtBUElfVVJMfS9nZXQvcmVwb3J0YCwge2RhdGVGcm9tLCBkYXRlVG8sIGRyYXcsIGJyYW5jaElkLCBvdXRsZXRJZH0pXG4gIH1cblxuICB1cGRhdGUoZG9jdW1lbnRJZDogc3RyaW5nLCB1cGRhdGVzKXtcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QoYCR7QVBJX1VSTH0vdXBkYXRlYCwgeyBkb2N1bWVudElkLCB1cGRhdGVzIH0pXG4gIH1cblxuICBpbnNlcnREb2N1bWVudChkb2N1bWVudDogYW55KTogT2JzZXJ2YWJsZTxhbnk+eyAvLyBjYW4gYmUgYW55IHR5cGUgb2YgdHlwZSBicmFuY2gsIHVzZXIsIG91dGxldFxuXG4gICAgc3dpdGNoIChkb2N1bWVudC5fc3lzdGVtSGVhZGVyLnR5cGUpe1xuICAgICAgY2FzZSBTeXN0ZW1UeXBlLlVTRVI6XG4gICAgICBjYXNlIFN5c3RlbVR5cGUuQlJBTkNIOlxuICAgICAgY2FzZSBTeXN0ZW1UeXBlLk9VVExFVDpcbiAgICAgICAgYnJlYWs7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0luc2VydGluZyBhbiBpbnZhbGlkIGRvY3VtZW50IHR5cGUnKTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3Q8YW55PihgJHtBUElfVVJMfS9pbnNlcnRgLCB7IGRvY3VtZW50czogW2RvY3VtZW50XSB9KTtcbiAgfVxuXG4gIHNldFdpbm5pbmdOdW1iZXIod2lubmluZ051bWJlcjogV2lubmluZ051bWJlcil7XG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KGAke0FQSV9VUkx9L3NldC93aW5uaW5nTnVtYmVyYCwgeyB3aW5uaW5nTnVtYmVyIH0pO1xuICB9XG5cbiAgZ2V0V2lubmluZ051bWJlcnMoZGF0ZTogRGF0ZSwgZHJhdzogc3RyaW5nfERyYXcpOiBPYnNlcnZhYmxlPFdpbm5pbmdOdW1iZXJbXT4ge1xuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdDxXaW5uaW5nTnVtYmVyW10+KGAke0FQSV9VUkx9L2dldC93aW5uaW5nTnVtYmVyc2AsIHsgZGF0ZSwgZHJhdyB9KVxuICB9XG5cblxuICBpbnNlcnREcmF3RG9jdW1lbnRzKGRvY3VtZW50cyl7XG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KGAke0FQSV9VUkx9L2luc2VydGAsIHsgZG9jdW1lbnRzOiBkb2N1bWVudHMgfSk7XG4gIH1cblxuICBhZGRNb2NrRGF0YSgpIHsgLy8gd2lsbCBpbnNlcnQgZGF0YSB0byBkYXRhYmFzZVxuXG4gICAgY29uc3QgbW9ja3MgPSBjcmVhdGVNb2NrRGF0YSgpO1xuICAgIGNvbnNvbGUubG9nKEpTT04uc3RyaW5naWZ5KG1vY2tzKSk7XG4gICAgdGhpcy5odHRwQ2xpZW50LnBvc3QoYCR7QVBJX1VSTH0vaW5zZXJ0YCwgeyBkb2N1bWVudHMgOiAgbW9ja3MgfSlcbiAgICAgIC5zdWJzY3JpYmUoKGRhdGEpID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XG4gICAgICB9LCAoZXJyKSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKGVycik7XG4gICAgICB9KTtcblxuICB9XG5cbiAgY2hlY2tMaW1pdChkcmF3OiBEcmF3LCBiZXR0aW5nOiBCZXQsIGJldHM6IEJldFtdKXtcbiAgICByZXR1cm4gT2ZmbGluZUFQSVNlcnZpY2UuY2hlY2tOZXdCZXQoZHJhdywgYmV0dGluZywgYmV0cyk7XG4gIH1cblxuICBhZGRUcmFuc2FjdGlvbih0cmFuc2FjdGlvbjogVHJhbnNhY3Rpb24pOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiBPZmZsaW5lQVBJU2VydmljZS5hZGRUcmFuc2FjdGlvbih0cmFuc2FjdGlvbik7XG4gIH1cblxuICBzZWFyY2hTdW1tYXJ5KGRhdGU6IERhdGUsIGRyYXc6IERyYXcsIGtleT8pIHsgLy9rZXkgY2FuIGJlIHRyYW5zYWN0aW9uIG51bWJlciBvciBjb21iaW5hdGlvbiBudW1iZXJcbiAgICByZXR1cm4gT2ZmbGluZUFQSVNlcnZpY2Uuc2VhcmNoU3VtbWFyeShkYXRlLCBkcmF3LCBrZXkpO1xuICB9XG5cbiAgYXN5bmMgY2xvc2VEcmF3KGRyYXc6IERyYXcsIGRhdGU/OiBEYXRlKSB7XG4gICAgcmV0dXJuIGF3YWl0IE9mZmxpbmVBUElTZXJ2aWNlLmNsb3NlRHJhdyhkcmF3LCBkYXRlKTtcbiAgfVxuXG4gIHN1Ym1pdERyYXcoY2xvc2VSZXN1bHQpe1xuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdChgJHtBUElfVVJMfS9pbnNlcnRgLCB7IGRvY3VtZW50czogY2xvc2VSZXN1bHQuZGF0YS5kb2N1bWVudHMgfSk7XG4gIH1cblxuICBjYW5jZWxUcmFuc2FjdGlvbihkcmF3OiBEcmF3LCB0cmFuc2FjdGlvbklkOiBzdHJpbmcpIHtcbiAgICByZXR1cm4gT2ZmbGluZUFQSVNlcnZpY2UuY2FuY2VsVHJhbnNhY3Rpb24oZHJhdywgdHJhbnNhY3Rpb25JZCk7XG4gIH1cblxuICBnZXRUcmFuc2FjdGlvbihkYXRlOiBEYXRlLCBkcmF3OiBEcmF3LCB0cmFuc2FjdGlvbklkOiBzdHJpbmcpOiBQcm9taXNlPFRyYW5zYWN0aW9uW10+eyAvL2VtcHR5IHRyYW5zYWN0aW9uSWQgd2lsbCByZXR1cm4gYWxsIHRyYW5zYWN0aW9uc1xuXG4gICAgaWYgKCFkcmF3KSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ05vIGRyYXcgc3BlY2lmaWVkLicpO1xuICAgIH1cblxuICAgIGNvbnN0IGRhdGVNaWxsaXMgPSBkYXRlTm9UaW1lKGRhdGUpLmdldFRpbWUoKTtcblxuICAgIHJldHVybiBPZmZsaW5lQVBJU2VydmljZS5nZXRUcmFuc2FjdGlvbihkYXRlTWlsbGlzLCBkcmF3LCB0cmFuc2FjdGlvbklkKTtcbiAgfVxufVxuIl19