
export class IdbTable {
  constructor(public name: string, public param: IdbTableParam, public indexes: IdbIndex[]) {
  }
}

export class IdbTableParam {
  constructor(public keyPath: string, public autoIncrement: boolean) {
  }
}

export class IdbIndex {
  constructor(public name: string, public path: string, public unique: boolean) {
  }
}

export class OfflineDatabase {

  private database;

  private static put(database, tblName: string, record: any) {

    return new Promise((resolve, reject) => {

      const transaction = database.transaction([tblName], 'readwrite');
      const request = transaction.objectStore(tblName).put(record);

      request.onsuccess = function (e) {
        resolve(true);
      };

      request.onerror = function (e) {
        reject(e);
      };

    });

  }

  private static fetch(database, tblName: string, indexName?: string, searchValue?: IDBKeyRange): Promise<any> {

    return new Promise((resolve, reject) => {

      const transaction = database.transaction([tblName], 'readwrite');
      let store = transaction.objectStore(tblName);
      // if no indexName and searchValue, return everything
      if (indexName && searchValue) {
        store = store.index(indexName);
      }

      store = store.openCursor(searchValue);

      const docs = [];

      store.onsuccess = function (event) {

        const cursor = event.target.result;

        if (cursor == null) {
          resolve(docs);
          return;
        }

        docs.push(cursor.value);
        cursor.continue();
      };

      store.onerror = function (e) {
        console.log('An error occured on idbservice getEntries.');
        reject(e);
      };

    });
  }

  private static clearTable(database, tblName: string) {

    return new Promise((resolve, reject) => {

      const transaction = database.transaction([tblName], 'readwrite');
      const request = transaction.objectStore(tblName).clear();

      request.onsuccess = function (e) {
        resolve(true);
      };

      request.onerror = function (e) {
        reject(e);
      };

    });

  }

  private static init(dbName, version, tables) {

    return new Promise((resolve, reject) => {

      const dbRequest = indexedDB.open(dbName, version);

      dbRequest.onblocked = function(event) {
        console.log('IDB Event: indexedDB blocked.');
      };

      dbRequest.onerror = function (event) {
        console.log('IDB Event: indexedDB error.');
        reject(new Error('Error in database.'));
      };

      dbRequest.onsuccess = function (event) {

        console.log('IDB Event: indexedDB opened: ' + dbName);

        const database = dbRequest.result;

        database.onclose = function (evt) {
          alert('Offline database closed: ' + database.name);
        };

        database.onabort = function (evt) {
          alert('IndexedDB abort occured: ' + evt.target.error.message);
        };

        database.onversionchange = function (evt) { // another tab might have upgraded the database
          console.log('IndexedDB Event: database version changed.');
        };

        database.onerror = function(evt) {
          alert('IDBDatabase error occurred: ' + evt.target.error.message);
        };

        resolve(database);

      };

      dbRequest.onupgradeneeded = function(event) {

        const database = dbRequest.result;

        tables.forEach((table) => {

          if (database.objectStoreNames.contains(table.name)) {
            database.deleteObjectStore(table.name);
          }

          const store = database.createObjectStore(table.name, table.param);
          table.indexes.forEach((index) => {
            if (!store.indexNames.contains(index.name)) {
              store.createIndex(index.name, index.path, {unique: index.unique});
            }
          });

        });

      };

    });

  }

  constructor(private databaseName, private _offlineTables: IdbTable[], private _idbVersion: number) {}

  private async getDatabase(): Promise<any> {

    if (!this.database) {
      this.database = await OfflineDatabase.init(this.databaseName, this._idbVersion, this._offlineTables);
    }

    return this.database;

  }

  public async clearDatabase(): Promise<any> {

    const database = await this.getDatabase();

    const promises: Promise<any>[] = [];

    this._offlineTables.forEach((table: IdbTable) => {

      promises.push(OfflineDatabase.clearTable(database, table.name));

    });

    const results = await Promise.all(promises);

    const dbName = this.databaseName;

    results.forEach((success) => {

      if (!success) {
        throw new Error('Failed to clear database database  ' + dbName + ': A table failed to get cleared.');
      }

    });

    database.close();

    this.database = null;

    return true;
  }

  public async saveEntry(tblName: string, record: any) {

    const database = await this.getDatabase();

    const result = await OfflineDatabase.put(database, tblName, record);

    return result;

  }

  public async getEntries(tblName: string, indexName?: string, searchValue?: IDBKeyRange): Promise<any> {

    const database = await this.getDatabase();

    const result = await OfflineDatabase.fetch(database, tblName, indexName, searchValue);

    return result;

  }

  public async put(tableName, record) {

    const database = await this.getDatabase();

    const result = await OfflineDatabase.put(database, tableName, record);

    return result;
  }

}
