"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IdbTable = /** @class */ (function () {
    function IdbTable(name, param, indexes) {
        this.name = name;
        this.param = param;
        this.indexes = indexes;
    }
    return IdbTable;
}());
exports.IdbTable = IdbTable;
var IdbTableParam = /** @class */ (function () {
    function IdbTableParam(keyPath, autoIncrement) {
        this.keyPath = keyPath;
        this.autoIncrement = autoIncrement;
    }
    return IdbTableParam;
}());
exports.IdbTableParam = IdbTableParam;
var IdbIndex = /** @class */ (function () {
    function IdbIndex(name, path, unique) {
        this.name = name;
        this.path = path;
        this.unique = unique;
    }
    return IdbIndex;
}());
exports.IdbIndex = IdbIndex;
var OfflineDatabase = /** @class */ (function () {
    function OfflineDatabase(databaseName, _offlineTables, _idbVersion) {
        this.databaseName = databaseName;
        this._offlineTables = _offlineTables;
        this._idbVersion = _idbVersion;
    }
    OfflineDatabase.put = function (database, tblName, record) {
        return new Promise(function (resolve, reject) {
            var transaction = database.transaction([tblName], 'readwrite');
            var request = transaction.objectStore(tblName).put(record);
            request.onsuccess = function (e) {
                resolve(true);
            };
            request.onerror = function (e) {
                reject(e);
            };
        });
    };
    OfflineDatabase.fetch = function (database, tblName, indexName, searchValue) {
        return new Promise(function (resolve, reject) {
            var transaction = database.transaction([tblName], 'readwrite');
            var store = transaction.objectStore(tblName);
            // if no indexName and searchValue, return everything
            if (indexName && searchValue) {
                store = store.index(indexName);
            }
            store = store.openCursor(searchValue);
            var docs = [];
            store.onsuccess = function (event) {
                var cursor = event.target.result;
                if (cursor == null) {
                    resolve(docs);
                    return;
                }
                docs.push(cursor.value);
                cursor.continue();
            };
            store.onerror = function (e) {
                console.log('An error occured on idbservice getEntries.');
                reject(e);
            };
        });
    };
    OfflineDatabase.clearTable = function (database, tblName) {
        return new Promise(function (resolve, reject) {
            var transaction = database.transaction([tblName], 'readwrite');
            var request = transaction.objectStore(tblName).clear();
            request.onsuccess = function (e) {
                resolve(true);
            };
            request.onerror = function (e) {
                reject(e);
            };
        });
    };
    OfflineDatabase.init = function (dbName, version, tables) {
        return new Promise(function (resolve, reject) {
            var dbRequest = indexedDB.open(dbName, version);
            dbRequest.onblocked = function (event) {
                console.log('IDB Event: indexedDB blocked.');
            };
            dbRequest.onerror = function (event) {
                console.log('IDB Event: indexedDB error.');
                reject(new Error('Error in database.'));
            };
            dbRequest.onsuccess = function (event) {
                console.log('IDB Event: indexedDB opened: ' + dbName);
                var database = dbRequest.result;
                database.onclose = function (evt) {
                    alert('Offline database closed: ' + database.name);
                };
                database.onabort = function (evt) {
                    alert('IndexedDB abort occured: ' + evt.target.error.message);
                };
                database.onversionchange = function (evt) {
                    console.log('IndexedDB Event: database version changed.');
                };
                database.onerror = function (evt) {
                    alert('IDBDatabase error occurred: ' + evt.target.error.message);
                };
                resolve(database);
            };
            dbRequest.onupgradeneeded = function (event) {
                var database = dbRequest.result;
                tables.forEach(function (table) {
                    if (database.objectStoreNames.contains(table.name)) {
                        database.deleteObjectStore(table.name);
                    }
                    var store = database.createObjectStore(table.name, table.param);
                    table.indexes.forEach(function (index) {
                        if (!store.indexNames.contains(index.name)) {
                            store.createIndex(index.name, index.path, { unique: index.unique });
                        }
                    });
                });
            };
        });
    };
    OfflineDatabase.prototype.getDatabase = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!!this.database) return [3 /*break*/, 2];
                        _a = this;
                        return [4 /*yield*/, OfflineDatabase.init(this.databaseName, this._idbVersion, this._offlineTables)];
                    case 1:
                        _a.database = _b.sent();
                        _b.label = 2;
                    case 2: return [2 /*return*/, this.database];
                }
            });
        });
    };
    OfflineDatabase.prototype.clearDatabase = function () {
        return __awaiter(this, void 0, void 0, function () {
            var database, promises, results, dbName;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getDatabase()];
                    case 1:
                        database = _a.sent();
                        promises = [];
                        this._offlineTables.forEach(function (table) {
                            promises.push(OfflineDatabase.clearTable(database, table.name));
                        });
                        return [4 /*yield*/, Promise.all(promises)];
                    case 2:
                        results = _a.sent();
                        dbName = this.databaseName;
                        results.forEach(function (success) {
                            if (!success) {
                                throw new Error('Failed to clear database database  ' + dbName + ': A table failed to get cleared.');
                            }
                        });
                        database.close();
                        this.database = null;
                        return [2 /*return*/, true];
                }
            });
        });
    };
    OfflineDatabase.prototype.saveEntry = function (tblName, record) {
        return __awaiter(this, void 0, void 0, function () {
            var database, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getDatabase()];
                    case 1:
                        database = _a.sent();
                        return [4 /*yield*/, OfflineDatabase.put(database, tblName, record)];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OfflineDatabase.prototype.getEntries = function (tblName, indexName, searchValue) {
        return __awaiter(this, void 0, void 0, function () {
            var database, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getDatabase()];
                    case 1:
                        database = _a.sent();
                        return [4 /*yield*/, OfflineDatabase.fetch(database, tblName, indexName, searchValue)];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    OfflineDatabase.prototype.put = function (tableName, record) {
        return __awaiter(this, void 0, void 0, function () {
            var database, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getDatabase()];
                    case 1:
                        database = _a.sent();
                        return [4 /*yield*/, OfflineDatabase.put(database, tableName, record)];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    return OfflineDatabase;
}());
exports.OfflineDatabase = OfflineDatabase;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2ZmbGluZS1kYXRhYmFzZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm9mZmxpbmUtZGF0YWJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQTtJQUNFLGtCQUFtQixJQUFZLEVBQVMsS0FBb0IsRUFBUyxPQUFtQjtRQUFyRSxTQUFJLEdBQUosSUFBSSxDQUFRO1FBQVMsVUFBSyxHQUFMLEtBQUssQ0FBZTtRQUFTLFlBQU8sR0FBUCxPQUFPLENBQVk7SUFDeEYsQ0FBQztJQUNILGVBQUM7QUFBRCxDQUFDLEFBSEQsSUFHQztBQUhZLDRCQUFRO0FBS3JCO0lBQ0UsdUJBQW1CLE9BQWUsRUFBUyxhQUFzQjtRQUE5QyxZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQVMsa0JBQWEsR0FBYixhQUFhLENBQVM7SUFDakUsQ0FBQztJQUNILG9CQUFDO0FBQUQsQ0FBQyxBQUhELElBR0M7QUFIWSxzQ0FBYTtBQUsxQjtJQUNFLGtCQUFtQixJQUFZLEVBQVMsSUFBWSxFQUFTLE1BQWU7UUFBekQsU0FBSSxHQUFKLElBQUksQ0FBUTtRQUFTLFNBQUksR0FBSixJQUFJLENBQVE7UUFBUyxXQUFNLEdBQU4sTUFBTSxDQUFTO0lBQzVFLENBQUM7SUFDSCxlQUFDO0FBQUQsQ0FBQyxBQUhELElBR0M7QUFIWSw0QkFBUTtBQUtyQjtJQWdKRSx5QkFBb0IsWUFBWSxFQUFVLGNBQTBCLEVBQVUsV0FBbUI7UUFBN0UsaUJBQVksR0FBWixZQUFZLENBQUE7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBWTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUFRO0lBQUcsQ0FBQztJQTVJdEYsbUJBQUcsR0FBbEIsVUFBbUIsUUFBUSxFQUFFLE9BQWUsRUFBRSxNQUFXO1FBRXZELE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRWpDLElBQU0sV0FBVyxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxXQUFXLENBQUMsQ0FBQztZQUNqRSxJQUFNLE9BQU8sR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUU3RCxPQUFPLENBQUMsU0FBUyxHQUFHLFVBQVUsQ0FBQztnQkFDN0IsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2hCLENBQUMsQ0FBQztZQUVGLE9BQU8sQ0FBQyxPQUFPLEdBQUcsVUFBVSxDQUFDO2dCQUMzQixNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDWixDQUFDLENBQUM7UUFFSixDQUFDLENBQUMsQ0FBQztJQUVMLENBQUM7SUFFYyxxQkFBSyxHQUFwQixVQUFxQixRQUFRLEVBQUUsT0FBZSxFQUFFLFNBQWtCLEVBQUUsV0FBeUI7UUFFM0YsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFakMsSUFBTSxXQUFXLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFLFdBQVcsQ0FBQyxDQUFDO1lBQ2pFLElBQUksS0FBSyxHQUFHLFdBQVcsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDN0MscURBQXFEO1lBQ3JELEVBQUUsQ0FBQyxDQUFDLFNBQVMsSUFBSSxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNqQyxDQUFDO1lBRUQsS0FBSyxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUM7WUFFdEMsSUFBTSxJQUFJLEdBQUcsRUFBRSxDQUFDO1lBRWhCLEtBQUssQ0FBQyxTQUFTLEdBQUcsVUFBVSxLQUFLO2dCQUUvQixJQUFNLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztnQkFFbkMsRUFBRSxDQUFDLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ25CLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDZCxNQUFNLENBQUM7Z0JBQ1QsQ0FBQztnQkFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDeEIsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3BCLENBQUMsQ0FBQztZQUVGLEtBQUssQ0FBQyxPQUFPLEdBQUcsVUFBVSxDQUFDO2dCQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLDRDQUE0QyxDQUFDLENBQUM7Z0JBQzFELE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNaLENBQUMsQ0FBQztRQUVKLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVjLDBCQUFVLEdBQXpCLFVBQTBCLFFBQVEsRUFBRSxPQUFlO1FBRWpELE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRWpDLElBQU0sV0FBVyxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxXQUFXLENBQUMsQ0FBQztZQUNqRSxJQUFNLE9BQU8sR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBRXpELE9BQU8sQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDO2dCQUM3QixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDaEIsQ0FBQyxDQUFDO1lBRUYsT0FBTyxDQUFDLE9BQU8sR0FBRyxVQUFVLENBQUM7Z0JBQzNCLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNaLENBQUMsQ0FBQztRQUVKLENBQUMsQ0FBQyxDQUFDO0lBRUwsQ0FBQztJQUVjLG9CQUFJLEdBQW5CLFVBQW9CLE1BQU0sRUFBRSxPQUFPLEVBQUUsTUFBTTtRQUV6QyxNQUFNLENBQUMsSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUVqQyxJQUFNLFNBQVMsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQztZQUVsRCxTQUFTLENBQUMsU0FBUyxHQUFHLFVBQVMsS0FBSztnQkFDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO1lBQy9DLENBQUMsQ0FBQztZQUVGLFNBQVMsQ0FBQyxPQUFPLEdBQUcsVUFBVSxLQUFLO2dCQUNqQyxPQUFPLENBQUMsR0FBRyxDQUFDLDZCQUE2QixDQUFDLENBQUM7Z0JBQzNDLE1BQU0sQ0FBQyxJQUFJLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUM7WUFDMUMsQ0FBQyxDQUFDO1lBRUYsU0FBUyxDQUFDLFNBQVMsR0FBRyxVQUFVLEtBQUs7Z0JBRW5DLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLEdBQUcsTUFBTSxDQUFDLENBQUM7Z0JBRXRELElBQU0sUUFBUSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUM7Z0JBRWxDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsVUFBVSxHQUFHO29CQUM5QixLQUFLLENBQUMsMkJBQTJCLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNyRCxDQUFDLENBQUM7Z0JBRUYsUUFBUSxDQUFDLE9BQU8sR0FBRyxVQUFVLEdBQUc7b0JBQzlCLEtBQUssQ0FBQywyQkFBMkIsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDaEUsQ0FBQyxDQUFDO2dCQUVGLFFBQVEsQ0FBQyxlQUFlLEdBQUcsVUFBVSxHQUFHO29CQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLDRDQUE0QyxDQUFDLENBQUM7Z0JBQzVELENBQUMsQ0FBQztnQkFFRixRQUFRLENBQUMsT0FBTyxHQUFHLFVBQVMsR0FBRztvQkFDN0IsS0FBSyxDQUFDLDhCQUE4QixHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNuRSxDQUFDLENBQUM7Z0JBRUYsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBRXBCLENBQUMsQ0FBQztZQUVGLFNBQVMsQ0FBQyxlQUFlLEdBQUcsVUFBUyxLQUFLO2dCQUV4QyxJQUFNLFFBQVEsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDO2dCQUVsQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUMsS0FBSztvQkFFbkIsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNuRCxRQUFRLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN6QyxDQUFDO29CQUVELElBQU0sS0FBSyxHQUFHLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDbEUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQyxLQUFLO3dCQUMxQixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQzNDLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsSUFBSSxFQUFFLEVBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxNQUFNLEVBQUMsQ0FBQyxDQUFDO3dCQUNwRSxDQUFDO29CQUNILENBQUMsQ0FBQyxDQUFDO2dCQUVMLENBQUMsQ0FBQyxDQUFDO1lBRUwsQ0FBQyxDQUFDO1FBRUosQ0FBQyxDQUFDLENBQUM7SUFFTCxDQUFDO0lBSWEscUNBQVcsR0FBekI7Ozs7Ozs2QkFFTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQWQsd0JBQWM7d0JBQ2hCLEtBQUEsSUFBSSxDQUFBO3dCQUFZLHFCQUFNLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBQTs7d0JBQXBHLEdBQUssUUFBUSxHQUFHLFNBQW9GLENBQUM7OzRCQUd2RyxzQkFBTyxJQUFJLENBQUMsUUFBUSxFQUFDOzs7O0tBRXRCO0lBRVksdUNBQWEsR0FBMUI7Ozs7OzRCQUVtQixxQkFBTSxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUE7O3dCQUFuQyxRQUFRLEdBQUcsU0FBd0I7d0JBRW5DLFFBQVEsR0FBbUIsRUFBRSxDQUFDO3dCQUVwQyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFDLEtBQWU7NEJBRTFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7d0JBRWxFLENBQUMsQ0FBQyxDQUFDO3dCQUVhLHFCQUFNLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEVBQUE7O3dCQUFyQyxPQUFPLEdBQUcsU0FBMkI7d0JBRXJDLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO3dCQUVqQyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUMsT0FBTzs0QkFFdEIsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dDQUNiLE1BQU0sSUFBSSxLQUFLLENBQUMscUNBQXFDLEdBQUcsTUFBTSxHQUFHLGtDQUFrQyxDQUFDLENBQUM7NEJBQ3ZHLENBQUM7d0JBRUgsQ0FBQyxDQUFDLENBQUM7d0JBRUgsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO3dCQUVqQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzt3QkFFckIsc0JBQU8sSUFBSSxFQUFDOzs7O0tBQ2I7SUFFWSxtQ0FBUyxHQUF0QixVQUF1QixPQUFlLEVBQUUsTUFBVzs7Ozs7NEJBRWhDLHFCQUFNLElBQUksQ0FBQyxXQUFXLEVBQUUsRUFBQTs7d0JBQW5DLFFBQVEsR0FBRyxTQUF3Qjt3QkFFMUIscUJBQU0sZUFBZSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxFQUFBOzt3QkFBN0QsTUFBTSxHQUFHLFNBQW9EO3dCQUVuRSxzQkFBTyxNQUFNLEVBQUM7Ozs7S0FFZjtJQUVZLG9DQUFVLEdBQXZCLFVBQXdCLE9BQWUsRUFBRSxTQUFrQixFQUFFLFdBQXlCOzs7Ozs0QkFFbkUscUJBQU0sSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFBOzt3QkFBbkMsUUFBUSxHQUFHLFNBQXdCO3dCQUUxQixxQkFBTSxlQUFlLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLFdBQVcsQ0FBQyxFQUFBOzt3QkFBL0UsTUFBTSxHQUFHLFNBQXNFO3dCQUVyRixzQkFBTyxNQUFNLEVBQUM7Ozs7S0FFZjtJQUVZLDZCQUFHLEdBQWhCLFVBQWlCLFNBQVMsRUFBRSxNQUFNOzs7Ozs0QkFFZixxQkFBTSxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUE7O3dCQUFuQyxRQUFRLEdBQUcsU0FBd0I7d0JBRTFCLHFCQUFNLGVBQWUsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRSxNQUFNLENBQUMsRUFBQTs7d0JBQS9ELE1BQU0sR0FBRyxTQUFzRDt3QkFFckUsc0JBQU8sTUFBTSxFQUFDOzs7O0tBQ2Y7SUFFSCxzQkFBQztBQUFELENBQUMsQUF4TkQsSUF3TkM7QUF4TlksMENBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJcbmV4cG9ydCBjbGFzcyBJZGJUYWJsZSB7XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBuYW1lOiBzdHJpbmcsIHB1YmxpYyBwYXJhbTogSWRiVGFibGVQYXJhbSwgcHVibGljIGluZGV4ZXM6IElkYkluZGV4W10pIHtcbiAgfVxufVxuXG5leHBvcnQgY2xhc3MgSWRiVGFibGVQYXJhbSB7XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBrZXlQYXRoOiBzdHJpbmcsIHB1YmxpYyBhdXRvSW5jcmVtZW50OiBib29sZWFuKSB7XG4gIH1cbn1cblxuZXhwb3J0IGNsYXNzIElkYkluZGV4IHtcbiAgY29uc3RydWN0b3IocHVibGljIG5hbWU6IHN0cmluZywgcHVibGljIHBhdGg6IHN0cmluZywgcHVibGljIHVuaXF1ZTogYm9vbGVhbikge1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBPZmZsaW5lRGF0YWJhc2Uge1xuXG4gIHByaXZhdGUgZGF0YWJhc2U7XG5cbiAgcHJpdmF0ZSBzdGF0aWMgcHV0KGRhdGFiYXNlLCB0YmxOYW1lOiBzdHJpbmcsIHJlY29yZDogYW55KSB7XG5cbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXG4gICAgICBjb25zdCB0cmFuc2FjdGlvbiA9IGRhdGFiYXNlLnRyYW5zYWN0aW9uKFt0YmxOYW1lXSwgJ3JlYWR3cml0ZScpO1xuICAgICAgY29uc3QgcmVxdWVzdCA9IHRyYW5zYWN0aW9uLm9iamVjdFN0b3JlKHRibE5hbWUpLnB1dChyZWNvcmQpO1xuXG4gICAgICByZXF1ZXN0Lm9uc3VjY2VzcyA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIHJlc29sdmUodHJ1ZSk7XG4gICAgICB9O1xuXG4gICAgICByZXF1ZXN0Lm9uZXJyb3IgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgICByZWplY3QoZSk7XG4gICAgICB9O1xuXG4gICAgfSk7XG5cbiAgfVxuXG4gIHByaXZhdGUgc3RhdGljIGZldGNoKGRhdGFiYXNlLCB0YmxOYW1lOiBzdHJpbmcsIGluZGV4TmFtZT86IHN0cmluZywgc2VhcmNoVmFsdWU/OiBJREJLZXlSYW5nZSk6IFByb21pc2U8YW55PiB7XG5cbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXG4gICAgICBjb25zdCB0cmFuc2FjdGlvbiA9IGRhdGFiYXNlLnRyYW5zYWN0aW9uKFt0YmxOYW1lXSwgJ3JlYWR3cml0ZScpO1xuICAgICAgbGV0IHN0b3JlID0gdHJhbnNhY3Rpb24ub2JqZWN0U3RvcmUodGJsTmFtZSk7XG4gICAgICAvLyBpZiBubyBpbmRleE5hbWUgYW5kIHNlYXJjaFZhbHVlLCByZXR1cm4gZXZlcnl0aGluZ1xuICAgICAgaWYgKGluZGV4TmFtZSAmJiBzZWFyY2hWYWx1ZSkge1xuICAgICAgICBzdG9yZSA9IHN0b3JlLmluZGV4KGluZGV4TmFtZSk7XG4gICAgICB9XG5cbiAgICAgIHN0b3JlID0gc3RvcmUub3BlbkN1cnNvcihzZWFyY2hWYWx1ZSk7XG5cbiAgICAgIGNvbnN0IGRvY3MgPSBbXTtcblxuICAgICAgc3RvcmUub25zdWNjZXNzID0gZnVuY3Rpb24gKGV2ZW50KSB7XG5cbiAgICAgICAgY29uc3QgY3Vyc29yID0gZXZlbnQudGFyZ2V0LnJlc3VsdDtcblxuICAgICAgICBpZiAoY3Vyc29yID09IG51bGwpIHtcbiAgICAgICAgICByZXNvbHZlKGRvY3MpO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGRvY3MucHVzaChjdXJzb3IudmFsdWUpO1xuICAgICAgICBjdXJzb3IuY29udGludWUoKTtcbiAgICAgIH07XG5cbiAgICAgIHN0b3JlLm9uZXJyb3IgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgICBjb25zb2xlLmxvZygnQW4gZXJyb3Igb2NjdXJlZCBvbiBpZGJzZXJ2aWNlIGdldEVudHJpZXMuJyk7XG4gICAgICAgIHJlamVjdChlKTtcbiAgICAgIH07XG5cbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgc3RhdGljIGNsZWFyVGFibGUoZGF0YWJhc2UsIHRibE5hbWU6IHN0cmluZykge1xuXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcblxuICAgICAgY29uc3QgdHJhbnNhY3Rpb24gPSBkYXRhYmFzZS50cmFuc2FjdGlvbihbdGJsTmFtZV0sICdyZWFkd3JpdGUnKTtcbiAgICAgIGNvbnN0IHJlcXVlc3QgPSB0cmFuc2FjdGlvbi5vYmplY3RTdG9yZSh0YmxOYW1lKS5jbGVhcigpO1xuXG4gICAgICByZXF1ZXN0Lm9uc3VjY2VzcyA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIHJlc29sdmUodHJ1ZSk7XG4gICAgICB9O1xuXG4gICAgICByZXF1ZXN0Lm9uZXJyb3IgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgICByZWplY3QoZSk7XG4gICAgICB9O1xuXG4gICAgfSk7XG5cbiAgfVxuXG4gIHByaXZhdGUgc3RhdGljIGluaXQoZGJOYW1lLCB2ZXJzaW9uLCB0YWJsZXMpIHtcblxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cbiAgICAgIGNvbnN0IGRiUmVxdWVzdCA9IGluZGV4ZWREQi5vcGVuKGRiTmFtZSwgdmVyc2lvbik7XG5cbiAgICAgIGRiUmVxdWVzdC5vbmJsb2NrZWQgPSBmdW5jdGlvbihldmVudCkge1xuICAgICAgICBjb25zb2xlLmxvZygnSURCIEV2ZW50OiBpbmRleGVkREIgYmxvY2tlZC4nKTtcbiAgICAgIH07XG5cbiAgICAgIGRiUmVxdWVzdC5vbmVycm9yID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdJREIgRXZlbnQ6IGluZGV4ZWREQiBlcnJvci4nKTtcbiAgICAgICAgcmVqZWN0KG5ldyBFcnJvcignRXJyb3IgaW4gZGF0YWJhc2UuJykpO1xuICAgICAgfTtcblxuICAgICAgZGJSZXF1ZXN0Lm9uc3VjY2VzcyA9IGZ1bmN0aW9uIChldmVudCkge1xuXG4gICAgICAgIGNvbnNvbGUubG9nKCdJREIgRXZlbnQ6IGluZGV4ZWREQiBvcGVuZWQ6ICcgKyBkYk5hbWUpO1xuXG4gICAgICAgIGNvbnN0IGRhdGFiYXNlID0gZGJSZXF1ZXN0LnJlc3VsdDtcblxuICAgICAgICBkYXRhYmFzZS5vbmNsb3NlID0gZnVuY3Rpb24gKGV2dCkge1xuICAgICAgICAgIGFsZXJ0KCdPZmZsaW5lIGRhdGFiYXNlIGNsb3NlZDogJyArIGRhdGFiYXNlLm5hbWUpO1xuICAgICAgICB9O1xuXG4gICAgICAgIGRhdGFiYXNlLm9uYWJvcnQgPSBmdW5jdGlvbiAoZXZ0KSB7XG4gICAgICAgICAgYWxlcnQoJ0luZGV4ZWREQiBhYm9ydCBvY2N1cmVkOiAnICsgZXZ0LnRhcmdldC5lcnJvci5tZXNzYWdlKTtcbiAgICAgICAgfTtcblxuICAgICAgICBkYXRhYmFzZS5vbnZlcnNpb25jaGFuZ2UgPSBmdW5jdGlvbiAoZXZ0KSB7IC8vIGFub3RoZXIgdGFiIG1pZ2h0IGhhdmUgdXBncmFkZWQgdGhlIGRhdGFiYXNlXG4gICAgICAgICAgY29uc29sZS5sb2coJ0luZGV4ZWREQiBFdmVudDogZGF0YWJhc2UgdmVyc2lvbiBjaGFuZ2VkLicpO1xuICAgICAgICB9O1xuXG4gICAgICAgIGRhdGFiYXNlLm9uZXJyb3IgPSBmdW5jdGlvbihldnQpIHtcbiAgICAgICAgICBhbGVydCgnSURCRGF0YWJhc2UgZXJyb3Igb2NjdXJyZWQ6ICcgKyBldnQudGFyZ2V0LmVycm9yLm1lc3NhZ2UpO1xuICAgICAgICB9O1xuXG4gICAgICAgIHJlc29sdmUoZGF0YWJhc2UpO1xuXG4gICAgICB9O1xuXG4gICAgICBkYlJlcXVlc3Qub251cGdyYWRlbmVlZGVkID0gZnVuY3Rpb24oZXZlbnQpIHtcblxuICAgICAgICBjb25zdCBkYXRhYmFzZSA9IGRiUmVxdWVzdC5yZXN1bHQ7XG5cbiAgICAgICAgdGFibGVzLmZvckVhY2goKHRhYmxlKSA9PiB7XG5cbiAgICAgICAgICBpZiAoZGF0YWJhc2Uub2JqZWN0U3RvcmVOYW1lcy5jb250YWlucyh0YWJsZS5uYW1lKSkge1xuICAgICAgICAgICAgZGF0YWJhc2UuZGVsZXRlT2JqZWN0U3RvcmUodGFibGUubmFtZSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgY29uc3Qgc3RvcmUgPSBkYXRhYmFzZS5jcmVhdGVPYmplY3RTdG9yZSh0YWJsZS5uYW1lLCB0YWJsZS5wYXJhbSk7XG4gICAgICAgICAgdGFibGUuaW5kZXhlcy5mb3JFYWNoKChpbmRleCkgPT4ge1xuICAgICAgICAgICAgaWYgKCFzdG9yZS5pbmRleE5hbWVzLmNvbnRhaW5zKGluZGV4Lm5hbWUpKSB7XG4gICAgICAgICAgICAgIHN0b3JlLmNyZWF0ZUluZGV4KGluZGV4Lm5hbWUsIGluZGV4LnBhdGgsIHt1bmlxdWU6IGluZGV4LnVuaXF1ZX0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pO1xuXG4gICAgICAgIH0pO1xuXG4gICAgICB9O1xuXG4gICAgfSk7XG5cbiAgfVxuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZGF0YWJhc2VOYW1lLCBwcml2YXRlIF9vZmZsaW5lVGFibGVzOiBJZGJUYWJsZVtdLCBwcml2YXRlIF9pZGJWZXJzaW9uOiBudW1iZXIpIHt9XG5cbiAgcHJpdmF0ZSBhc3luYyBnZXREYXRhYmFzZSgpOiBQcm9taXNlPGFueT4ge1xuXG4gICAgaWYgKCF0aGlzLmRhdGFiYXNlKSB7XG4gICAgICB0aGlzLmRhdGFiYXNlID0gYXdhaXQgT2ZmbGluZURhdGFiYXNlLmluaXQodGhpcy5kYXRhYmFzZU5hbWUsIHRoaXMuX2lkYlZlcnNpb24sIHRoaXMuX29mZmxpbmVUYWJsZXMpO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLmRhdGFiYXNlO1xuXG4gIH1cblxuICBwdWJsaWMgYXN5bmMgY2xlYXJEYXRhYmFzZSgpOiBQcm9taXNlPGFueT4ge1xuXG4gICAgY29uc3QgZGF0YWJhc2UgPSBhd2FpdCB0aGlzLmdldERhdGFiYXNlKCk7XG5cbiAgICBjb25zdCBwcm9taXNlczogUHJvbWlzZTxhbnk+W10gPSBbXTtcblxuICAgIHRoaXMuX29mZmxpbmVUYWJsZXMuZm9yRWFjaCgodGFibGU6IElkYlRhYmxlKSA9PiB7XG5cbiAgICAgIHByb21pc2VzLnB1c2goT2ZmbGluZURhdGFiYXNlLmNsZWFyVGFibGUoZGF0YWJhc2UsIHRhYmxlLm5hbWUpKTtcblxuICAgIH0pO1xuXG4gICAgY29uc3QgcmVzdWx0cyA9IGF3YWl0IFByb21pc2UuYWxsKHByb21pc2VzKTtcblxuICAgIGNvbnN0IGRiTmFtZSA9IHRoaXMuZGF0YWJhc2VOYW1lO1xuXG4gICAgcmVzdWx0cy5mb3JFYWNoKChzdWNjZXNzKSA9PiB7XG5cbiAgICAgIGlmICghc3VjY2Vzcykge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0ZhaWxlZCB0byBjbGVhciBkYXRhYmFzZSBkYXRhYmFzZSAgJyArIGRiTmFtZSArICc6IEEgdGFibGUgZmFpbGVkIHRvIGdldCBjbGVhcmVkLicpO1xuICAgICAgfVxuXG4gICAgfSk7XG5cbiAgICBkYXRhYmFzZS5jbG9zZSgpO1xuXG4gICAgdGhpcy5kYXRhYmFzZSA9IG51bGw7XG5cbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIHB1YmxpYyBhc3luYyBzYXZlRW50cnkodGJsTmFtZTogc3RyaW5nLCByZWNvcmQ6IGFueSkge1xuXG4gICAgY29uc3QgZGF0YWJhc2UgPSBhd2FpdCB0aGlzLmdldERhdGFiYXNlKCk7XG5cbiAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBPZmZsaW5lRGF0YWJhc2UucHV0KGRhdGFiYXNlLCB0YmxOYW1lLCByZWNvcmQpO1xuXG4gICAgcmV0dXJuIHJlc3VsdDtcblxuICB9XG5cbiAgcHVibGljIGFzeW5jIGdldEVudHJpZXModGJsTmFtZTogc3RyaW5nLCBpbmRleE5hbWU/OiBzdHJpbmcsIHNlYXJjaFZhbHVlPzogSURCS2V5UmFuZ2UpOiBQcm9taXNlPGFueT4ge1xuXG4gICAgY29uc3QgZGF0YWJhc2UgPSBhd2FpdCB0aGlzLmdldERhdGFiYXNlKCk7XG5cbiAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBPZmZsaW5lRGF0YWJhc2UuZmV0Y2goZGF0YWJhc2UsIHRibE5hbWUsIGluZGV4TmFtZSwgc2VhcmNoVmFsdWUpO1xuXG4gICAgcmV0dXJuIHJlc3VsdDtcblxuICB9XG5cbiAgcHVibGljIGFzeW5jIHB1dCh0YWJsZU5hbWUsIHJlY29yZCkge1xuXG4gICAgY29uc3QgZGF0YWJhc2UgPSBhd2FpdCB0aGlzLmdldERhdGFiYXNlKCk7XG5cbiAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBPZmZsaW5lRGF0YWJhc2UucHV0KGRhdGFiYXNlLCB0YWJsZU5hbWUsIHJlY29yZCk7XG5cbiAgICByZXR1cm4gcmVzdWx0O1xuICB9XG5cbn1cbiJdfQ==