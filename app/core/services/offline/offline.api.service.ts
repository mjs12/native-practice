
import {OfflineDatabase} from './database/offline-database';
import {
  Bet, Transaction, Draw, Raffle, Summary, SummaryTrace, SummaryTraceDetail,
  User
} from '../../models/outlet.model';
import {dateNoTime} from '../helpers/date-helper';
import {paddy, rambolito, rambolitoInt} from '../helpers/number-helper';
import {OFFLINE_CONSTANTS} from '../helpers/contants';
import {DatePipe} from "@angular/common";
import * as localStorage from '/nativescript-localstorage';
const _offlineTables = [
  {
    name: OFFLINE_CONSTANTS.TABLES.TRANSACTION,
    param: { keyPath: '_systemHeader.documentId', autoIncrement: false},
    indexes : [
      { name: OFFLINE_CONSTANTS.INDEXES.DOCUMENT_ID, path: '_systemHeader.documentId', unique: true },
      { name: OFFLINE_CONSTANTS.INDEXES.TRANSACTION_ID, path: 'transactionId', unique: true }
    ]
  },
  {
    name: OFFLINE_CONSTANTS.TABLES.SUMMARY,
    param: { keyPath: 'combinationId', autoIncrement: false},
    indexes : [
      // document specific index
      { name: OFFLINE_CONSTANTS.INDEXES.COMBINATION_ID, path: 'combinationId', unique: true },
      { name: OFFLINE_CONSTANTS.INDEXES.COMBINATION, path: 'combination', unique: false }
    ]
  }
];

export module OfflineAPIService {

  // helper
  // will contain all database connections to different databases (each database is created for a draw) j sa
  const pool = {};

  // helper
  function getDBIndex(dateMillis: number, draw: Draw) {

    const drawStr = draw ? draw + '' : '';
    const outletId = localStorage.getItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.OUTLET_ID);

    return OFFLINE_CONSTANTS.IDB_NAME_PREFIX + '-' + outletId + '-' + dateMillis + '-' + drawStr;

  }

  // download(jsonData, 'json.txt', 'text/plain');
  function download(content, fileName, contentType) {
    const a = document.createElement('a');
    const file = new Blob([content], {type: contentType});
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
  }

  // helper
  function getDatabase(dateMillis: number, draw: Draw) {

    const index = getDBIndex(dateMillis, draw);
    let idb: OfflineDatabase = pool[index];

    if (!idb) {
      idb = new OfflineDatabase(index, _offlineTables, OFFLINE_CONSTANTS.IDB_VERSION);
      pool[index] = idb;
    }

    return idb;

  }

  // helper
  async function updateSummaryOnTransactionCancelled(database: OfflineDatabase, transaction: Transaction, bet: Bet) {

    // get the summary id which is just a combination of bet and raffle
    const combinationId = Summary.combinationId(transaction.date, transaction.draw, bet.raffle, bet.combination);

    // search for the combination summary if it already exist
    const result = await database
      .getEntries(OFFLINE_CONSTANTS.TABLES.SUMMARY, OFFLINE_CONSTANTS.INDEXES.COMBINATION_ID, IDBKeyRange.only(combinationId));

    if (result.length > 1) {
      throw new Error('Found an abnormal number of summaries for a given combination.');
    }

    const summary: Summary = result[0];

    if (!summary) {
      return true;
    }

    summary.amount = 0;

    summary.trace.forEach((traceItem: SummaryTrace) => {
      if (traceItem.transactionId === transaction.transactionId) {
        traceItem.cancelled = true;
      } else if (traceItem.cancelled === false || traceItem.cancelled == null) {
        traceItem.details.forEach((traceDetail: SummaryTraceDetail) => {
          summary.amount += traceDetail.amount;
        });
      }
    });

    return await database.put(OFFLINE_CONSTANTS.TABLES.SUMMARY, summary);

  }

  async function getCurrentValue(draw: Draw, combination: number ){

    const date = dateNoTime(new Date()).getTime();

    const database: OfflineDatabase = getDatabase(date, draw);

    const target = combination; //s3r and s3

    let _currentValue = 0;

    const s3Id = Summary.combinationId(date, draw, Raffle.S3, combination);
    let result = await database.getEntries(OFFLINE_CONSTANTS.TABLES.SUMMARY, OFFLINE_CONSTANTS.INDEXES.COMBINATION_ID, IDBKeyRange.only(s3Id));
    if(result && result.length > 0){
      const s3Summary: Summary = result[0];
      _currentValue += s3Summary.amount;
    }

    const rambol = Array.from(rambolito(target));

    for (let i of rambol){
      const id = Summary.combinationId(date, draw, Raffle.S3R, Number(i));
      let r = await database.getEntries(OFFLINE_CONSTANTS.TABLES.SUMMARY, OFFLINE_CONSTANTS.INDEXES.COMBINATION_ID, IDBKeyRange.only(id));
      if(r && r.length > 0){
        const s: Summary = r[0];
        _currentValue += (s.amount / rambol.length);
      }
    }

    return _currentValue;

  }

  // helper
  async function updateSummary(transaction: Transaction, database?: OfflineDatabase, ) {

    database = database ? database : getDatabase(transaction.date, transaction.draw);

    for(const bet of transaction.bets){

      const summaryCombinationID = Summary.combinationId(transaction.date, transaction.draw, bet.raffle, bet.combination);

      // search for the combination summary if it already exist
      const result = await database.getEntries(OFFLINE_CONSTANTS.TABLES.SUMMARY, OFFLINE_CONSTANTS.INDEXES.COMBINATION_ID, IDBKeyRange.only(summaryCombinationID));

      if (result.length > 1) {
        // throw new Error('Found an abnormal number of summaries for a given combination');
        console.error('Found an abnormal number of summaries for a given combination.');
      }

      let summary: Summary = result[0];

      if (!summary) {
        summary = new Summary(transaction.date, transaction.draw, bet.raffle, bet.combination);
      }

      //focus on adding the trace detail
      let trace: SummaryTrace = null;
      for(const currentTrace of summary.trace){
        if(currentTrace.transactionId === transaction.transactionId){
          trace = currentTrace;
          break;
        }
      }

      if(!trace){
        trace = new SummaryTrace(transaction.transactionId, []);
        summary.trace.push(trace);
      }

      let traceDetail: SummaryTraceDetail = null;
      for(const currentTraceItem of trace.details){
        if(currentTraceItem.betId === bet.id){
          traceDetail = currentTraceItem;
          break;
        }
      }

      if(!traceDetail){
        traceDetail = new SummaryTraceDetail(bet.id, bet.amount);
        trace.details.push(traceDetail);
      }

      // update summary amount
      summary.amount = 0;

      for(const currentTrace of summary.trace){
        for(const currentTraceItem of currentTrace.details){
          summary.amount += currentTraceItem.amount;
        }
      }

      await database.put(OFFLINE_CONSTANTS.TABLES.SUMMARY, summary);

    }

  }

  // helper
  class DrawData {

    public documentId: string;
    public dateNoTime: number;
    public closeCode: number;

    constructor(public date: Date, public draw: Draw) {
      this.dateNoTime = dateNoTime(date).getTime();
      this.documentId = localStorage.getItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.OUTLET_ID) + '-' +this.dateNoTime + '-' + draw;
    }

  }

  // helper
  function getClosedDraw(drawData: DrawData): DrawData {

    const stored = localStorage.getItem(btoa(drawData.documentId));

    if (!stored || stored === 'null') {
      return null; // reject no active draw
    }

    return JSON.parse(atob(stored));
  }

  // helper
  async function downloadDraw(dateMillis: number, draw: Draw, closeCode: number) {

    const offlineDatabase: OfflineDatabase = getDatabase(dateMillis, draw);
    const transactionList: Transaction [] = await offlineDatabase.getEntries(OFFLINE_CONSTANTS.TABLES.TRANSACTION);
    let summaryList: Summary[] = await offlineDatabase.getEntries(OFFLINE_CONSTANTS.TABLES.SUMMARY);

    function getTransactionSales(){
      let sales = 0;
      transactionList.forEach((transaction: Transaction) => {
        sales += transaction.total;
      });
      return sales;
    }

    function getSummarySales(summaryList: Summary[]){
      let sales = 0;
      summaryList.forEach((summary: Summary) => {
        sales += summary.amount;
      });

      return sales;
    }

    let transactionSales = getTransactionSales();
    let summarySales = getSummarySales(summaryList);

    if(summarySales !== transactionSales){
      // Attempt to update summary again:

      for(const transaction of transactionList){
        await updateSummary(transaction);
      }

      summaryList = await offlineDatabase.getEntries(OFFLINE_CONSTANTS.TABLES.SUMMARY);
      summarySales = getSummarySales(summaryList);
      // attempt still failed so there is a problem with the update summary code
      if(summarySales !== transactionSales){
        throw new Error('Transaction sales do not match with summary sales ' + transactionSales + ' != ' + summarySales);
      }

    }

    let data = {
      closeCode : closeCode,
      date : dateMillis,
      draw : draw,
      outletId : localStorage.getItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.OUTLET_ID),
      branchId : localStorage.getItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.BRANCH_ID),
      createdBy : localStorage.getItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.USER_ID),
      documents : [].concat(transactionList).concat(summaryList),
      totalSales: summarySales
    };

    const loggedUser = getLoggedUser();

    if(!loggedUser){
      throw new Error('No outlet user logged in.');
    }

    const datePipe = new DatePipe("en-US");

    let drawStr = null;

    switch (draw){

      case Draw.DRAW1:
        drawStr = '11AM';
        break;
      case Draw.DRAW1:
        drawStr = '4PM';
        break;
      case Draw.DRAW1:
        drawStr = '9PM';
        break;

    }

    const fn =
      loggedUser._.outlet.name +
      '_' +
      drawStr +
      '_' +
      datePipe.transform(new Date(dateMillis), 'dd-MMM-yyyy') +
      '_' +
      loggedUser._.branch.name +
      '.txt';

    download(btoa(JSON.stringify(data)), fn, 'text/plain');

    return data;

  }

  // --------------- offline api ----------------------

  export async function checkNewBet(draw: Draw, bet: Bet, bets: Bet[]){

    // check for duplicates
    const s3rCombos = bet.raffle === Raffle.S3R ? rambolitoInt(bet.combination) : null;

    const LIMIT_PER_COMBINATION = 100;

    let currentValue = 0;

    bets.forEach(each => {

      let matches = false;
      let possibleCombos = 1;

      if(s3rCombos){
        matches = s3rCombos.some((c) => {return c === each.combination});
      }else if(each.raffle === Raffle.S3R ){
        const s3rEach = rambolitoInt(each.combination);
        matches = s3rEach.some(c => {return c === bet.combination});
        possibleCombos = s3rEach.length;
      }else {
        matches = each.combination === bet.combination;
      }

      if(matches && each.raffle === bet.raffle){
        throw new Error('Bet already added: ' + bet.raffle + '.' + paddy(bet.combination));
      }else if(matches){
        currentValue += (each.raffle === Raffle.S3R ? (each.amount / possibleCombos) : each.amount) ;
      }

    });

    currentValue += await getCurrentValue(draw, bet.combination);

    let leftAmount = LIMIT_PER_COMBINATION - currentValue;

    if(leftAmount <= 0){
      throw new Error('Limit reached: '+ paddy(bet.combination, 3));
    }

    if(leftAmount < (s3rCombos ? bet.amount / s3rCombos.length : bet.amount)){

      if(s3rCombos){
        leftAmount = leftAmount * s3rCombos.length;
      }

      throw new Error('Only ' + leftAmount + ' pesos left for ' + bet.raffle + '.' + paddy(bet.combination, 3));
    }

    return true;

  }

  // add transaction and bets to offline database
  export async function addTransaction(transaction: Transaction) {

    const currentDate = new Date();

    // check if transaction's draw is open, cancel if not
    const drawClosed = getClosedDraw(new DrawData(currentDate, transaction.draw)) != null;

    if (drawClosed) {
      throw new Error('Aborted: draw is already closed.');
    }

    const transactionDate = dateNoTime(currentDate).getTime(); // adding transactions will always set the transactionDate to now

    // make sure the transactionDate is updated to now
    transaction.date = transactionDate;

    const offline: OfflineDatabase = getDatabase(transactionDate, transaction.draw);

    await updateSummary(transaction, offline);

    // TODO: before saving, check again if transaction id exists and change if it does
    const transactionSaved = await offline.saveEntry('transaction', transaction);

    if (!transactionSaved) {
      throw new Error('Aborting adding transaction and bets: Transaction entry failed to get recorded.');
      // TODO: revert
    }

    return true;

  }

  export async function searchSummary(dateArg: Date, draw: Draw, key?) { //key can be number or transaction

    if (!draw) {
      throw new Error('No draw specified.');
    }

    const date = dateNoTime(dateArg).getTime();

    if(key && key.length > 7){ //if transaction number, transaction numbers are always > 7 in length
      return await searchTransaction(date, draw, key);
    }

    return await searchBetSummary(date, draw, Number(key));

  }

  // search for bets at the offline database (used in outlet>summary table
  export async function searchTransaction(dateMillis: number, draw: Draw, transactionNumber: string) { //key can be number or transaction

    const offlineDatabase: OfflineDatabase = getDatabase(dateMillis, draw);

    const result = await offlineDatabase
      .getEntries(OFFLINE_CONSTANTS.TABLES.TRANSACTION, OFFLINE_CONSTANTS.INDEXES.TRANSACTION_ID, IDBKeyRange.only(transactionNumber));

    if(!result || result.length === 0){
      return {
        summaryList : [],
        totalAmount : 0
      }
    }

    const transaction: Transaction = result[0];

    let summaryList: Summary[] =  [];

    const bets = transaction.bets;

    for(let bet of bets){

      const summary = new Summary(dateMillis, draw, bet.raffle, bet.combination);
      summary.amount = bet.amount;

      summary.trace = [];

      const details: SummaryTraceDetail[] = [];
      details.push(new SummaryTraceDetail(bet.id, bet.amount));

      summary.trace.push(new SummaryTrace(transaction.transactionId, details))

      summaryList.push(summary);
    }

    return {
      summaryList : summaryList,
      totalAmount : transaction.total
    };
  }

  // Used in transaction list
  export async function getTransaction(dateMillis: number, draw: Draw, transactionNumber?: string): Promise<Transaction[]> { //key can be number or transaction

    const offlineDatabase: OfflineDatabase = getDatabase(dateMillis, draw);

    let result: Transaction[] = [];

    transactionNumber = transactionNumber ? transactionNumber.trim() : null;

    if(!transactionNumber || transactionNumber === ''){ //retrieve all
      result = await offlineDatabase.getEntries(OFFLINE_CONSTANTS.TABLES.TRANSACTION);
    } else {
      result = await offlineDatabase.getEntries(OFFLINE_CONSTANTS.TABLES.TRANSACTION, OFFLINE_CONSTANTS.INDEXES.TRANSACTION_ID, IDBKeyRange.only(transactionNumber));
    }

    result.sort((a, b) => {
      return b._systemHeader.createdDate - a._systemHeader.createdDate;
    });

    return result;

  }

  // search for bets at the offline database
  export async function searchBetSummary(dateMillis: number, draw: Draw, combination:number) { //key can be number or transaction

    const offlineDatabase: OfflineDatabase = getDatabase(dateMillis, draw);

    let summaryList: Summary[] =  [];

    if (!combination) {
      // return everything
      summaryList = await offlineDatabase.getEntries(OFFLINE_CONSTANTS.TABLES.SUMMARY);
    } else {
      // return including rambolito combinations
      const possibilities = Array.from(rambolito(combination));
      for (let x = 0; x < possibilities.length; x++) {
        const combo = possibilities[x];
        // + operator will convert combo to a number
        // https://stackoverflow.com/questions/14667713/typescript-converting-a-string-to-a-number
        const result = await offlineDatabase
          .getEntries(OFFLINE_CONSTANTS.TABLES.SUMMARY, OFFLINE_CONSTANTS.INDEXES.COMBINATION, IDBKeyRange.only(+combo));
        summaryList = summaryList.concat(result);
      }
    }

    let totalAmount = 0;
    summaryList.forEach((bet) => {
      totalAmount += bet.amount;
    });

    return {
      summaryList : summaryList,
      totalAmount : totalAmount
    };
  }

  // clears up transactions and bet for a given date (all draws)
  export async function clear(date ?: number) {

    if (!date) {
      date = Date.now();
    }

    const toClear = [
      getDBIndex(date, Draw.DRAW1),
      getDBIndex(date, Draw.DRAW2),
      getDBIndex(date, Draw.DRAW3)
    ];

    const promises: Promise<any>[] = [];

    toClear.forEach(index => {

      let offlineDB: OfflineDatabase = pool[index];

      if (!offlineDB) {
        offlineDB = new OfflineDatabase(index, _offlineTables, OFFLINE_CONSTANTS.IDB_VERSION);
      } else {
        delete pool[index];
      }

      promises.push(offlineDB.clearDatabase());

    });

    await Promise.all(promises);

    return true;

  }

  // always assumes that this is a raw date an not processed by dateNoTime
  export async function closeDraw(draw: Draw, date?: Date): Promise<any> {

    date = date ? date : new Date();

    const candidate: DrawData =  new DrawData (date, draw);

    // if candidate is already closed then download immediately
    const closedDraw: DrawData = getClosedDraw(candidate);

    if (closedDraw) {
      const data = await downloadDraw(closedDraw.dateNoTime, closedDraw.draw, closedDraw.closeCode);
      return {
        cc : closedDraw.closeCode,
        data : data
      };
    }

    // candidate is not yet closed:

    candidate.closeCode = Date.now();

    localStorage.setItem(btoa(candidate.documentId), btoa(JSON.stringify(candidate)));

    const data = await downloadDraw(candidate.dateNoTime, candidate.draw, candidate.closeCode);

    return {
      cc : candidate.closeCode,
      data : data
    };

  }

  // you can only cancel transactions on the current date
  export async function cancelTransaction(draw: Draw, transactionId: string) {

    if (!draw) {
      throw new Error('Draw not specified when cancelling transaction.');
    }

    if (transactionId.length === 0) {
      throw new Error('Invalid transaction id specified on cancelling transaction.');
    }

    const currentDate = new Date();

    // check if transaction's draw is open, cancel if not
    const drawClosed = getClosedDraw(new DrawData(currentDate, draw)) != null;

    if (drawClosed) {
      throw new Error('Aborted: draw is already closed.');
    }

    const offline: OfflineDatabase = getDatabase(dateNoTime(currentDate).getTime(), draw);

    const result = await offline
      .getEntries(OFFLINE_CONSTANTS.TABLES.TRANSACTION, OFFLINE_CONSTANTS.INDEXES.TRANSACTION_ID, IDBKeyRange.only(transactionId));

    if (result.length > 1) {
      throw new Error('Found an abnormal number of transaction for a given transactionId');
    }

    const transaction: Transaction = result[0];

    if (!transaction) {
      throw new Error('Transaction not found on current date (today) and given draw.');
    }

    // update summaries
    for (let x = 0 ; x < transaction.bets.length ; x++) {
      const bet = transaction.bets[x];
      const summaryUpdated = await updateSummaryOnTransactionCancelled(offline, transaction, bet);
      if (!summaryUpdated) {
        throw new Error('Aborted cancelling transaction. Summary was not updated.');
        // TODO: revert
      }
    }

    // update transaction cancelled = true
    transaction.cancelled = true;
    return await offline.put(OFFLINE_CONSTANTS.TABLES.TRANSACTION, transaction);

  }

  export function getLoggedUser(){

    try{
      const raw = localStorage.getItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.USER_INFO);
      return JSON.parse(raw);
    }catch(err){
      return null;
    }

  }

  export function login( user: User){
    localStorage.setItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.OUTLET_ID, user.outletId);
    localStorage.setItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.BRANCH_ID, user.branchId);
    localStorage.setItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.USER_ID, user._systemHeader.documentId);
    localStorage.setItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.USER_INFO, JSON.stringify(user));
  }

  export function logout() {
    localStorage.setItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.OUTLET_ID, '');
    localStorage.setItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.BRANCH_ID, '');
    localStorage.setItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.USER_ID, '');
    localStorage.setItem(OFFLINE_CONSTANTS.LOCAL_STORAGE.USER_INFO, '');
  }

}


