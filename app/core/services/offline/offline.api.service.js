"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var offline_database_1 = require("./database/offline-database");
var outlet_model_1 = require("../../models/outlet.model");
var date_helper_1 = require("../helpers/date-helper");
var number_helper_1 = require("../helpers/number-helper");
var contants_1 = require("../helpers/contants");
var common_1 = require("@angular/common");
var localStorage = require("nativescript-localstorage");
var _offlineTables = [
    {
        name: contants_1.OFFLINE_CONSTANTS.TABLES.TRANSACTION,
        param: { keyPath: '_systemHeader.documentId', autoIncrement: false },
        indexes: [
            { name: contants_1.OFFLINE_CONSTANTS.INDEXES.DOCUMENT_ID, path: '_systemHeader.documentId', unique: true },
            { name: contants_1.OFFLINE_CONSTANTS.INDEXES.TRANSACTION_ID, path: 'transactionId', unique: true }
        ]
    },
    {
        name: contants_1.OFFLINE_CONSTANTS.TABLES.SUMMARY,
        param: { keyPath: 'combinationId', autoIncrement: false },
        indexes: [
            // document specific index
            { name: contants_1.OFFLINE_CONSTANTS.INDEXES.COMBINATION_ID, path: 'combinationId', unique: true },
            { name: contants_1.OFFLINE_CONSTANTS.INDEXES.COMBINATION, path: 'combination', unique: false }
        ]
    }
];
var OfflineAPIService;
(function (OfflineAPIService) {
    // helper
    // will contain all database connections to different databases (each database is created for a draw) j sa
    var pool = {};
    // helper
    function getDBIndex(dateMillis, draw) {
        var drawStr = draw ? draw + '' : '';
        var outletId = localStorage.getItem(contants_1.OFFLINE_CONSTANTS.LOCAL_STORAGE.OUTLET_ID);
        return contants_1.OFFLINE_CONSTANTS.IDB_NAME_PREFIX + '-' + outletId + '-' + dateMillis + '-' + drawStr;
    }
    // download(jsonData, 'json.txt', 'text/plain');
    function download(content, fileName, contentType) {
        var a = document.createElement('a');
        var file = new Blob([content], { type: contentType });
        a.href = URL.createObjectURL(file);
        a.download = fileName;
        a.click();
    }
    // helper
    function getDatabase(dateMillis, draw) {
        var index = getDBIndex(dateMillis, draw);
        var idb = pool[index];
        if (!idb) {
            idb = new offline_database_1.OfflineDatabase(index, _offlineTables, contants_1.OFFLINE_CONSTANTS.IDB_VERSION);
            pool[index] = idb;
        }
        return idb;
    }
    // helper
    function updateSummaryOnTransactionCancelled(database, transaction, bet) {
        return __awaiter(this, void 0, void 0, function () {
            var combinationId, result, summary;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        combinationId = outlet_model_1.Summary.combinationId(transaction.date, transaction.draw, bet.raffle, bet.combination);
                        return [4 /*yield*/, database
                                .getEntries(contants_1.OFFLINE_CONSTANTS.TABLES.SUMMARY, contants_1.OFFLINE_CONSTANTS.INDEXES.COMBINATION_ID, IDBKeyRange.only(combinationId))];
                    case 1:
                        result = _a.sent();
                        if (result.length > 1) {
                            throw new Error('Found an abnormal number of summaries for a given combination.');
                        }
                        summary = result[0];
                        if (!summary) {
                            return [2 /*return*/, true];
                        }
                        summary.amount = 0;
                        summary.trace.forEach(function (traceItem) {
                            if (traceItem.transactionId === transaction.transactionId) {
                                traceItem.cancelled = true;
                            }
                            else if (traceItem.cancelled === false || traceItem.cancelled == null) {
                                traceItem.details.forEach(function (traceDetail) {
                                    summary.amount += traceDetail.amount;
                                });
                            }
                        });
                        return [4 /*yield*/, database.put(contants_1.OFFLINE_CONSTANTS.TABLES.SUMMARY, summary)];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    }
    function getCurrentValue(draw, combination) {
        return __awaiter(this, void 0, void 0, function () {
            var date, database, target, _currentValue, s3Id, result, s3Summary, rambol, _i, rambol_1, i, id, r, s;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        date = date_helper_1.dateNoTime(new Date()).getTime();
                        database = getDatabase(date, draw);
                        target = combination;
                        _currentValue = 0;
                        s3Id = outlet_model_1.Summary.combinationId(date, draw, outlet_model_1.Raffle.S3, combination);
                        return [4 /*yield*/, database.getEntries(contants_1.OFFLINE_CONSTANTS.TABLES.SUMMARY, contants_1.OFFLINE_CONSTANTS.INDEXES.COMBINATION_ID, IDBKeyRange.only(s3Id))];
                    case 1:
                        result = _a.sent();
                        if (result && result.length > 0) {
                            s3Summary = result[0];
                            _currentValue += s3Summary.amount;
                        }
                        rambol = Array.from(number_helper_1.rambolito(target));
                        _i = 0, rambol_1 = rambol;
                        _a.label = 2;
                    case 2:
                        if (!(_i < rambol_1.length)) return [3 /*break*/, 5];
                        i = rambol_1[_i];
                        id = outlet_model_1.Summary.combinationId(date, draw, outlet_model_1.Raffle.S3R, Number(i));
                        return [4 /*yield*/, database.getEntries(contants_1.OFFLINE_CONSTANTS.TABLES.SUMMARY, contants_1.OFFLINE_CONSTANTS.INDEXES.COMBINATION_ID, IDBKeyRange.only(id))];
                    case 3:
                        r = _a.sent();
                        if (r && r.length > 0) {
                            s = r[0];
                            _currentValue += (s.amount / rambol.length);
                        }
                        _a.label = 4;
                    case 4:
                        _i++;
                        return [3 /*break*/, 2];
                    case 5: return [2 /*return*/, _currentValue];
                }
            });
        });
    }
    // helper
    function updateSummary(transaction, database) {
        return __awaiter(this, void 0, void 0, function () {
            var _i, _a, bet, summaryCombinationID, result, summary, trace, _b, _c, currentTrace, traceDetail, _d, _e, currentTraceItem, _f, _g, currentTrace, _h, _j, currentTraceItem;
            return __generator(this, function (_k) {
                switch (_k.label) {
                    case 0:
                        database = database ? database : getDatabase(transaction.date, transaction.draw);
                        _i = 0, _a = transaction.bets;
                        _k.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3 /*break*/, 5];
                        bet = _a[_i];
                        summaryCombinationID = outlet_model_1.Summary.combinationId(transaction.date, transaction.draw, bet.raffle, bet.combination);
                        return [4 /*yield*/, database.getEntries(contants_1.OFFLINE_CONSTANTS.TABLES.SUMMARY, contants_1.OFFLINE_CONSTANTS.INDEXES.COMBINATION_ID, IDBKeyRange.only(summaryCombinationID))];
                    case 2:
                        result = _k.sent();
                        if (result.length > 1) {
                            // throw new Error('Found an abnormal number of summaries for a given combination');
                            console.error('Found an abnormal number of summaries for a given combination.');
                        }
                        summary = result[0];
                        if (!summary) {
                            summary = new outlet_model_1.Summary(transaction.date, transaction.draw, bet.raffle, bet.combination);
                        }
                        trace = null;
                        for (_b = 0, _c = summary.trace; _b < _c.length; _b++) {
                            currentTrace = _c[_b];
                            if (currentTrace.transactionId === transaction.transactionId) {
                                trace = currentTrace;
                                break;
                            }
                        }
                        if (!trace) {
                            trace = new outlet_model_1.SummaryTrace(transaction.transactionId, []);
                            summary.trace.push(trace);
                        }
                        traceDetail = null;
                        for (_d = 0, _e = trace.details; _d < _e.length; _d++) {
                            currentTraceItem = _e[_d];
                            if (currentTraceItem.betId === bet.id) {
                                traceDetail = currentTraceItem;
                                break;
                            }
                        }
                        if (!traceDetail) {
                            traceDetail = new outlet_model_1.SummaryTraceDetail(bet.id, bet.amount);
                            trace.details.push(traceDetail);
                        }
                        // update summary amount
                        summary.amount = 0;
                        for (_f = 0, _g = summary.trace; _f < _g.length; _f++) {
                            currentTrace = _g[_f];
                            for (_h = 0, _j = currentTrace.details; _h < _j.length; _h++) {
                                currentTraceItem = _j[_h];
                                summary.amount += currentTraceItem.amount;
                            }
                        }
                        return [4 /*yield*/, database.put(contants_1.OFFLINE_CONSTANTS.TABLES.SUMMARY, summary)];
                    case 3:
                        _k.sent();
                        _k.label = 4;
                    case 4:
                        _i++;
                        return [3 /*break*/, 1];
                    case 5: return [2 /*return*/];
                }
            });
        });
    }
    // helper
    var DrawData = /** @class */ (function () {
        function DrawData(date, draw) {
            this.date = date;
            this.draw = draw;
            this.dateNoTime = date_helper_1.dateNoTime(date).getTime();
            this.documentId = localStorage.getItem(contants_1.OFFLINE_CONSTANTS.LOCAL_STORAGE.OUTLET_ID) + '-' + this.dateNoTime + '-' + draw;
        }
        return DrawData;
    }());
    // helper
    function getClosedDraw(drawData) {
        var stored = localStorage.getItem(btoa(drawData.documentId));
        if (!stored || stored === 'null') {
            return null; // reject no active draw
        }
        return JSON.parse(atob(stored));
    }
    // helper
    function downloadDraw(dateMillis, draw, closeCode) {
        return __awaiter(this, void 0, void 0, function () {
            function getTransactionSales() {
                var sales = 0;
                transactionList.forEach(function (transaction) {
                    sales += transaction.total;
                });
                return sales;
            }
            function getSummarySales(summaryList) {
                var sales = 0;
                summaryList.forEach(function (summary) {
                    sales += summary.amount;
                });
                return sales;
            }
            var offlineDatabase, transactionList, summaryList, transactionSales, summarySales, _i, transactionList_1, transaction, data, loggedUser, datePipe, drawStr, fn;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        offlineDatabase = getDatabase(dateMillis, draw);
                        return [4 /*yield*/, offlineDatabase.getEntries(contants_1.OFFLINE_CONSTANTS.TABLES.TRANSACTION)];
                    case 1:
                        transactionList = _a.sent();
                        return [4 /*yield*/, offlineDatabase.getEntries(contants_1.OFFLINE_CONSTANTS.TABLES.SUMMARY)];
                    case 2:
                        summaryList = _a.sent();
                        transactionSales = getTransactionSales();
                        summarySales = getSummarySales(summaryList);
                        if (!(summarySales !== transactionSales)) return [3 /*break*/, 8];
                        _i = 0, transactionList_1 = transactionList;
                        _a.label = 3;
                    case 3:
                        if (!(_i < transactionList_1.length)) return [3 /*break*/, 6];
                        transaction = transactionList_1[_i];
                        return [4 /*yield*/, updateSummary(transaction)];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5:
                        _i++;
                        return [3 /*break*/, 3];
                    case 6: return [4 /*yield*/, offlineDatabase.getEntries(contants_1.OFFLINE_CONSTANTS.TABLES.SUMMARY)];
                    case 7:
                        summaryList = _a.sent();
                        summarySales = getSummarySales(summaryList);
                        // attempt still failed so there is a problem with the update summary code
                        if (summarySales !== transactionSales) {
                            throw new Error('Transaction sales do not match with summary sales ' + transactionSales + ' != ' + summarySales);
                        }
                        _a.label = 8;
                    case 8:
                        data = {
                            closeCode: closeCode,
                            date: dateMillis,
                            draw: draw,
                            outletId: localStorage.getItem(contants_1.OFFLINE_CONSTANTS.LOCAL_STORAGE.OUTLET_ID),
                            branchId: localStorage.getItem(contants_1.OFFLINE_CONSTANTS.LOCAL_STORAGE.BRANCH_ID),
                            createdBy: localStorage.getItem(contants_1.OFFLINE_CONSTANTS.LOCAL_STORAGE.USER_ID),
                            documents: [].concat(transactionList).concat(summaryList),
                            totalSales: summarySales
                        };
                        loggedUser = getLoggedUser();
                        if (!loggedUser) {
                            throw new Error('No outlet user logged in.');
                        }
                        datePipe = new common_1.DatePipe("en-US");
                        drawStr = null;
                        switch (draw) {
                            case outlet_model_1.Draw.DRAW1:
                                drawStr = '11AM';
                                break;
                            case outlet_model_1.Draw.DRAW1:
                                drawStr = '4PM';
                                break;
                            case outlet_model_1.Draw.DRAW1:
                                drawStr = '9PM';
                                break;
                        }
                        fn = loggedUser._.outlet.name +
                            '_' +
                            drawStr +
                            '_' +
                            datePipe.transform(new Date(dateMillis), 'dd-MMM-yyyy') +
                            '_' +
                            loggedUser._.branch.name +
                            '.txt';
                        download(btoa(JSON.stringify(data)), fn, 'text/plain');
                        return [2 /*return*/, data];
                }
            });
        });
    }
    // --------------- offline api ----------------------
    function checkNewBet(draw, bet, bets) {
        return __awaiter(this, void 0, void 0, function () {
            var s3rCombos, LIMIT_PER_COMBINATION, currentValue, _a, leftAmount;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        s3rCombos = bet.raffle === outlet_model_1.Raffle.S3R ? number_helper_1.rambolitoInt(bet.combination) : null;
                        LIMIT_PER_COMBINATION = 100;
                        currentValue = 0;
                        bets.forEach(function (each) {
                            var matches = false;
                            var possibleCombos = 1;
                            if (s3rCombos) {
                                matches = s3rCombos.some(function (c) { return c === each.combination; });
                            }
                            else if (each.raffle === outlet_model_1.Raffle.S3R) {
                                var s3rEach = number_helper_1.rambolitoInt(each.combination);
                                matches = s3rEach.some(function (c) { return c === bet.combination; });
                                possibleCombos = s3rEach.length;
                            }
                            else {
                                matches = each.combination === bet.combination;
                            }
                            if (matches && each.raffle === bet.raffle) {
                                throw new Error('Bet already added: ' + bet.raffle + '.' + number_helper_1.paddy(bet.combination));
                            }
                            else if (matches) {
                                currentValue += (each.raffle === outlet_model_1.Raffle.S3R ? (each.amount / possibleCombos) : each.amount);
                            }
                        });
                        _a = currentValue;
                        return [4 /*yield*/, getCurrentValue(draw, bet.combination)];
                    case 1:
                        currentValue = _a + _b.sent();
                        leftAmount = LIMIT_PER_COMBINATION - currentValue;
                        if (leftAmount <= 0) {
                            throw new Error('Limit reached: ' + number_helper_1.paddy(bet.combination, 3));
                        }
                        if (leftAmount < (s3rCombos ? bet.amount / s3rCombos.length : bet.amount)) {
                            if (s3rCombos) {
                                leftAmount = leftAmount * s3rCombos.length;
                            }
                            throw new Error('Only ' + leftAmount + ' pesos left for ' + bet.raffle + '.' + number_helper_1.paddy(bet.combination, 3));
                        }
                        return [2 /*return*/, true];
                }
            });
        });
    }
    OfflineAPIService.checkNewBet = checkNewBet;
    // add transaction and bets to offline database
    function addTransaction(transaction) {
        return __awaiter(this, void 0, void 0, function () {
            var currentDate, drawClosed, transactionDate, offline, transactionSaved;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        currentDate = new Date();
                        drawClosed = getClosedDraw(new DrawData(currentDate, transaction.draw)) != null;
                        if (drawClosed) {
                            throw new Error('Aborted: draw is already closed.');
                        }
                        transactionDate = date_helper_1.dateNoTime(currentDate).getTime();
                        // make sure the transactionDate is updated to now
                        transaction.date = transactionDate;
                        offline = getDatabase(transactionDate, transaction.draw);
                        return [4 /*yield*/, updateSummary(transaction, offline)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, offline.saveEntry('transaction', transaction)];
                    case 2:
                        transactionSaved = _a.sent();
                        if (!transactionSaved) {
                            throw new Error('Aborting adding transaction and bets: Transaction entry failed to get recorded.');
                            // TODO: revert
                        }
                        return [2 /*return*/, true];
                }
            });
        });
    }
    OfflineAPIService.addTransaction = addTransaction;
    function searchSummary(dateArg, draw, key) {
        return __awaiter(this, void 0, void 0, function () {
            var date;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!draw) {
                            throw new Error('No draw specified.');
                        }
                        date = date_helper_1.dateNoTime(dateArg).getTime();
                        if (!(key && key.length > 7)) return [3 /*break*/, 2];
                        return [4 /*yield*/, searchTransaction(date, draw, key)];
                    case 1: //if transaction number, transaction numbers are always > 7 in length
                    return [2 /*return*/, _a.sent()];
                    case 2: return [4 /*yield*/, searchBetSummary(date, draw, Number(key))];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    }
    OfflineAPIService.searchSummary = searchSummary;
    // search for bets at the offline database (used in outlet>summary table
    function searchTransaction(dateMillis, draw, transactionNumber) {
        return __awaiter(this, void 0, void 0, function () {
            var offlineDatabase, result, transaction, summaryList, bets, _i, bets_1, bet, summary, details;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        offlineDatabase = getDatabase(dateMillis, draw);
                        return [4 /*yield*/, offlineDatabase
                                .getEntries(contants_1.OFFLINE_CONSTANTS.TABLES.TRANSACTION, contants_1.OFFLINE_CONSTANTS.INDEXES.TRANSACTION_ID, IDBKeyRange.only(transactionNumber))];
                    case 1:
                        result = _a.sent();
                        if (!result || result.length === 0) {
                            return [2 /*return*/, {
                                    summaryList: [],
                                    totalAmount: 0
                                }];
                        }
                        transaction = result[0];
                        summaryList = [];
                        bets = transaction.bets;
                        for (_i = 0, bets_1 = bets; _i < bets_1.length; _i++) {
                            bet = bets_1[_i];
                            summary = new outlet_model_1.Summary(dateMillis, draw, bet.raffle, bet.combination);
                            summary.amount = bet.amount;
                            summary.trace = [];
                            details = [];
                            details.push(new outlet_model_1.SummaryTraceDetail(bet.id, bet.amount));
                            summary.trace.push(new outlet_model_1.SummaryTrace(transaction.transactionId, details));
                            summaryList.push(summary);
                        }
                        return [2 /*return*/, {
                                summaryList: summaryList,
                                totalAmount: transaction.total
                            }];
                }
            });
        });
    }
    OfflineAPIService.searchTransaction = searchTransaction;
    // Used in transaction list
    function getTransaction(dateMillis, draw, transactionNumber) {
        return __awaiter(this, void 0, void 0, function () {
            var offlineDatabase, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        offlineDatabase = getDatabase(dateMillis, draw);
                        result = [];
                        transactionNumber = transactionNumber ? transactionNumber.trim() : null;
                        if (!(!transactionNumber || transactionNumber === '')) return [3 /*break*/, 2];
                        return [4 /*yield*/, offlineDatabase.getEntries(contants_1.OFFLINE_CONSTANTS.TABLES.TRANSACTION)];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, offlineDatabase.getEntries(contants_1.OFFLINE_CONSTANTS.TABLES.TRANSACTION, contants_1.OFFLINE_CONSTANTS.INDEXES.TRANSACTION_ID, IDBKeyRange.only(transactionNumber))];
                    case 3:
                        result = _a.sent();
                        _a.label = 4;
                    case 4:
                        result.sort(function (a, b) {
                            return b._systemHeader.createdDate - a._systemHeader.createdDate;
                        });
                        return [2 /*return*/, result];
                }
            });
        });
    }
    OfflineAPIService.getTransaction = getTransaction;
    // search for bets at the offline database
    function searchBetSummary(dateMillis, draw, combination) {
        return __awaiter(this, void 0, void 0, function () {
            var offlineDatabase, summaryList, possibilities, x, combo, result, totalAmount;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        offlineDatabase = getDatabase(dateMillis, draw);
                        summaryList = [];
                        if (!!combination) return [3 /*break*/, 2];
                        return [4 /*yield*/, offlineDatabase.getEntries(contants_1.OFFLINE_CONSTANTS.TABLES.SUMMARY)];
                    case 1:
                        // return everything
                        summaryList = _a.sent();
                        return [3 /*break*/, 6];
                    case 2:
                        possibilities = Array.from(number_helper_1.rambolito(combination));
                        x = 0;
                        _a.label = 3;
                    case 3:
                        if (!(x < possibilities.length)) return [3 /*break*/, 6];
                        combo = possibilities[x];
                        return [4 /*yield*/, offlineDatabase
                                .getEntries(contants_1.OFFLINE_CONSTANTS.TABLES.SUMMARY, contants_1.OFFLINE_CONSTANTS.INDEXES.COMBINATION, IDBKeyRange.only(+combo))];
                    case 4:
                        result = _a.sent();
                        summaryList = summaryList.concat(result);
                        _a.label = 5;
                    case 5:
                        x++;
                        return [3 /*break*/, 3];
                    case 6:
                        totalAmount = 0;
                        summaryList.forEach(function (bet) {
                            totalAmount += bet.amount;
                        });
                        return [2 /*return*/, {
                                summaryList: summaryList,
                                totalAmount: totalAmount
                            }];
                }
            });
        });
    }
    OfflineAPIService.searchBetSummary = searchBetSummary;
    // clears up transactions and bet for a given date (all draws)
    function clear(date) {
        return __awaiter(this, void 0, void 0, function () {
            var toClear, promises;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!date) {
                            date = Date.now();
                        }
                        toClear = [
                            getDBIndex(date, outlet_model_1.Draw.DRAW1),
                            getDBIndex(date, outlet_model_1.Draw.DRAW2),
                            getDBIndex(date, outlet_model_1.Draw.DRAW3)
                        ];
                        promises = [];
                        toClear.forEach(function (index) {
                            var offlineDB = pool[index];
                            if (!offlineDB) {
                                offlineDB = new offline_database_1.OfflineDatabase(index, _offlineTables, contants_1.OFFLINE_CONSTANTS.IDB_VERSION);
                            }
                            else {
                                delete pool[index];
                            }
                            promises.push(offlineDB.clearDatabase());
                        });
                        return [4 /*yield*/, Promise.all(promises)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, true];
                }
            });
        });
    }
    OfflineAPIService.clear = clear;
    // always assumes that this is a raw date an not processed by dateNoTime
    function closeDraw(draw, date) {
        return __awaiter(this, void 0, void 0, function () {
            var candidate, closedDraw, data_1, data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        date = date ? date : new Date();
                        candidate = new DrawData(date, draw);
                        closedDraw = getClosedDraw(candidate);
                        if (!closedDraw) return [3 /*break*/, 2];
                        return [4 /*yield*/, downloadDraw(closedDraw.dateNoTime, closedDraw.draw, closedDraw.closeCode)];
                    case 1:
                        data_1 = _a.sent();
                        return [2 /*return*/, {
                                cc: closedDraw.closeCode,
                                data: data_1
                            }];
                    case 2:
                        // candidate is not yet closed:
                        candidate.closeCode = Date.now();
                        localStorage.setItem(btoa(candidate.documentId), btoa(JSON.stringify(candidate)));
                        return [4 /*yield*/, downloadDraw(candidate.dateNoTime, candidate.draw, candidate.closeCode)];
                    case 3:
                        data = _a.sent();
                        return [2 /*return*/, {
                                cc: candidate.closeCode,
                                data: data
                            }];
                }
            });
        });
    }
    OfflineAPIService.closeDraw = closeDraw;
    // you can only cancel transactions on the current date
    function cancelTransaction(draw, transactionId) {
        return __awaiter(this, void 0, void 0, function () {
            var currentDate, drawClosed, offline, result, transaction, x, bet, summaryUpdated;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!draw) {
                            throw new Error('Draw not specified when cancelling transaction.');
                        }
                        if (transactionId.length === 0) {
                            throw new Error('Invalid transaction id specified on cancelling transaction.');
                        }
                        currentDate = new Date();
                        drawClosed = getClosedDraw(new DrawData(currentDate, draw)) != null;
                        if (drawClosed) {
                            throw new Error('Aborted: draw is already closed.');
                        }
                        offline = getDatabase(date_helper_1.dateNoTime(currentDate).getTime(), draw);
                        return [4 /*yield*/, offline
                                .getEntries(contants_1.OFFLINE_CONSTANTS.TABLES.TRANSACTION, contants_1.OFFLINE_CONSTANTS.INDEXES.TRANSACTION_ID, IDBKeyRange.only(transactionId))];
                    case 1:
                        result = _a.sent();
                        if (result.length > 1) {
                            throw new Error('Found an abnormal number of transaction for a given transactionId');
                        }
                        transaction = result[0];
                        if (!transaction) {
                            throw new Error('Transaction not found on current date (today) and given draw.');
                        }
                        x = 0;
                        _a.label = 2;
                    case 2:
                        if (!(x < transaction.bets.length)) return [3 /*break*/, 5];
                        bet = transaction.bets[x];
                        return [4 /*yield*/, updateSummaryOnTransactionCancelled(offline, transaction, bet)];
                    case 3:
                        summaryUpdated = _a.sent();
                        if (!summaryUpdated) {
                            throw new Error('Aborted cancelling transaction. Summary was not updated.');
                            // TODO: revert
                        }
                        _a.label = 4;
                    case 4:
                        x++;
                        return [3 /*break*/, 2];
                    case 5:
                        // update transaction cancelled = true
                        transaction.cancelled = true;
                        return [4 /*yield*/, offline.put(contants_1.OFFLINE_CONSTANTS.TABLES.TRANSACTION, transaction)];
                    case 6: return [2 /*return*/, _a.sent()];
                }
            });
        });
    }
    OfflineAPIService.cancelTransaction = cancelTransaction;
    function getLoggedUser() {
        try {
            var raw = localStorage.getItem(contants_1.OFFLINE_CONSTANTS.LOCAL_STORAGE.USER_INFO);
            return JSON.parse(raw);
        }
        catch (err) {
            return null;
        }
    }
    OfflineAPIService.getLoggedUser = getLoggedUser;
    function login(user) {
        localStorage.setItem(contants_1.OFFLINE_CONSTANTS.LOCAL_STORAGE.OUTLET_ID, user.outletId);
        localStorage.setItem(contants_1.OFFLINE_CONSTANTS.LOCAL_STORAGE.BRANCH_ID, user.branchId);
        localStorage.setItem(contants_1.OFFLINE_CONSTANTS.LOCAL_STORAGE.USER_ID, user._systemHeader.documentId);
        localStorage.setItem(contants_1.OFFLINE_CONSTANTS.LOCAL_STORAGE.USER_INFO, JSON.stringify(user));
    }
    OfflineAPIService.login = login;
    function logout() {
        localStorage.setItem(contants_1.OFFLINE_CONSTANTS.LOCAL_STORAGE.OUTLET_ID, '');
        localStorage.setItem(contants_1.OFFLINE_CONSTANTS.LOCAL_STORAGE.BRANCH_ID, '');
        localStorage.setItem(contants_1.OFFLINE_CONSTANTS.LOCAL_STORAGE.USER_ID, '');
        localStorage.setItem(contants_1.OFFLINE_CONSTANTS.LOCAL_STORAGE.USER_INFO, '');
    }
    OfflineAPIService.logout = logout;
})(OfflineAPIService = exports.OfflineAPIService || (exports.OfflineAPIService = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2ZmbGluZS5hcGkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm9mZmxpbmUuYXBpLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSxnRUFBNEQ7QUFDNUQsMERBR21DO0FBQ25DLHNEQUFrRDtBQUNsRCwwREFBd0U7QUFDeEUsZ0RBQXNEO0FBQ3RELDBDQUF5QztBQUN6Qyx3REFBMEQ7QUFDMUQsSUFBTSxjQUFjLEdBQUc7SUFDckI7UUFDRSxJQUFJLEVBQUUsNEJBQWlCLENBQUMsTUFBTSxDQUFDLFdBQVc7UUFDMUMsS0FBSyxFQUFFLEVBQUUsT0FBTyxFQUFFLDBCQUEwQixFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUM7UUFDbkUsT0FBTyxFQUFHO1lBQ1IsRUFBRSxJQUFJLEVBQUUsNEJBQWlCLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxJQUFJLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTtZQUMvRixFQUFFLElBQUksRUFBRSw0QkFBaUIsQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTtTQUN4RjtLQUNGO0lBQ0Q7UUFDRSxJQUFJLEVBQUUsNEJBQWlCLENBQUMsTUFBTSxDQUFDLE9BQU87UUFDdEMsS0FBSyxFQUFFLEVBQUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFDO1FBQ3hELE9BQU8sRUFBRztZQUNSLDBCQUEwQjtZQUMxQixFQUFFLElBQUksRUFBRSw0QkFBaUIsQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksRUFBRSxlQUFlLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTtZQUN2RixFQUFFLElBQUksRUFBRSw0QkFBaUIsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTtTQUNwRjtLQUNGO0NBQ0YsQ0FBQztBQUVGLElBQWMsaUJBQWlCLENBZ29COUI7QUFob0JELFdBQWMsaUJBQWlCO0lBRTdCLFNBQVM7SUFDVCwwR0FBMEc7SUFDMUcsSUFBTSxJQUFJLEdBQUcsRUFBRSxDQUFDO0lBRWhCLFNBQVM7SUFDVCxvQkFBb0IsVUFBa0IsRUFBRSxJQUFVO1FBRWhELElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ3RDLElBQU0sUUFBUSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsNEJBQWlCLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRWpGLE1BQU0sQ0FBQyw0QkFBaUIsQ0FBQyxlQUFlLEdBQUcsR0FBRyxHQUFHLFFBQVEsR0FBRyxHQUFHLEdBQUcsVUFBVSxHQUFHLEdBQUcsR0FBRyxPQUFPLENBQUM7SUFFL0YsQ0FBQztJQUVELGdEQUFnRDtJQUNoRCxrQkFBa0IsT0FBTyxFQUFFLFFBQVEsRUFBRSxXQUFXO1FBQzlDLElBQU0sQ0FBQyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdEMsSUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxFQUFDLElBQUksRUFBRSxXQUFXLEVBQUMsQ0FBQyxDQUFDO1FBQ3RELENBQUMsQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuQyxDQUFDLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN0QixDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDWixDQUFDO0lBRUQsU0FBUztJQUNULHFCQUFxQixVQUFrQixFQUFFLElBQVU7UUFFakQsSUFBTSxLQUFLLEdBQUcsVUFBVSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMzQyxJQUFJLEdBQUcsR0FBb0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXZDLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNULEdBQUcsR0FBRyxJQUFJLGtDQUFlLENBQUMsS0FBSyxFQUFFLGNBQWMsRUFBRSw0QkFBaUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNoRixJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxDQUFDO1FBQ3BCLENBQUM7UUFFRCxNQUFNLENBQUMsR0FBRyxDQUFDO0lBRWIsQ0FBQztJQUVELFNBQVM7SUFDVCw2Q0FBbUQsUUFBeUIsRUFBRSxXQUF3QixFQUFFLEdBQVE7Ozs7Ozt3QkFHeEcsYUFBYSxHQUFHLHNCQUFPLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFHOUYscUJBQU0sUUFBUTtpQ0FDMUIsVUFBVSxDQUFDLDRCQUFpQixDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsNEJBQWlCLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxXQUFXLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUE7O3dCQURwSCxNQUFNLEdBQUcsU0FDMkc7d0JBRTFILEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDdEIsTUFBTSxJQUFJLEtBQUssQ0FBQyxnRUFBZ0UsQ0FBQyxDQUFDO3dCQUNwRixDQUFDO3dCQUVLLE9BQU8sR0FBWSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBRW5DLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzs0QkFDYixNQUFNLGdCQUFDLElBQUksRUFBQzt3QkFDZCxDQUFDO3dCQUVELE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO3dCQUVuQixPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFNBQXVCOzRCQUM1QyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsYUFBYSxLQUFLLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO2dDQUMxRCxTQUFTLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQzs0QkFDN0IsQ0FBQzs0QkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFNBQVMsS0FBSyxLQUFLLElBQUksU0FBUyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO2dDQUN4RSxTQUFTLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFdBQStCO29DQUN4RCxPQUFPLENBQUMsTUFBTSxJQUFJLFdBQVcsQ0FBQyxNQUFNLENBQUM7Z0NBQ3ZDLENBQUMsQ0FBQyxDQUFDOzRCQUNMLENBQUM7d0JBQ0gsQ0FBQyxDQUFDLENBQUM7d0JBRUkscUJBQU0sUUFBUSxDQUFDLEdBQUcsQ0FBQyw0QkFBaUIsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxFQUFBOzRCQUFwRSxzQkFBTyxTQUE2RCxFQUFDOzs7O0tBRXRFO0lBRUQseUJBQStCLElBQVUsRUFBRSxXQUFtQjs7Ozs7O3dCQUV0RCxJQUFJLEdBQUcsd0JBQVUsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBRXhDLFFBQVEsR0FBb0IsV0FBVyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFFcEQsTUFBTSxHQUFHLFdBQVcsQ0FBQzt3QkFFdkIsYUFBYSxHQUFHLENBQUMsQ0FBQzt3QkFFaEIsSUFBSSxHQUFHLHNCQUFPLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUscUJBQU0sQ0FBQyxFQUFFLEVBQUUsV0FBVyxDQUFDLENBQUM7d0JBQzFELHFCQUFNLFFBQVEsQ0FBQyxVQUFVLENBQUMsNEJBQWlCLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSw0QkFBaUIsQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBQTs7d0JBQXRJLE1BQU0sR0FBRyxTQUE2SDt3QkFDMUksRUFBRSxDQUFBLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUEsQ0FBQzs0QkFDeEIsU0FBUyxHQUFZLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDckMsYUFBYSxJQUFJLFNBQVMsQ0FBQyxNQUFNLENBQUM7d0JBQ3BDLENBQUM7d0JBRUssTUFBTSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMseUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDOzhCQUV6QixFQUFOLGlCQUFNOzs7NkJBQU4sQ0FBQSxvQkFBTSxDQUFBO3dCQUFYLENBQUM7d0JBQ0YsRUFBRSxHQUFHLHNCQUFPLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUscUJBQU0sQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzVELHFCQUFNLFFBQVEsQ0FBQyxVQUFVLENBQUMsNEJBQWlCLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSw0QkFBaUIsQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBQTs7d0JBQS9ILENBQUMsR0FBRyxTQUEySDt3QkFDbkksRUFBRSxDQUFBLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUEsQ0FBQzs0QkFDZCxDQUFDLEdBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUN4QixhQUFhLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDOUMsQ0FBQzs7O3dCQU5XLElBQU0sQ0FBQTs7NEJBU3BCLHNCQUFPLGFBQWEsRUFBQzs7OztLQUV0QjtJQUVELFNBQVM7SUFDVCx1QkFBNkIsV0FBd0IsRUFBRSxRQUEwQjs7Ozs7O3dCQUUvRSxRQUFRLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQzs4QkFFaEQsRUFBaEIsS0FBQSxXQUFXLENBQUMsSUFBSTs7OzZCQUFoQixDQUFBLGNBQWdCLENBQUE7d0JBQXZCLEdBQUc7d0JBRUwsb0JBQW9CLEdBQUcsc0JBQU8sQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO3dCQUdyRyxxQkFBTSxRQUFRLENBQUMsVUFBVSxDQUFDLDRCQUFpQixDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsNEJBQWlCLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxXQUFXLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsRUFBQTs7d0JBQXRKLE1BQU0sR0FBRyxTQUE2STt3QkFFNUosRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUN0QixvRkFBb0Y7NEJBQ3BGLE9BQU8sQ0FBQyxLQUFLLENBQUMsZ0VBQWdFLENBQUMsQ0FBQzt3QkFDbEYsQ0FBQzt3QkFFRyxPQUFPLEdBQVksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUVqQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7NEJBQ2IsT0FBTyxHQUFHLElBQUksc0JBQU8sQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7d0JBQ3pGLENBQUM7d0JBR0csS0FBSyxHQUFpQixJQUFJLENBQUM7d0JBQy9CLEdBQUcsQ0FBQSxPQUFvQyxFQUFiLEtBQUEsT0FBTyxDQUFDLEtBQUssRUFBYixjQUFhLEVBQWIsSUFBYTs0QkFBN0IsWUFBWTs0QkFDcEIsRUFBRSxDQUFBLENBQUMsWUFBWSxDQUFDLGFBQWEsS0FBSyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUEsQ0FBQztnQ0FDM0QsS0FBSyxHQUFHLFlBQVksQ0FBQztnQ0FDckIsS0FBSyxDQUFDOzRCQUNSLENBQUM7eUJBQ0Y7d0JBRUQsRUFBRSxDQUFBLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQSxDQUFDOzRCQUNULEtBQUssR0FBRyxJQUFJLDJCQUFZLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxFQUFFLENBQUMsQ0FBQzs0QkFDeEQsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQzVCLENBQUM7d0JBRUcsV0FBVyxHQUF1QixJQUFJLENBQUM7d0JBQzNDLEdBQUcsQ0FBQSxPQUF3QyxFQUFiLEtBQUEsS0FBSyxDQUFDLE9BQU8sRUFBYixjQUFhLEVBQWIsSUFBYTs0QkFBakMsZ0JBQWdCOzRCQUN4QixFQUFFLENBQUEsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEtBQUssR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFBLENBQUM7Z0NBQ3BDLFdBQVcsR0FBRyxnQkFBZ0IsQ0FBQztnQ0FDL0IsS0FBSyxDQUFDOzRCQUNSLENBQUM7eUJBQ0Y7d0JBRUQsRUFBRSxDQUFBLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQSxDQUFDOzRCQUNmLFdBQVcsR0FBRyxJQUFJLGlDQUFrQixDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDOzRCQUN6RCxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDbEMsQ0FBQzt3QkFFRCx3QkFBd0I7d0JBQ3hCLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO3dCQUVuQixHQUFHLENBQUEsT0FBb0MsRUFBYixLQUFBLE9BQU8sQ0FBQyxLQUFLLEVBQWIsY0FBYSxFQUFiLElBQWE7NEJBQTdCLFlBQVk7NEJBQ3BCLEdBQUcsQ0FBQSxPQUErQyxFQUFwQixLQUFBLFlBQVksQ0FBQyxPQUFPLEVBQXBCLGNBQW9CLEVBQXBCLElBQW9CO2dDQUF4QyxnQkFBZ0I7Z0NBQ3hCLE9BQU8sQ0FBQyxNQUFNLElBQUksZ0JBQWdCLENBQUMsTUFBTSxDQUFDOzZCQUMzQzt5QkFDRjt3QkFFRCxxQkFBTSxRQUFRLENBQUMsR0FBRyxDQUFDLDRCQUFpQixDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsT0FBTyxDQUFDLEVBQUE7O3dCQUE3RCxTQUE2RCxDQUFDOzs7d0JBdEQvQyxJQUFnQixDQUFBOzs7Ozs7S0EwRGxDO0lBRUQsU0FBUztJQUNUO1FBTUUsa0JBQW1CLElBQVUsRUFBUyxJQUFVO1lBQTdCLFNBQUksR0FBSixJQUFJLENBQU07WUFBUyxTQUFJLEdBQUosSUFBSSxDQUFNO1lBQzlDLElBQUksQ0FBQyxVQUFVLEdBQUcsd0JBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUM3QyxJQUFJLENBQUMsVUFBVSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsNEJBQWlCLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEdBQUcsR0FBRSxJQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUM7UUFDeEgsQ0FBQztRQUVILGVBQUM7SUFBRCxDQUFDLEFBWEQsSUFXQztJQUVELFNBQVM7SUFDVCx1QkFBdUIsUUFBa0I7UUFFdkMsSUFBTSxNQUFNLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFFL0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLElBQUksTUFBTSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDakMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLHdCQUF3QjtRQUN2QyxDQUFDO1FBRUQsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVELFNBQVM7SUFDVCxzQkFBNEIsVUFBa0IsRUFBRSxJQUFVLEVBQUUsU0FBaUI7O1lBTTNFO2dCQUNFLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztnQkFDZCxlQUFlLENBQUMsT0FBTyxDQUFDLFVBQUMsV0FBd0I7b0JBQy9DLEtBQUssSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDO2dCQUM3QixDQUFDLENBQUMsQ0FBQztnQkFDSCxNQUFNLENBQUMsS0FBSyxDQUFDO1lBQ2YsQ0FBQztZQUVELHlCQUF5QixXQUFzQjtnQkFDN0MsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO2dCQUNkLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQyxPQUFnQjtvQkFDbkMsS0FBSyxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxDQUFDO2dCQUVILE1BQU0sQ0FBQyxLQUFLLENBQUM7WUFDZixDQUFDOzs7Ozt3QkFuQkssZUFBZSxHQUFvQixXQUFXLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO3dCQUMvQixxQkFBTSxlQUFlLENBQUMsVUFBVSxDQUFDLDRCQUFpQixDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBQTs7d0JBQXhHLGVBQWUsR0FBbUIsU0FBc0U7d0JBQ2pGLHFCQUFNLGVBQWUsQ0FBQyxVQUFVLENBQUMsNEJBQWlCLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFBOzt3QkFBM0YsV0FBVyxHQUFjLFNBQWtFO3dCQW1CM0YsZ0JBQWdCLEdBQUcsbUJBQW1CLEVBQUUsQ0FBQzt3QkFDekMsWUFBWSxHQUFHLGVBQWUsQ0FBQyxXQUFXLENBQUMsQ0FBQzs2QkFFN0MsQ0FBQSxZQUFZLEtBQUssZ0JBQWdCLENBQUEsRUFBakMsd0JBQWlDOzhCQUdNLEVBQWYsbUNBQWU7Ozs2QkFBZixDQUFBLDZCQUFlLENBQUE7d0JBQTlCLFdBQVc7d0JBQ25CLHFCQUFNLGFBQWEsQ0FBQyxXQUFXLENBQUMsRUFBQTs7d0JBQWhDLFNBQWdDLENBQUM7Ozt3QkFEVixJQUFlLENBQUE7OzRCQUkxQixxQkFBTSxlQUFlLENBQUMsVUFBVSxDQUFDLDRCQUFpQixDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBQTs7d0JBQWhGLFdBQVcsR0FBRyxTQUFrRSxDQUFDO3dCQUNqRixZQUFZLEdBQUcsZUFBZSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3dCQUM1QywwRUFBMEU7d0JBQzFFLEVBQUUsQ0FBQSxDQUFDLFlBQVksS0FBSyxnQkFBZ0IsQ0FBQyxDQUFBLENBQUM7NEJBQ3BDLE1BQU0sSUFBSSxLQUFLLENBQUMsb0RBQW9ELEdBQUcsZ0JBQWdCLEdBQUcsTUFBTSxHQUFHLFlBQVksQ0FBQyxDQUFDO3dCQUNuSCxDQUFDOzs7d0JBSUMsSUFBSSxHQUFHOzRCQUNULFNBQVMsRUFBRyxTQUFTOzRCQUNyQixJQUFJLEVBQUcsVUFBVTs0QkFDakIsSUFBSSxFQUFHLElBQUk7NEJBQ1gsUUFBUSxFQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsNEJBQWlCLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQzs0QkFDMUUsUUFBUSxFQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsNEJBQWlCLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQzs0QkFDMUUsU0FBUyxFQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsNEJBQWlCLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQzs0QkFDekUsU0FBUyxFQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQzs0QkFDMUQsVUFBVSxFQUFFLFlBQVk7eUJBQ3pCLENBQUM7d0JBRUksVUFBVSxHQUFHLGFBQWEsRUFBRSxDQUFDO3dCQUVuQyxFQUFFLENBQUEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFBLENBQUM7NEJBQ2QsTUFBTSxJQUFJLEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO3dCQUMvQyxDQUFDO3dCQUVLLFFBQVEsR0FBRyxJQUFJLGlCQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBRW5DLE9BQU8sR0FBRyxJQUFJLENBQUM7d0JBRW5CLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFBLENBQUM7NEJBRVosS0FBSyxtQkFBSSxDQUFDLEtBQUs7Z0NBQ2IsT0FBTyxHQUFHLE1BQU0sQ0FBQztnQ0FDakIsS0FBSyxDQUFDOzRCQUNSLEtBQUssbUJBQUksQ0FBQyxLQUFLO2dDQUNiLE9BQU8sR0FBRyxLQUFLLENBQUM7Z0NBQ2hCLEtBQUssQ0FBQzs0QkFDUixLQUFLLG1CQUFJLENBQUMsS0FBSztnQ0FDYixPQUFPLEdBQUcsS0FBSyxDQUFDO2dDQUNoQixLQUFLLENBQUM7d0JBRVYsQ0FBQzt3QkFFSyxFQUFFLEdBQ04sVUFBVSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSTs0QkFDeEIsR0FBRzs0QkFDSCxPQUFPOzRCQUNQLEdBQUc7NEJBQ0gsUUFBUSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxhQUFhLENBQUM7NEJBQ3ZELEdBQUc7NEJBQ0gsVUFBVSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSTs0QkFDeEIsTUFBTSxDQUFDO3dCQUVULFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxZQUFZLENBQUMsQ0FBQzt3QkFFdkQsc0JBQU8sSUFBSSxFQUFDOzs7O0tBRWI7SUFFRCxxREFBcUQ7SUFFckQscUJBQWtDLElBQVUsRUFBRSxHQUFRLEVBQUUsSUFBVzs7Ozs7O3dCQUczRCxTQUFTLEdBQUcsR0FBRyxDQUFDLE1BQU0sS0FBSyxxQkFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsNEJBQVksQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQzt3QkFFN0UscUJBQXFCLEdBQUcsR0FBRyxDQUFDO3dCQUU5QixZQUFZLEdBQUcsQ0FBQyxDQUFDO3dCQUVyQixJQUFJLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTs0QkFFZixJQUFJLE9BQU8sR0FBRyxLQUFLLENBQUM7NEJBQ3BCLElBQUksY0FBYyxHQUFHLENBQUMsQ0FBQzs0QkFFdkIsRUFBRSxDQUFBLENBQUMsU0FBUyxDQUFDLENBQUEsQ0FBQztnQ0FDWixPQUFPLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFDLENBQUMsSUFBTSxNQUFNLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxXQUFXLENBQUEsQ0FBQSxDQUFDLENBQUMsQ0FBQzs0QkFDbkUsQ0FBQzs0QkFBQSxJQUFJLENBQUMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLE1BQU0sS0FBSyxxQkFBTSxDQUFDLEdBQUksQ0FBQyxDQUFBLENBQUM7Z0NBQ3BDLElBQU0sT0FBTyxHQUFHLDRCQUFZLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dDQUMvQyxPQUFPLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSyxNQUFNLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxXQUFXLENBQUEsQ0FBQSxDQUFDLENBQUMsQ0FBQztnQ0FDNUQsY0FBYyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUM7NEJBQ2xDLENBQUM7NEJBQUEsSUFBSSxDQUFDLENBQUM7Z0NBQ0wsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLEtBQUssR0FBRyxDQUFDLFdBQVcsQ0FBQzs0QkFDakQsQ0FBQzs0QkFFRCxFQUFFLENBQUEsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUEsQ0FBQztnQ0FDeEMsTUFBTSxJQUFJLEtBQUssQ0FBQyxxQkFBcUIsR0FBRyxHQUFHLENBQUMsTUFBTSxHQUFHLEdBQUcsR0FBRyxxQkFBSyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDOzRCQUNyRixDQUFDOzRCQUFBLElBQUksQ0FBQyxFQUFFLENBQUEsQ0FBQyxPQUFPLENBQUMsQ0FBQSxDQUFDO2dDQUNoQixZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLHFCQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBRTs0QkFDL0YsQ0FBQzt3QkFFSCxDQUFDLENBQUMsQ0FBQzt3QkFFSCxLQUFBLFlBQVksQ0FBQTt3QkFBSSxxQkFBTSxlQUFlLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxXQUFXLENBQUMsRUFBQTs7d0JBQTVELFlBQVksR0FBWixLQUFnQixTQUE0QyxDQUFDO3dCQUV6RCxVQUFVLEdBQUcscUJBQXFCLEdBQUcsWUFBWSxDQUFDO3dCQUV0RCxFQUFFLENBQUEsQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLENBQUEsQ0FBQzs0QkFDbEIsTUFBTSxJQUFJLEtBQUssQ0FBQyxpQkFBaUIsR0FBRSxxQkFBSyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDaEUsQ0FBQzt3QkFFRCxFQUFFLENBQUEsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUEsQ0FBQzs0QkFFeEUsRUFBRSxDQUFBLENBQUMsU0FBUyxDQUFDLENBQUEsQ0FBQztnQ0FDWixVQUFVLEdBQUcsVUFBVSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUM7NEJBQzdDLENBQUM7NEJBRUQsTUFBTSxJQUFJLEtBQUssQ0FBQyxPQUFPLEdBQUcsVUFBVSxHQUFHLGtCQUFrQixHQUFHLEdBQUcsQ0FBQyxNQUFNLEdBQUcsR0FBRyxHQUFHLHFCQUFLLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUM1RyxDQUFDO3dCQUVELHNCQUFPLElBQUksRUFBQzs7OztLQUViO0lBbkRxQiw2QkFBVyxjQW1EaEMsQ0FBQTtJQUVELCtDQUErQztJQUMvQyx3QkFBcUMsV0FBd0I7Ozs7Ozt3QkFFckQsV0FBVyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7d0JBR3pCLFVBQVUsR0FBRyxhQUFhLENBQUMsSUFBSSxRQUFRLENBQUMsV0FBVyxFQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQzt3QkFFdEYsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzs0QkFDZixNQUFNLElBQUksS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUM7d0JBQ3RELENBQUM7d0JBRUssZUFBZSxHQUFHLHdCQUFVLENBQUMsV0FBVyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBRTFELGtEQUFrRDt3QkFDbEQsV0FBVyxDQUFDLElBQUksR0FBRyxlQUFlLENBQUM7d0JBRTdCLE9BQU8sR0FBb0IsV0FBVyxDQUFDLGVBQWUsRUFBRSxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBRWhGLHFCQUFNLGFBQWEsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLEVBQUE7O3dCQUF6QyxTQUF5QyxDQUFDO3dCQUdqQixxQkFBTSxPQUFPLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxXQUFXLENBQUMsRUFBQTs7d0JBQXRFLGdCQUFnQixHQUFHLFNBQW1EO3dCQUU1RSxFQUFFLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQzs0QkFDdEIsTUFBTSxJQUFJLEtBQUssQ0FBQyxpRkFBaUYsQ0FBQyxDQUFDOzRCQUNuRyxlQUFlO3dCQUNqQixDQUFDO3dCQUVELHNCQUFPLElBQUksRUFBQzs7OztLQUViO0lBOUJxQixnQ0FBYyxpQkE4Qm5DLENBQUE7SUFFRCx1QkFBb0MsT0FBYSxFQUFFLElBQVUsRUFBRSxHQUFJOzs7Ozs7d0JBRWpFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzs0QkFDVixNQUFNLElBQUksS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUM7d0JBQ3hDLENBQUM7d0JBRUssSUFBSSxHQUFHLHdCQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7NkJBRXhDLENBQUEsR0FBRyxJQUFJLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFBLEVBQXJCLHdCQUFxQjt3QkFDZixxQkFBTSxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxFQUFBOzRCQUR0QixxRUFBcUU7b0JBQzlGLHNCQUFPLFNBQXdDLEVBQUM7NEJBRzNDLHFCQUFNLGdCQUFnQixDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUE7NEJBQXRELHNCQUFPLFNBQStDLEVBQUM7Ozs7S0FFeEQ7SUFkcUIsK0JBQWEsZ0JBY2xDLENBQUE7SUFFRCx3RUFBd0U7SUFDeEUsMkJBQXdDLFVBQWtCLEVBQUUsSUFBVSxFQUFFLGlCQUF5Qjs7Ozs7O3dCQUV6RixlQUFlLEdBQW9CLFdBQVcsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7d0JBRXhELHFCQUFNLGVBQWU7aUNBQ2pDLFVBQVUsQ0FBQyw0QkFBaUIsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLDRCQUFpQixDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsV0FBVyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUE7O3dCQUQ1SCxNQUFNLEdBQUcsU0FDbUg7d0JBRWxJLEVBQUUsQ0FBQSxDQUFDLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUEsQ0FBQzs0QkFDakMsTUFBTSxnQkFBQztvQ0FDTCxXQUFXLEVBQUcsRUFBRTtvQ0FDaEIsV0FBVyxFQUFHLENBQUM7aUNBQ2hCLEVBQUE7d0JBQ0gsQ0FBQzt3QkFFSyxXQUFXLEdBQWdCLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFFdkMsV0FBVyxHQUFlLEVBQUUsQ0FBQzt3QkFFM0IsSUFBSSxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUM7d0JBRTlCLEdBQUcsQ0FBQSxPQUFnQixFQUFKLGFBQUksRUFBSixrQkFBSSxFQUFKLElBQUk7NEJBQVgsR0FBRzs0QkFFSCxPQUFPLEdBQUcsSUFBSSxzQkFBTyxDQUFDLFVBQVUsRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7NEJBQzNFLE9BQU8sQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQzs0QkFFNUIsT0FBTyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7NEJBRWIsT0FBTyxHQUF5QixFQUFFLENBQUM7NEJBQ3pDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxpQ0FBa0IsQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDOzRCQUV6RCxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLDJCQUFZLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFBOzRCQUV4RSxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3lCQUMzQjt3QkFFRCxzQkFBTztnQ0FDTCxXQUFXLEVBQUcsV0FBVztnQ0FDekIsV0FBVyxFQUFHLFdBQVcsQ0FBQyxLQUFLOzZCQUNoQyxFQUFDOzs7O0tBQ0g7SUF2Q3FCLG1DQUFpQixvQkF1Q3RDLENBQUE7SUFFRCwyQkFBMkI7SUFDM0Isd0JBQXFDLFVBQWtCLEVBQUUsSUFBVSxFQUFFLGlCQUEwQjs7Ozs7O3dCQUV2RixlQUFlLEdBQW9CLFdBQVcsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7d0JBRW5FLE1BQU0sR0FBa0IsRUFBRSxDQUFDO3dCQUUvQixpQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQzs2QkFFckUsQ0FBQSxDQUFDLGlCQUFpQixJQUFJLGlCQUFpQixLQUFLLEVBQUUsQ0FBQSxFQUE5Qyx3QkFBOEM7d0JBQ3RDLHFCQUFNLGVBQWUsQ0FBQyxVQUFVLENBQUMsNEJBQWlCLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFBOzt3QkFBL0UsTUFBTSxHQUFHLFNBQXNFLENBQUM7OzRCQUV2RSxxQkFBTSxlQUFlLENBQUMsVUFBVSxDQUFDLDRCQUFpQixDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsNEJBQWlCLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxXQUFXLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBQTs7d0JBQTlKLE1BQU0sR0FBRyxTQUFxSixDQUFDOzs7d0JBR2pLLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFDLEVBQUUsQ0FBQzs0QkFDZixNQUFNLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUM7d0JBQ25FLENBQUMsQ0FBQyxDQUFDO3dCQUVILHNCQUFPLE1BQU0sRUFBQzs7OztLQUVmO0lBcEJxQixnQ0FBYyxpQkFvQm5DLENBQUE7SUFFRCwwQ0FBMEM7SUFDMUMsMEJBQXVDLFVBQWtCLEVBQUUsSUFBVSxFQUFFLFdBQWtCOzs7Ozs7d0JBRWpGLGVBQWUsR0FBb0IsV0FBVyxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFFbkUsV0FBVyxHQUFlLEVBQUUsQ0FBQzs2QkFFN0IsQ0FBQyxXQUFXLEVBQVosd0JBQVk7d0JBRUEscUJBQU0sZUFBZSxDQUFDLFVBQVUsQ0FBQyw0QkFBaUIsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEVBQUE7O3dCQURoRixvQkFBb0I7d0JBQ3BCLFdBQVcsR0FBRyxTQUFrRSxDQUFDOzs7d0JBRzNFLGFBQWEsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLHlCQUFTLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQzt3QkFDaEQsQ0FBQyxHQUFHLENBQUM7Ozs2QkFBRSxDQUFBLENBQUMsR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFBO3dCQUNoQyxLQUFLLEdBQUcsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUdoQixxQkFBTSxlQUFlO2lDQUNqQyxVQUFVLENBQUMsNEJBQWlCLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSw0QkFBaUIsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFBOzt3QkFEMUcsTUFBTSxHQUFHLFNBQ2lHO3dCQUNoSCxXQUFXLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzs7O3dCQU5ELENBQUMsRUFBRSxDQUFBOzs7d0JBVTNDLFdBQVcsR0FBRyxDQUFDLENBQUM7d0JBQ3BCLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQyxHQUFHOzRCQUN0QixXQUFXLElBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQzt3QkFDNUIsQ0FBQyxDQUFDLENBQUM7d0JBRUgsc0JBQU87Z0NBQ0wsV0FBVyxFQUFHLFdBQVc7Z0NBQ3pCLFdBQVcsRUFBRyxXQUFXOzZCQUMxQixFQUFDOzs7O0tBQ0g7SUEvQnFCLGtDQUFnQixtQkErQnJDLENBQUE7SUFFRCw4REFBOEQ7SUFDOUQsZUFBNEIsSUFBYzs7Ozs7O3dCQUV4QyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7NEJBQ1YsSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQzt3QkFDcEIsQ0FBQzt3QkFFSyxPQUFPLEdBQUc7NEJBQ2QsVUFBVSxDQUFDLElBQUksRUFBRSxtQkFBSSxDQUFDLEtBQUssQ0FBQzs0QkFDNUIsVUFBVSxDQUFDLElBQUksRUFBRSxtQkFBSSxDQUFDLEtBQUssQ0FBQzs0QkFDNUIsVUFBVSxDQUFDLElBQUksRUFBRSxtQkFBSSxDQUFDLEtBQUssQ0FBQzt5QkFDN0IsQ0FBQzt3QkFFSSxRQUFRLEdBQW1CLEVBQUUsQ0FBQzt3QkFFcEMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFBLEtBQUs7NEJBRW5CLElBQUksU0FBUyxHQUFvQixJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBRTdDLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQ0FDZixTQUFTLEdBQUcsSUFBSSxrQ0FBZSxDQUFDLEtBQUssRUFBRSxjQUFjLEVBQUUsNEJBQWlCLENBQUMsV0FBVyxDQUFDLENBQUM7NEJBQ3hGLENBQUM7NEJBQUMsSUFBSSxDQUFDLENBQUM7Z0NBQ04sT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQ3JCLENBQUM7NEJBRUQsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQzt3QkFFM0MsQ0FBQyxDQUFDLENBQUM7d0JBRUgscUJBQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsRUFBQTs7d0JBQTNCLFNBQTJCLENBQUM7d0JBRTVCLHNCQUFPLElBQUksRUFBQzs7OztLQUViO0lBaENxQix1QkFBSyxRQWdDMUIsQ0FBQTtJQUVELHdFQUF3RTtJQUN4RSxtQkFBZ0MsSUFBVSxFQUFFLElBQVc7Ozs7Ozt3QkFFckQsSUFBSSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDO3dCQUUxQixTQUFTLEdBQWMsSUFBSSxRQUFRLENBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO3dCQUdqRCxVQUFVLEdBQWEsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDOzZCQUVsRCxVQUFVLEVBQVYsd0JBQVU7d0JBQ0MscUJBQU0sWUFBWSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLEVBQUE7O3dCQUF2RixTQUFPLFNBQWdGO3dCQUM3RixzQkFBTztnQ0FDTCxFQUFFLEVBQUcsVUFBVSxDQUFDLFNBQVM7Z0NBQ3pCLElBQUksRUFBRyxNQUFJOzZCQUNaLEVBQUM7O3dCQUdKLCtCQUErQjt3QkFFL0IsU0FBUyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7d0JBRWpDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBRXJFLHFCQUFNLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLFNBQVMsQ0FBQyxFQUFBOzt3QkFBcEYsSUFBSSxHQUFHLFNBQTZFO3dCQUUxRixzQkFBTztnQ0FDTCxFQUFFLEVBQUcsU0FBUyxDQUFDLFNBQVM7Z0NBQ3hCLElBQUksRUFBRyxJQUFJOzZCQUNaLEVBQUM7Ozs7S0FFSDtJQTlCcUIsMkJBQVMsWUE4QjlCLENBQUE7SUFFRCx1REFBdUQ7SUFDdkQsMkJBQXdDLElBQVUsRUFBRSxhQUFxQjs7Ozs7O3dCQUV2RSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7NEJBQ1YsTUFBTSxJQUFJLEtBQUssQ0FBQyxpREFBaUQsQ0FBQyxDQUFDO3dCQUNyRSxDQUFDO3dCQUVELEVBQUUsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDL0IsTUFBTSxJQUFJLEtBQUssQ0FBQyw2REFBNkQsQ0FBQyxDQUFDO3dCQUNqRixDQUFDO3dCQUVLLFdBQVcsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO3dCQUd6QixVQUFVLEdBQUcsYUFBYSxDQUFDLElBQUksUUFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQzt3QkFFMUUsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzs0QkFDZixNQUFNLElBQUksS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUM7d0JBQ3RELENBQUM7d0JBRUssT0FBTyxHQUFvQixXQUFXLENBQUMsd0JBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFFdkUscUJBQU0sT0FBTztpQ0FDekIsVUFBVSxDQUFDLDRCQUFpQixDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsNEJBQWlCLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxXQUFXLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUE7O3dCQUR4SCxNQUFNLEdBQUcsU0FDK0c7d0JBRTlILEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDdEIsTUFBTSxJQUFJLEtBQUssQ0FBQyxtRUFBbUUsQ0FBQyxDQUFDO3dCQUN2RixDQUFDO3dCQUVLLFdBQVcsR0FBZ0IsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUUzQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7NEJBQ2pCLE1BQU0sSUFBSSxLQUFLLENBQUMsK0RBQStELENBQUMsQ0FBQzt3QkFDbkYsQ0FBQzt3QkFHUSxDQUFDLEdBQUcsQ0FBQzs7OzZCQUFHLENBQUEsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFBO3dCQUNwQyxHQUFHLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDVCxxQkFBTSxtQ0FBbUMsQ0FBQyxPQUFPLEVBQUUsV0FBVyxFQUFFLEdBQUcsQ0FBQyxFQUFBOzt3QkFBckYsY0FBYyxHQUFHLFNBQW9FO3dCQUMzRixFQUFFLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7NEJBQ3BCLE1BQU0sSUFBSSxLQUFLLENBQUMsMERBQTBELENBQUMsQ0FBQzs0QkFDNUUsZUFBZTt3QkFDakIsQ0FBQzs7O3dCQU40QyxDQUFDLEVBQUUsQ0FBQTs7O3dCQVNsRCxzQ0FBc0M7d0JBQ3RDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO3dCQUN0QixxQkFBTSxPQUFPLENBQUMsR0FBRyxDQUFDLDRCQUFpQixDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsV0FBVyxDQUFDLEVBQUE7NEJBQTNFLHNCQUFPLFNBQW9FLEVBQUM7Ozs7S0FFN0U7SUFoRHFCLG1DQUFpQixvQkFnRHRDLENBQUE7SUFFRDtRQUVFLElBQUcsQ0FBQztZQUNGLElBQU0sR0FBRyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsNEJBQWlCLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzVFLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3pCLENBQUM7UUFBQSxLQUFLLENBQUEsQ0FBQyxHQUFHLENBQUMsQ0FBQSxDQUFDO1lBQ1YsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNkLENBQUM7SUFFSCxDQUFDO0lBVGUsK0JBQWEsZ0JBUzVCLENBQUE7SUFFRCxlQUF1QixJQUFVO1FBQy9CLFlBQVksQ0FBQyxPQUFPLENBQUMsNEJBQWlCLENBQUMsYUFBYSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDL0UsWUFBWSxDQUFDLE9BQU8sQ0FBQyw0QkFBaUIsQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMvRSxZQUFZLENBQUMsT0FBTyxDQUFDLDRCQUFpQixDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM3RixZQUFZLENBQUMsT0FBTyxDQUFDLDRCQUFpQixDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ3hGLENBQUM7SUFMZSx1QkFBSyxRQUtwQixDQUFBO0lBRUQ7UUFDRSxZQUFZLENBQUMsT0FBTyxDQUFDLDRCQUFpQixDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDcEUsWUFBWSxDQUFDLE9BQU8sQ0FBQyw0QkFBaUIsQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3BFLFlBQVksQ0FBQyxPQUFPLENBQUMsNEJBQWlCLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztRQUNsRSxZQUFZLENBQUMsT0FBTyxDQUFDLDRCQUFpQixDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDdEUsQ0FBQztJQUxlLHdCQUFNLFNBS3JCLENBQUE7QUFFSCxDQUFDLEVBaG9CYSxpQkFBaUIsR0FBakIseUJBQWlCLEtBQWpCLHlCQUFpQixRQWdvQjlCIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQge09mZmxpbmVEYXRhYmFzZX0gZnJvbSAnLi9kYXRhYmFzZS9vZmZsaW5lLWRhdGFiYXNlJztcbmltcG9ydCB7XG4gIEJldCwgVHJhbnNhY3Rpb24sIERyYXcsIFJhZmZsZSwgU3VtbWFyeSwgU3VtbWFyeVRyYWNlLCBTdW1tYXJ5VHJhY2VEZXRhaWwsXG4gIFVzZXJcbn0gZnJvbSAnLi4vLi4vbW9kZWxzL291dGxldC5tb2RlbCc7XG5pbXBvcnQge2RhdGVOb1RpbWV9IGZyb20gJy4uL2hlbHBlcnMvZGF0ZS1oZWxwZXInO1xuaW1wb3J0IHtwYWRkeSwgcmFtYm9saXRvLCByYW1ib2xpdG9JbnR9IGZyb20gJy4uL2hlbHBlcnMvbnVtYmVyLWhlbHBlcic7XG5pbXBvcnQge09GRkxJTkVfQ09OU1RBTlRTfSBmcm9tICcuLi9oZWxwZXJzL2NvbnRhbnRzJztcbmltcG9ydCB7RGF0ZVBpcGV9IGZyb20gXCJAYW5ndWxhci9jb21tb25cIjtcbmltcG9ydCAqIGFzIGxvY2FsU3RvcmFnZSBmcm9tICduYXRpdmVzY3JpcHQtbG9jYWxzdG9yYWdlJztcbmNvbnN0IF9vZmZsaW5lVGFibGVzID0gW1xuICB7XG4gICAgbmFtZTogT0ZGTElORV9DT05TVEFOVFMuVEFCTEVTLlRSQU5TQUNUSU9OLFxuICAgIHBhcmFtOiB7IGtleVBhdGg6ICdfc3lzdGVtSGVhZGVyLmRvY3VtZW50SWQnLCBhdXRvSW5jcmVtZW50OiBmYWxzZX0sXG4gICAgaW5kZXhlcyA6IFtcbiAgICAgIHsgbmFtZTogT0ZGTElORV9DT05TVEFOVFMuSU5ERVhFUy5ET0NVTUVOVF9JRCwgcGF0aDogJ19zeXN0ZW1IZWFkZXIuZG9jdW1lbnRJZCcsIHVuaXF1ZTogdHJ1ZSB9LFxuICAgICAgeyBuYW1lOiBPRkZMSU5FX0NPTlNUQU5UUy5JTkRFWEVTLlRSQU5TQUNUSU9OX0lELCBwYXRoOiAndHJhbnNhY3Rpb25JZCcsIHVuaXF1ZTogdHJ1ZSB9XG4gICAgXVxuICB9LFxuICB7XG4gICAgbmFtZTogT0ZGTElORV9DT05TVEFOVFMuVEFCTEVTLlNVTU1BUlksXG4gICAgcGFyYW06IHsga2V5UGF0aDogJ2NvbWJpbmF0aW9uSWQnLCBhdXRvSW5jcmVtZW50OiBmYWxzZX0sXG4gICAgaW5kZXhlcyA6IFtcbiAgICAgIC8vIGRvY3VtZW50IHNwZWNpZmljIGluZGV4XG4gICAgICB7IG5hbWU6IE9GRkxJTkVfQ09OU1RBTlRTLklOREVYRVMuQ09NQklOQVRJT05fSUQsIHBhdGg6ICdjb21iaW5hdGlvbklkJywgdW5pcXVlOiB0cnVlIH0sXG4gICAgICB7IG5hbWU6IE9GRkxJTkVfQ09OU1RBTlRTLklOREVYRVMuQ09NQklOQVRJT04sIHBhdGg6ICdjb21iaW5hdGlvbicsIHVuaXF1ZTogZmFsc2UgfVxuICAgIF1cbiAgfVxuXTtcblxuZXhwb3J0IG1vZHVsZSBPZmZsaW5lQVBJU2VydmljZSB7XG5cbiAgLy8gaGVscGVyXG4gIC8vIHdpbGwgY29udGFpbiBhbGwgZGF0YWJhc2UgY29ubmVjdGlvbnMgdG8gZGlmZmVyZW50IGRhdGFiYXNlcyAoZWFjaCBkYXRhYmFzZSBpcyBjcmVhdGVkIGZvciBhIGRyYXcpIGogc2FcbiAgY29uc3QgcG9vbCA9IHt9O1xuXG4gIC8vIGhlbHBlclxuICBmdW5jdGlvbiBnZXREQkluZGV4KGRhdGVNaWxsaXM6IG51bWJlciwgZHJhdzogRHJhdykge1xuXG4gICAgY29uc3QgZHJhd1N0ciA9IGRyYXcgPyBkcmF3ICsgJycgOiAnJztcbiAgICBjb25zdCBvdXRsZXRJZCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKE9GRkxJTkVfQ09OU1RBTlRTLkxPQ0FMX1NUT1JBR0UuT1VUTEVUX0lEKTtcblxuICAgIHJldHVybiBPRkZMSU5FX0NPTlNUQU5UUy5JREJfTkFNRV9QUkVGSVggKyAnLScgKyBvdXRsZXRJZCArICctJyArIGRhdGVNaWxsaXMgKyAnLScgKyBkcmF3U3RyO1xuXG4gIH1cblxuICAvLyBkb3dubG9hZChqc29uRGF0YSwgJ2pzb24udHh0JywgJ3RleHQvcGxhaW4nKTtcbiAgZnVuY3Rpb24gZG93bmxvYWQoY29udGVudCwgZmlsZU5hbWUsIGNvbnRlbnRUeXBlKSB7XG4gICAgY29uc3QgYSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKTtcbiAgICBjb25zdCBmaWxlID0gbmV3IEJsb2IoW2NvbnRlbnRdLCB7dHlwZTogY29udGVudFR5cGV9KTtcbiAgICBhLmhyZWYgPSBVUkwuY3JlYXRlT2JqZWN0VVJMKGZpbGUpO1xuICAgIGEuZG93bmxvYWQgPSBmaWxlTmFtZTtcbiAgICBhLmNsaWNrKCk7XG4gIH1cblxuICAvLyBoZWxwZXJcbiAgZnVuY3Rpb24gZ2V0RGF0YWJhc2UoZGF0ZU1pbGxpczogbnVtYmVyLCBkcmF3OiBEcmF3KSB7XG5cbiAgICBjb25zdCBpbmRleCA9IGdldERCSW5kZXgoZGF0ZU1pbGxpcywgZHJhdyk7XG4gICAgbGV0IGlkYjogT2ZmbGluZURhdGFiYXNlID0gcG9vbFtpbmRleF07XG5cbiAgICBpZiAoIWlkYikge1xuICAgICAgaWRiID0gbmV3IE9mZmxpbmVEYXRhYmFzZShpbmRleCwgX29mZmxpbmVUYWJsZXMsIE9GRkxJTkVfQ09OU1RBTlRTLklEQl9WRVJTSU9OKTtcbiAgICAgIHBvb2xbaW5kZXhdID0gaWRiO1xuICAgIH1cblxuICAgIHJldHVybiBpZGI7XG5cbiAgfVxuXG4gIC8vIGhlbHBlclxuICBhc3luYyBmdW5jdGlvbiB1cGRhdGVTdW1tYXJ5T25UcmFuc2FjdGlvbkNhbmNlbGxlZChkYXRhYmFzZTogT2ZmbGluZURhdGFiYXNlLCB0cmFuc2FjdGlvbjogVHJhbnNhY3Rpb24sIGJldDogQmV0KSB7XG5cbiAgICAvLyBnZXQgdGhlIHN1bW1hcnkgaWQgd2hpY2ggaXMganVzdCBhIGNvbWJpbmF0aW9uIG9mIGJldCBhbmQgcmFmZmxlXG4gICAgY29uc3QgY29tYmluYXRpb25JZCA9IFN1bW1hcnkuY29tYmluYXRpb25JZCh0cmFuc2FjdGlvbi5kYXRlLCB0cmFuc2FjdGlvbi5kcmF3LCBiZXQucmFmZmxlLCBiZXQuY29tYmluYXRpb24pO1xuXG4gICAgLy8gc2VhcmNoIGZvciB0aGUgY29tYmluYXRpb24gc3VtbWFyeSBpZiBpdCBhbHJlYWR5IGV4aXN0XG4gICAgY29uc3QgcmVzdWx0ID0gYXdhaXQgZGF0YWJhc2VcbiAgICAgIC5nZXRFbnRyaWVzKE9GRkxJTkVfQ09OU1RBTlRTLlRBQkxFUy5TVU1NQVJZLCBPRkZMSU5FX0NPTlNUQU5UUy5JTkRFWEVTLkNPTUJJTkFUSU9OX0lELCBJREJLZXlSYW5nZS5vbmx5KGNvbWJpbmF0aW9uSWQpKTtcblxuICAgIGlmIChyZXN1bHQubGVuZ3RoID4gMSkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdGb3VuZCBhbiBhYm5vcm1hbCBudW1iZXIgb2Ygc3VtbWFyaWVzIGZvciBhIGdpdmVuIGNvbWJpbmF0aW9uLicpO1xuICAgIH1cblxuICAgIGNvbnN0IHN1bW1hcnk6IFN1bW1hcnkgPSByZXN1bHRbMF07XG5cbiAgICBpZiAoIXN1bW1hcnkpIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIHN1bW1hcnkuYW1vdW50ID0gMDtcblxuICAgIHN1bW1hcnkudHJhY2UuZm9yRWFjaCgodHJhY2VJdGVtOiBTdW1tYXJ5VHJhY2UpID0+IHtcbiAgICAgIGlmICh0cmFjZUl0ZW0udHJhbnNhY3Rpb25JZCA9PT0gdHJhbnNhY3Rpb24udHJhbnNhY3Rpb25JZCkge1xuICAgICAgICB0cmFjZUl0ZW0uY2FuY2VsbGVkID0gdHJ1ZTtcbiAgICAgIH0gZWxzZSBpZiAodHJhY2VJdGVtLmNhbmNlbGxlZCA9PT0gZmFsc2UgfHwgdHJhY2VJdGVtLmNhbmNlbGxlZCA9PSBudWxsKSB7XG4gICAgICAgIHRyYWNlSXRlbS5kZXRhaWxzLmZvckVhY2goKHRyYWNlRGV0YWlsOiBTdW1tYXJ5VHJhY2VEZXRhaWwpID0+IHtcbiAgICAgICAgICBzdW1tYXJ5LmFtb3VudCArPSB0cmFjZURldGFpbC5hbW91bnQ7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgcmV0dXJuIGF3YWl0IGRhdGFiYXNlLnB1dChPRkZMSU5FX0NPTlNUQU5UUy5UQUJMRVMuU1VNTUFSWSwgc3VtbWFyeSk7XG5cbiAgfVxuXG4gIGFzeW5jIGZ1bmN0aW9uIGdldEN1cnJlbnRWYWx1ZShkcmF3OiBEcmF3LCBjb21iaW5hdGlvbjogbnVtYmVyICl7XG5cbiAgICBjb25zdCBkYXRlID0gZGF0ZU5vVGltZShuZXcgRGF0ZSgpKS5nZXRUaW1lKCk7XG5cbiAgICBjb25zdCBkYXRhYmFzZTogT2ZmbGluZURhdGFiYXNlID0gZ2V0RGF0YWJhc2UoZGF0ZSwgZHJhdyk7XG5cbiAgICBjb25zdCB0YXJnZXQgPSBjb21iaW5hdGlvbjsgLy9zM3IgYW5kIHMzXG5cbiAgICBsZXQgX2N1cnJlbnRWYWx1ZSA9IDA7XG5cbiAgICBjb25zdCBzM0lkID0gU3VtbWFyeS5jb21iaW5hdGlvbklkKGRhdGUsIGRyYXcsIFJhZmZsZS5TMywgY29tYmluYXRpb24pO1xuICAgIGxldCByZXN1bHQgPSBhd2FpdCBkYXRhYmFzZS5nZXRFbnRyaWVzKE9GRkxJTkVfQ09OU1RBTlRTLlRBQkxFUy5TVU1NQVJZLCBPRkZMSU5FX0NPTlNUQU5UUy5JTkRFWEVTLkNPTUJJTkFUSU9OX0lELCBJREJLZXlSYW5nZS5vbmx5KHMzSWQpKTtcbiAgICBpZihyZXN1bHQgJiYgcmVzdWx0Lmxlbmd0aCA+IDApe1xuICAgICAgY29uc3QgczNTdW1tYXJ5OiBTdW1tYXJ5ID0gcmVzdWx0WzBdO1xuICAgICAgX2N1cnJlbnRWYWx1ZSArPSBzM1N1bW1hcnkuYW1vdW50O1xuICAgIH1cblxuICAgIGNvbnN0IHJhbWJvbCA9IEFycmF5LmZyb20ocmFtYm9saXRvKHRhcmdldCkpO1xuXG4gICAgZm9yIChsZXQgaSBvZiByYW1ib2wpe1xuICAgICAgY29uc3QgaWQgPSBTdW1tYXJ5LmNvbWJpbmF0aW9uSWQoZGF0ZSwgZHJhdywgUmFmZmxlLlMzUiwgTnVtYmVyKGkpKTtcbiAgICAgIGxldCByID0gYXdhaXQgZGF0YWJhc2UuZ2V0RW50cmllcyhPRkZMSU5FX0NPTlNUQU5UUy5UQUJMRVMuU1VNTUFSWSwgT0ZGTElORV9DT05TVEFOVFMuSU5ERVhFUy5DT01CSU5BVElPTl9JRCwgSURCS2V5UmFuZ2Uub25seShpZCkpO1xuICAgICAgaWYociAmJiByLmxlbmd0aCA+IDApe1xuICAgICAgICBjb25zdCBzOiBTdW1tYXJ5ID0gclswXTtcbiAgICAgICAgX2N1cnJlbnRWYWx1ZSArPSAocy5hbW91bnQgLyByYW1ib2wubGVuZ3RoKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gX2N1cnJlbnRWYWx1ZTtcblxuICB9XG5cbiAgLy8gaGVscGVyXG4gIGFzeW5jIGZ1bmN0aW9uIHVwZGF0ZVN1bW1hcnkodHJhbnNhY3Rpb246IFRyYW5zYWN0aW9uLCBkYXRhYmFzZT86IE9mZmxpbmVEYXRhYmFzZSwgKSB7XG5cbiAgICBkYXRhYmFzZSA9IGRhdGFiYXNlID8gZGF0YWJhc2UgOiBnZXREYXRhYmFzZSh0cmFuc2FjdGlvbi5kYXRlLCB0cmFuc2FjdGlvbi5kcmF3KTtcblxuICAgIGZvcihjb25zdCBiZXQgb2YgdHJhbnNhY3Rpb24uYmV0cyl7XG5cbiAgICAgIGNvbnN0IHN1bW1hcnlDb21iaW5hdGlvbklEID0gU3VtbWFyeS5jb21iaW5hdGlvbklkKHRyYW5zYWN0aW9uLmRhdGUsIHRyYW5zYWN0aW9uLmRyYXcsIGJldC5yYWZmbGUsIGJldC5jb21iaW5hdGlvbik7XG5cbiAgICAgIC8vIHNlYXJjaCBmb3IgdGhlIGNvbWJpbmF0aW9uIHN1bW1hcnkgaWYgaXQgYWxyZWFkeSBleGlzdFxuICAgICAgY29uc3QgcmVzdWx0ID0gYXdhaXQgZGF0YWJhc2UuZ2V0RW50cmllcyhPRkZMSU5FX0NPTlNUQU5UUy5UQUJMRVMuU1VNTUFSWSwgT0ZGTElORV9DT05TVEFOVFMuSU5ERVhFUy5DT01CSU5BVElPTl9JRCwgSURCS2V5UmFuZ2Uub25seShzdW1tYXJ5Q29tYmluYXRpb25JRCkpO1xuXG4gICAgICBpZiAocmVzdWx0Lmxlbmd0aCA+IDEpIHtcbiAgICAgICAgLy8gdGhyb3cgbmV3IEVycm9yKCdGb3VuZCBhbiBhYm5vcm1hbCBudW1iZXIgb2Ygc3VtbWFyaWVzIGZvciBhIGdpdmVuIGNvbWJpbmF0aW9uJyk7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ0ZvdW5kIGFuIGFibm9ybWFsIG51bWJlciBvZiBzdW1tYXJpZXMgZm9yIGEgZ2l2ZW4gY29tYmluYXRpb24uJyk7XG4gICAgICB9XG5cbiAgICAgIGxldCBzdW1tYXJ5OiBTdW1tYXJ5ID0gcmVzdWx0WzBdO1xuXG4gICAgICBpZiAoIXN1bW1hcnkpIHtcbiAgICAgICAgc3VtbWFyeSA9IG5ldyBTdW1tYXJ5KHRyYW5zYWN0aW9uLmRhdGUsIHRyYW5zYWN0aW9uLmRyYXcsIGJldC5yYWZmbGUsIGJldC5jb21iaW5hdGlvbik7XG4gICAgICB9XG5cbiAgICAgIC8vZm9jdXMgb24gYWRkaW5nIHRoZSB0cmFjZSBkZXRhaWxcbiAgICAgIGxldCB0cmFjZTogU3VtbWFyeVRyYWNlID0gbnVsbDtcbiAgICAgIGZvcihjb25zdCBjdXJyZW50VHJhY2Ugb2Ygc3VtbWFyeS50cmFjZSl7XG4gICAgICAgIGlmKGN1cnJlbnRUcmFjZS50cmFuc2FjdGlvbklkID09PSB0cmFuc2FjdGlvbi50cmFuc2FjdGlvbklkKXtcbiAgICAgICAgICB0cmFjZSA9IGN1cnJlbnRUcmFjZTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZighdHJhY2Upe1xuICAgICAgICB0cmFjZSA9IG5ldyBTdW1tYXJ5VHJhY2UodHJhbnNhY3Rpb24udHJhbnNhY3Rpb25JZCwgW10pO1xuICAgICAgICBzdW1tYXJ5LnRyYWNlLnB1c2godHJhY2UpO1xuICAgICAgfVxuXG4gICAgICBsZXQgdHJhY2VEZXRhaWw6IFN1bW1hcnlUcmFjZURldGFpbCA9IG51bGw7XG4gICAgICBmb3IoY29uc3QgY3VycmVudFRyYWNlSXRlbSBvZiB0cmFjZS5kZXRhaWxzKXtcbiAgICAgICAgaWYoY3VycmVudFRyYWNlSXRlbS5iZXRJZCA9PT0gYmV0LmlkKXtcbiAgICAgICAgICB0cmFjZURldGFpbCA9IGN1cnJlbnRUcmFjZUl0ZW07XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYoIXRyYWNlRGV0YWlsKXtcbiAgICAgICAgdHJhY2VEZXRhaWwgPSBuZXcgU3VtbWFyeVRyYWNlRGV0YWlsKGJldC5pZCwgYmV0LmFtb3VudCk7XG4gICAgICAgIHRyYWNlLmRldGFpbHMucHVzaCh0cmFjZURldGFpbCk7XG4gICAgICB9XG5cbiAgICAgIC8vIHVwZGF0ZSBzdW1tYXJ5IGFtb3VudFxuICAgICAgc3VtbWFyeS5hbW91bnQgPSAwO1xuXG4gICAgICBmb3IoY29uc3QgY3VycmVudFRyYWNlIG9mIHN1bW1hcnkudHJhY2Upe1xuICAgICAgICBmb3IoY29uc3QgY3VycmVudFRyYWNlSXRlbSBvZiBjdXJyZW50VHJhY2UuZGV0YWlscyl7XG4gICAgICAgICAgc3VtbWFyeS5hbW91bnQgKz0gY3VycmVudFRyYWNlSXRlbS5hbW91bnQ7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgYXdhaXQgZGF0YWJhc2UucHV0KE9GRkxJTkVfQ09OU1RBTlRTLlRBQkxFUy5TVU1NQVJZLCBzdW1tYXJ5KTtcblxuICAgIH1cblxuICB9XG5cbiAgLy8gaGVscGVyXG4gIGNsYXNzIERyYXdEYXRhIHtcblxuICAgIHB1YmxpYyBkb2N1bWVudElkOiBzdHJpbmc7XG4gICAgcHVibGljIGRhdGVOb1RpbWU6IG51bWJlcjtcbiAgICBwdWJsaWMgY2xvc2VDb2RlOiBudW1iZXI7XG5cbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZGF0ZTogRGF0ZSwgcHVibGljIGRyYXc6IERyYXcpIHtcbiAgICAgIHRoaXMuZGF0ZU5vVGltZSA9IGRhdGVOb1RpbWUoZGF0ZSkuZ2V0VGltZSgpO1xuICAgICAgdGhpcy5kb2N1bWVudElkID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oT0ZGTElORV9DT05TVEFOVFMuTE9DQUxfU1RPUkFHRS5PVVRMRVRfSUQpICsgJy0nICt0aGlzLmRhdGVOb1RpbWUgKyAnLScgKyBkcmF3O1xuICAgIH1cblxuICB9XG5cbiAgLy8gaGVscGVyXG4gIGZ1bmN0aW9uIGdldENsb3NlZERyYXcoZHJhd0RhdGE6IERyYXdEYXRhKTogRHJhd0RhdGEge1xuXG4gICAgY29uc3Qgc3RvcmVkID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oYnRvYShkcmF3RGF0YS5kb2N1bWVudElkKSk7XG5cbiAgICBpZiAoIXN0b3JlZCB8fCBzdG9yZWQgPT09ICdudWxsJykge1xuICAgICAgcmV0dXJuIG51bGw7IC8vIHJlamVjdCBubyBhY3RpdmUgZHJhd1xuICAgIH1cblxuICAgIHJldHVybiBKU09OLnBhcnNlKGF0b2Ioc3RvcmVkKSk7XG4gIH1cblxuICAvLyBoZWxwZXJcbiAgYXN5bmMgZnVuY3Rpb24gZG93bmxvYWREcmF3KGRhdGVNaWxsaXM6IG51bWJlciwgZHJhdzogRHJhdywgY2xvc2VDb2RlOiBudW1iZXIpIHtcblxuICAgIGNvbnN0IG9mZmxpbmVEYXRhYmFzZTogT2ZmbGluZURhdGFiYXNlID0gZ2V0RGF0YWJhc2UoZGF0ZU1pbGxpcywgZHJhdyk7XG4gICAgY29uc3QgdHJhbnNhY3Rpb25MaXN0OiBUcmFuc2FjdGlvbiBbXSA9IGF3YWl0IG9mZmxpbmVEYXRhYmFzZS5nZXRFbnRyaWVzKE9GRkxJTkVfQ09OU1RBTlRTLlRBQkxFUy5UUkFOU0FDVElPTik7XG4gICAgbGV0IHN1bW1hcnlMaXN0OiBTdW1tYXJ5W10gPSBhd2FpdCBvZmZsaW5lRGF0YWJhc2UuZ2V0RW50cmllcyhPRkZMSU5FX0NPTlNUQU5UUy5UQUJMRVMuU1VNTUFSWSk7XG5cbiAgICBmdW5jdGlvbiBnZXRUcmFuc2FjdGlvblNhbGVzKCl7XG4gICAgICBsZXQgc2FsZXMgPSAwO1xuICAgICAgdHJhbnNhY3Rpb25MaXN0LmZvckVhY2goKHRyYW5zYWN0aW9uOiBUcmFuc2FjdGlvbikgPT4ge1xuICAgICAgICBzYWxlcyArPSB0cmFuc2FjdGlvbi50b3RhbDtcbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIHNhbGVzO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldFN1bW1hcnlTYWxlcyhzdW1tYXJ5TGlzdDogU3VtbWFyeVtdKXtcbiAgICAgIGxldCBzYWxlcyA9IDA7XG4gICAgICBzdW1tYXJ5TGlzdC5mb3JFYWNoKChzdW1tYXJ5OiBTdW1tYXJ5KSA9PiB7XG4gICAgICAgIHNhbGVzICs9IHN1bW1hcnkuYW1vdW50O1xuICAgICAgfSk7XG5cbiAgICAgIHJldHVybiBzYWxlcztcbiAgICB9XG5cbiAgICBsZXQgdHJhbnNhY3Rpb25TYWxlcyA9IGdldFRyYW5zYWN0aW9uU2FsZXMoKTtcbiAgICBsZXQgc3VtbWFyeVNhbGVzID0gZ2V0U3VtbWFyeVNhbGVzKHN1bW1hcnlMaXN0KTtcblxuICAgIGlmKHN1bW1hcnlTYWxlcyAhPT0gdHJhbnNhY3Rpb25TYWxlcyl7XG4gICAgICAvLyBBdHRlbXB0IHRvIHVwZGF0ZSBzdW1tYXJ5IGFnYWluOlxuXG4gICAgICBmb3IoY29uc3QgdHJhbnNhY3Rpb24gb2YgdHJhbnNhY3Rpb25MaXN0KXtcbiAgICAgICAgYXdhaXQgdXBkYXRlU3VtbWFyeSh0cmFuc2FjdGlvbik7XG4gICAgICB9XG5cbiAgICAgIHN1bW1hcnlMaXN0ID0gYXdhaXQgb2ZmbGluZURhdGFiYXNlLmdldEVudHJpZXMoT0ZGTElORV9DT05TVEFOVFMuVEFCTEVTLlNVTU1BUlkpO1xuICAgICAgc3VtbWFyeVNhbGVzID0gZ2V0U3VtbWFyeVNhbGVzKHN1bW1hcnlMaXN0KTtcbiAgICAgIC8vIGF0dGVtcHQgc3RpbGwgZmFpbGVkIHNvIHRoZXJlIGlzIGEgcHJvYmxlbSB3aXRoIHRoZSB1cGRhdGUgc3VtbWFyeSBjb2RlXG4gICAgICBpZihzdW1tYXJ5U2FsZXMgIT09IHRyYW5zYWN0aW9uU2FsZXMpe1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RyYW5zYWN0aW9uIHNhbGVzIGRvIG5vdCBtYXRjaCB3aXRoIHN1bW1hcnkgc2FsZXMgJyArIHRyYW5zYWN0aW9uU2FsZXMgKyAnICE9ICcgKyBzdW1tYXJ5U2FsZXMpO1xuICAgICAgfVxuXG4gICAgfVxuXG4gICAgbGV0IGRhdGEgPSB7XG4gICAgICBjbG9zZUNvZGUgOiBjbG9zZUNvZGUsXG4gICAgICBkYXRlIDogZGF0ZU1pbGxpcyxcbiAgICAgIGRyYXcgOiBkcmF3LFxuICAgICAgb3V0bGV0SWQgOiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShPRkZMSU5FX0NPTlNUQU5UUy5MT0NBTF9TVE9SQUdFLk9VVExFVF9JRCksXG4gICAgICBicmFuY2hJZCA6IGxvY2FsU3RvcmFnZS5nZXRJdGVtKE9GRkxJTkVfQ09OU1RBTlRTLkxPQ0FMX1NUT1JBR0UuQlJBTkNIX0lEKSxcbiAgICAgIGNyZWF0ZWRCeSA6IGxvY2FsU3RvcmFnZS5nZXRJdGVtKE9GRkxJTkVfQ09OU1RBTlRTLkxPQ0FMX1NUT1JBR0UuVVNFUl9JRCksXG4gICAgICBkb2N1bWVudHMgOiBbXS5jb25jYXQodHJhbnNhY3Rpb25MaXN0KS5jb25jYXQoc3VtbWFyeUxpc3QpLFxuICAgICAgdG90YWxTYWxlczogc3VtbWFyeVNhbGVzXG4gICAgfTtcblxuICAgIGNvbnN0IGxvZ2dlZFVzZXIgPSBnZXRMb2dnZWRVc2VyKCk7XG5cbiAgICBpZighbG9nZ2VkVXNlcil7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ05vIG91dGxldCB1c2VyIGxvZ2dlZCBpbi4nKTtcbiAgICB9XG5cbiAgICBjb25zdCBkYXRlUGlwZSA9IG5ldyBEYXRlUGlwZShcImVuLVVTXCIpO1xuXG4gICAgbGV0IGRyYXdTdHIgPSBudWxsO1xuXG4gICAgc3dpdGNoIChkcmF3KXtcblxuICAgICAgY2FzZSBEcmF3LkRSQVcxOlxuICAgICAgICBkcmF3U3RyID0gJzExQU0nO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgRHJhdy5EUkFXMTpcbiAgICAgICAgZHJhd1N0ciA9ICc0UE0nO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgRHJhdy5EUkFXMTpcbiAgICAgICAgZHJhd1N0ciA9ICc5UE0nO1xuICAgICAgICBicmVhaztcblxuICAgIH1cblxuICAgIGNvbnN0IGZuID1cbiAgICAgIGxvZ2dlZFVzZXIuXy5vdXRsZXQubmFtZSArXG4gICAgICAnXycgK1xuICAgICAgZHJhd1N0ciArXG4gICAgICAnXycgK1xuICAgICAgZGF0ZVBpcGUudHJhbnNmb3JtKG5ldyBEYXRlKGRhdGVNaWxsaXMpLCAnZGQtTU1NLXl5eXknKSArXG4gICAgICAnXycgK1xuICAgICAgbG9nZ2VkVXNlci5fLmJyYW5jaC5uYW1lICtcbiAgICAgICcudHh0JztcblxuICAgIGRvd25sb2FkKGJ0b2EoSlNPTi5zdHJpbmdpZnkoZGF0YSkpLCBmbiwgJ3RleHQvcGxhaW4nKTtcblxuICAgIHJldHVybiBkYXRhO1xuXG4gIH1cblxuICAvLyAtLS0tLS0tLS0tLS0tLS0gb2ZmbGluZSBhcGkgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG4gIGV4cG9ydCBhc3luYyBmdW5jdGlvbiBjaGVja05ld0JldChkcmF3OiBEcmF3LCBiZXQ6IEJldCwgYmV0czogQmV0W10pe1xuXG4gICAgLy8gY2hlY2sgZm9yIGR1cGxpY2F0ZXNcbiAgICBjb25zdCBzM3JDb21ib3MgPSBiZXQucmFmZmxlID09PSBSYWZmbGUuUzNSID8gcmFtYm9saXRvSW50KGJldC5jb21iaW5hdGlvbikgOiBudWxsO1xuXG4gICAgY29uc3QgTElNSVRfUEVSX0NPTUJJTkFUSU9OID0gMTAwO1xuXG4gICAgbGV0IGN1cnJlbnRWYWx1ZSA9IDA7XG5cbiAgICBiZXRzLmZvckVhY2goZWFjaCA9PiB7XG5cbiAgICAgIGxldCBtYXRjaGVzID0gZmFsc2U7XG4gICAgICBsZXQgcG9zc2libGVDb21ib3MgPSAxO1xuXG4gICAgICBpZihzM3JDb21ib3Mpe1xuICAgICAgICBtYXRjaGVzID0gczNyQ29tYm9zLnNvbWUoKGMpID0+IHtyZXR1cm4gYyA9PT0gZWFjaC5jb21iaW5hdGlvbn0pO1xuICAgICAgfWVsc2UgaWYoZWFjaC5yYWZmbGUgPT09IFJhZmZsZS5TM1IgKXtcbiAgICAgICAgY29uc3QgczNyRWFjaCA9IHJhbWJvbGl0b0ludChlYWNoLmNvbWJpbmF0aW9uKTtcbiAgICAgICAgbWF0Y2hlcyA9IHMzckVhY2guc29tZShjID0+IHtyZXR1cm4gYyA9PT0gYmV0LmNvbWJpbmF0aW9ufSk7XG4gICAgICAgIHBvc3NpYmxlQ29tYm9zID0gczNyRWFjaC5sZW5ndGg7XG4gICAgICB9ZWxzZSB7XG4gICAgICAgIG1hdGNoZXMgPSBlYWNoLmNvbWJpbmF0aW9uID09PSBiZXQuY29tYmluYXRpb247XG4gICAgICB9XG5cbiAgICAgIGlmKG1hdGNoZXMgJiYgZWFjaC5yYWZmbGUgPT09IGJldC5yYWZmbGUpe1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0JldCBhbHJlYWR5IGFkZGVkOiAnICsgYmV0LnJhZmZsZSArICcuJyArIHBhZGR5KGJldC5jb21iaW5hdGlvbikpO1xuICAgICAgfWVsc2UgaWYobWF0Y2hlcyl7XG4gICAgICAgIGN1cnJlbnRWYWx1ZSArPSAoZWFjaC5yYWZmbGUgPT09IFJhZmZsZS5TM1IgPyAoZWFjaC5hbW91bnQgLyBwb3NzaWJsZUNvbWJvcykgOiBlYWNoLmFtb3VudCkgO1xuICAgICAgfVxuXG4gICAgfSk7XG5cbiAgICBjdXJyZW50VmFsdWUgKz0gYXdhaXQgZ2V0Q3VycmVudFZhbHVlKGRyYXcsIGJldC5jb21iaW5hdGlvbik7XG5cbiAgICBsZXQgbGVmdEFtb3VudCA9IExJTUlUX1BFUl9DT01CSU5BVElPTiAtIGN1cnJlbnRWYWx1ZTtcblxuICAgIGlmKGxlZnRBbW91bnQgPD0gMCl7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0xpbWl0IHJlYWNoZWQ6ICcrIHBhZGR5KGJldC5jb21iaW5hdGlvbiwgMykpO1xuICAgIH1cblxuICAgIGlmKGxlZnRBbW91bnQgPCAoczNyQ29tYm9zID8gYmV0LmFtb3VudCAvIHMzckNvbWJvcy5sZW5ndGggOiBiZXQuYW1vdW50KSl7XG5cbiAgICAgIGlmKHMzckNvbWJvcyl7XG4gICAgICAgIGxlZnRBbW91bnQgPSBsZWZ0QW1vdW50ICogczNyQ29tYm9zLmxlbmd0aDtcbiAgICAgIH1cblxuICAgICAgdGhyb3cgbmV3IEVycm9yKCdPbmx5ICcgKyBsZWZ0QW1vdW50ICsgJyBwZXNvcyBsZWZ0IGZvciAnICsgYmV0LnJhZmZsZSArICcuJyArIHBhZGR5KGJldC5jb21iaW5hdGlvbiwgMykpO1xuICAgIH1cblxuICAgIHJldHVybiB0cnVlO1xuXG4gIH1cblxuICAvLyBhZGQgdHJhbnNhY3Rpb24gYW5kIGJldHMgdG8gb2ZmbGluZSBkYXRhYmFzZVxuICBleHBvcnQgYXN5bmMgZnVuY3Rpb24gYWRkVHJhbnNhY3Rpb24odHJhbnNhY3Rpb246IFRyYW5zYWN0aW9uKSB7XG5cbiAgICBjb25zdCBjdXJyZW50RGF0ZSA9IG5ldyBEYXRlKCk7XG5cbiAgICAvLyBjaGVjayBpZiB0cmFuc2FjdGlvbidzIGRyYXcgaXMgb3BlbiwgY2FuY2VsIGlmIG5vdFxuICAgIGNvbnN0IGRyYXdDbG9zZWQgPSBnZXRDbG9zZWREcmF3KG5ldyBEcmF3RGF0YShjdXJyZW50RGF0ZSwgdHJhbnNhY3Rpb24uZHJhdykpICE9IG51bGw7XG5cbiAgICBpZiAoZHJhd0Nsb3NlZCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdBYm9ydGVkOiBkcmF3IGlzIGFscmVhZHkgY2xvc2VkLicpO1xuICAgIH1cblxuICAgIGNvbnN0IHRyYW5zYWN0aW9uRGF0ZSA9IGRhdGVOb1RpbWUoY3VycmVudERhdGUpLmdldFRpbWUoKTsgLy8gYWRkaW5nIHRyYW5zYWN0aW9ucyB3aWxsIGFsd2F5cyBzZXQgdGhlIHRyYW5zYWN0aW9uRGF0ZSB0byBub3dcblxuICAgIC8vIG1ha2Ugc3VyZSB0aGUgdHJhbnNhY3Rpb25EYXRlIGlzIHVwZGF0ZWQgdG8gbm93XG4gICAgdHJhbnNhY3Rpb24uZGF0ZSA9IHRyYW5zYWN0aW9uRGF0ZTtcblxuICAgIGNvbnN0IG9mZmxpbmU6IE9mZmxpbmVEYXRhYmFzZSA9IGdldERhdGFiYXNlKHRyYW5zYWN0aW9uRGF0ZSwgdHJhbnNhY3Rpb24uZHJhdyk7XG5cbiAgICBhd2FpdCB1cGRhdGVTdW1tYXJ5KHRyYW5zYWN0aW9uLCBvZmZsaW5lKTtcblxuICAgIC8vIFRPRE86IGJlZm9yZSBzYXZpbmcsIGNoZWNrIGFnYWluIGlmIHRyYW5zYWN0aW9uIGlkIGV4aXN0cyBhbmQgY2hhbmdlIGlmIGl0IGRvZXNcbiAgICBjb25zdCB0cmFuc2FjdGlvblNhdmVkID0gYXdhaXQgb2ZmbGluZS5zYXZlRW50cnkoJ3RyYW5zYWN0aW9uJywgdHJhbnNhY3Rpb24pO1xuXG4gICAgaWYgKCF0cmFuc2FjdGlvblNhdmVkKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0Fib3J0aW5nIGFkZGluZyB0cmFuc2FjdGlvbiBhbmQgYmV0czogVHJhbnNhY3Rpb24gZW50cnkgZmFpbGVkIHRvIGdldCByZWNvcmRlZC4nKTtcbiAgICAgIC8vIFRPRE86IHJldmVydFxuICAgIH1cblxuICAgIHJldHVybiB0cnVlO1xuXG4gIH1cblxuICBleHBvcnQgYXN5bmMgZnVuY3Rpb24gc2VhcmNoU3VtbWFyeShkYXRlQXJnOiBEYXRlLCBkcmF3OiBEcmF3LCBrZXk/KSB7IC8va2V5IGNhbiBiZSBudW1iZXIgb3IgdHJhbnNhY3Rpb25cblxuICAgIGlmICghZHJhdykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdObyBkcmF3IHNwZWNpZmllZC4nKTtcbiAgICB9XG5cbiAgICBjb25zdCBkYXRlID0gZGF0ZU5vVGltZShkYXRlQXJnKS5nZXRUaW1lKCk7XG5cbiAgICBpZihrZXkgJiYga2V5Lmxlbmd0aCA+IDcpeyAvL2lmIHRyYW5zYWN0aW9uIG51bWJlciwgdHJhbnNhY3Rpb24gbnVtYmVycyBhcmUgYWx3YXlzID4gNyBpbiBsZW5ndGhcbiAgICAgIHJldHVybiBhd2FpdCBzZWFyY2hUcmFuc2FjdGlvbihkYXRlLCBkcmF3LCBrZXkpO1xuICAgIH1cblxuICAgIHJldHVybiBhd2FpdCBzZWFyY2hCZXRTdW1tYXJ5KGRhdGUsIGRyYXcsIE51bWJlcihrZXkpKTtcblxuICB9XG5cbiAgLy8gc2VhcmNoIGZvciBiZXRzIGF0IHRoZSBvZmZsaW5lIGRhdGFiYXNlICh1c2VkIGluIG91dGxldD5zdW1tYXJ5IHRhYmxlXG4gIGV4cG9ydCBhc3luYyBmdW5jdGlvbiBzZWFyY2hUcmFuc2FjdGlvbihkYXRlTWlsbGlzOiBudW1iZXIsIGRyYXc6IERyYXcsIHRyYW5zYWN0aW9uTnVtYmVyOiBzdHJpbmcpIHsgLy9rZXkgY2FuIGJlIG51bWJlciBvciB0cmFuc2FjdGlvblxuXG4gICAgY29uc3Qgb2ZmbGluZURhdGFiYXNlOiBPZmZsaW5lRGF0YWJhc2UgPSBnZXREYXRhYmFzZShkYXRlTWlsbGlzLCBkcmF3KTtcblxuICAgIGNvbnN0IHJlc3VsdCA9IGF3YWl0IG9mZmxpbmVEYXRhYmFzZVxuICAgICAgLmdldEVudHJpZXMoT0ZGTElORV9DT05TVEFOVFMuVEFCTEVTLlRSQU5TQUNUSU9OLCBPRkZMSU5FX0NPTlNUQU5UUy5JTkRFWEVTLlRSQU5TQUNUSU9OX0lELCBJREJLZXlSYW5nZS5vbmx5KHRyYW5zYWN0aW9uTnVtYmVyKSk7XG5cbiAgICBpZighcmVzdWx0IHx8IHJlc3VsdC5sZW5ndGggPT09IDApe1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3VtbWFyeUxpc3QgOiBbXSxcbiAgICAgICAgdG90YWxBbW91bnQgOiAwXG4gICAgICB9XG4gICAgfVxuXG4gICAgY29uc3QgdHJhbnNhY3Rpb246IFRyYW5zYWN0aW9uID0gcmVzdWx0WzBdO1xuXG4gICAgbGV0IHN1bW1hcnlMaXN0OiBTdW1tYXJ5W10gPSAgW107XG5cbiAgICBjb25zdCBiZXRzID0gdHJhbnNhY3Rpb24uYmV0cztcblxuICAgIGZvcihsZXQgYmV0IG9mIGJldHMpe1xuXG4gICAgICBjb25zdCBzdW1tYXJ5ID0gbmV3IFN1bW1hcnkoZGF0ZU1pbGxpcywgZHJhdywgYmV0LnJhZmZsZSwgYmV0LmNvbWJpbmF0aW9uKTtcbiAgICAgIHN1bW1hcnkuYW1vdW50ID0gYmV0LmFtb3VudDtcblxuICAgICAgc3VtbWFyeS50cmFjZSA9IFtdO1xuXG4gICAgICBjb25zdCBkZXRhaWxzOiBTdW1tYXJ5VHJhY2VEZXRhaWxbXSA9IFtdO1xuICAgICAgZGV0YWlscy5wdXNoKG5ldyBTdW1tYXJ5VHJhY2VEZXRhaWwoYmV0LmlkLCBiZXQuYW1vdW50KSk7XG5cbiAgICAgIHN1bW1hcnkudHJhY2UucHVzaChuZXcgU3VtbWFyeVRyYWNlKHRyYW5zYWN0aW9uLnRyYW5zYWN0aW9uSWQsIGRldGFpbHMpKVxuXG4gICAgICBzdW1tYXJ5TGlzdC5wdXNoKHN1bW1hcnkpO1xuICAgIH1cblxuICAgIHJldHVybiB7XG4gICAgICBzdW1tYXJ5TGlzdCA6IHN1bW1hcnlMaXN0LFxuICAgICAgdG90YWxBbW91bnQgOiB0cmFuc2FjdGlvbi50b3RhbFxuICAgIH07XG4gIH1cblxuICAvLyBVc2VkIGluIHRyYW5zYWN0aW9uIGxpc3RcbiAgZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldFRyYW5zYWN0aW9uKGRhdGVNaWxsaXM6IG51bWJlciwgZHJhdzogRHJhdywgdHJhbnNhY3Rpb25OdW1iZXI/OiBzdHJpbmcpOiBQcm9taXNlPFRyYW5zYWN0aW9uW10+IHsgLy9rZXkgY2FuIGJlIG51bWJlciBvciB0cmFuc2FjdGlvblxuXG4gICAgY29uc3Qgb2ZmbGluZURhdGFiYXNlOiBPZmZsaW5lRGF0YWJhc2UgPSBnZXREYXRhYmFzZShkYXRlTWlsbGlzLCBkcmF3KTtcblxuICAgIGxldCByZXN1bHQ6IFRyYW5zYWN0aW9uW10gPSBbXTtcblxuICAgIHRyYW5zYWN0aW9uTnVtYmVyID0gdHJhbnNhY3Rpb25OdW1iZXIgPyB0cmFuc2FjdGlvbk51bWJlci50cmltKCkgOiBudWxsO1xuXG4gICAgaWYoIXRyYW5zYWN0aW9uTnVtYmVyIHx8IHRyYW5zYWN0aW9uTnVtYmVyID09PSAnJyl7IC8vcmV0cmlldmUgYWxsXG4gICAgICByZXN1bHQgPSBhd2FpdCBvZmZsaW5lRGF0YWJhc2UuZ2V0RW50cmllcyhPRkZMSU5FX0NPTlNUQU5UUy5UQUJMRVMuVFJBTlNBQ1RJT04pO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXN1bHQgPSBhd2FpdCBvZmZsaW5lRGF0YWJhc2UuZ2V0RW50cmllcyhPRkZMSU5FX0NPTlNUQU5UUy5UQUJMRVMuVFJBTlNBQ1RJT04sIE9GRkxJTkVfQ09OU1RBTlRTLklOREVYRVMuVFJBTlNBQ1RJT05fSUQsIElEQktleVJhbmdlLm9ubHkodHJhbnNhY3Rpb25OdW1iZXIpKTtcbiAgICB9XG5cbiAgICByZXN1bHQuc29ydCgoYSwgYikgPT4ge1xuICAgICAgcmV0dXJuIGIuX3N5c3RlbUhlYWRlci5jcmVhdGVkRGF0ZSAtIGEuX3N5c3RlbUhlYWRlci5jcmVhdGVkRGF0ZTtcbiAgICB9KTtcblxuICAgIHJldHVybiByZXN1bHQ7XG5cbiAgfVxuXG4gIC8vIHNlYXJjaCBmb3IgYmV0cyBhdCB0aGUgb2ZmbGluZSBkYXRhYmFzZVxuICBleHBvcnQgYXN5bmMgZnVuY3Rpb24gc2VhcmNoQmV0U3VtbWFyeShkYXRlTWlsbGlzOiBudW1iZXIsIGRyYXc6IERyYXcsIGNvbWJpbmF0aW9uOm51bWJlcikgeyAvL2tleSBjYW4gYmUgbnVtYmVyIG9yIHRyYW5zYWN0aW9uXG5cbiAgICBjb25zdCBvZmZsaW5lRGF0YWJhc2U6IE9mZmxpbmVEYXRhYmFzZSA9IGdldERhdGFiYXNlKGRhdGVNaWxsaXMsIGRyYXcpO1xuXG4gICAgbGV0IHN1bW1hcnlMaXN0OiBTdW1tYXJ5W10gPSAgW107XG5cbiAgICBpZiAoIWNvbWJpbmF0aW9uKSB7XG4gICAgICAvLyByZXR1cm4gZXZlcnl0aGluZ1xuICAgICAgc3VtbWFyeUxpc3QgPSBhd2FpdCBvZmZsaW5lRGF0YWJhc2UuZ2V0RW50cmllcyhPRkZMSU5FX0NPTlNUQU5UUy5UQUJMRVMuU1VNTUFSWSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIHJldHVybiBpbmNsdWRpbmcgcmFtYm9saXRvIGNvbWJpbmF0aW9uc1xuICAgICAgY29uc3QgcG9zc2liaWxpdGllcyA9IEFycmF5LmZyb20ocmFtYm9saXRvKGNvbWJpbmF0aW9uKSk7XG4gICAgICBmb3IgKGxldCB4ID0gMDsgeCA8IHBvc3NpYmlsaXRpZXMubGVuZ3RoOyB4KyspIHtcbiAgICAgICAgY29uc3QgY29tYm8gPSBwb3NzaWJpbGl0aWVzW3hdO1xuICAgICAgICAvLyArIG9wZXJhdG9yIHdpbGwgY29udmVydCBjb21ibyB0byBhIG51bWJlclxuICAgICAgICAvLyBodHRwczovL3N0YWNrb3ZlcmZsb3cuY29tL3F1ZXN0aW9ucy8xNDY2NzcxMy90eXBlc2NyaXB0LWNvbnZlcnRpbmctYS1zdHJpbmctdG8tYS1udW1iZXJcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gYXdhaXQgb2ZmbGluZURhdGFiYXNlXG4gICAgICAgICAgLmdldEVudHJpZXMoT0ZGTElORV9DT05TVEFOVFMuVEFCTEVTLlNVTU1BUlksIE9GRkxJTkVfQ09OU1RBTlRTLklOREVYRVMuQ09NQklOQVRJT04sIElEQktleVJhbmdlLm9ubHkoK2NvbWJvKSk7XG4gICAgICAgIHN1bW1hcnlMaXN0ID0gc3VtbWFyeUxpc3QuY29uY2F0KHJlc3VsdCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgbGV0IHRvdGFsQW1vdW50ID0gMDtcbiAgICBzdW1tYXJ5TGlzdC5mb3JFYWNoKChiZXQpID0+IHtcbiAgICAgIHRvdGFsQW1vdW50ICs9IGJldC5hbW91bnQ7XG4gICAgfSk7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3VtbWFyeUxpc3QgOiBzdW1tYXJ5TGlzdCxcbiAgICAgIHRvdGFsQW1vdW50IDogdG90YWxBbW91bnRcbiAgICB9O1xuICB9XG5cbiAgLy8gY2xlYXJzIHVwIHRyYW5zYWN0aW9ucyBhbmQgYmV0IGZvciBhIGdpdmVuIGRhdGUgKGFsbCBkcmF3cylcbiAgZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGNsZWFyKGRhdGUgPzogbnVtYmVyKSB7XG5cbiAgICBpZiAoIWRhdGUpIHtcbiAgICAgIGRhdGUgPSBEYXRlLm5vdygpO1xuICAgIH1cblxuICAgIGNvbnN0IHRvQ2xlYXIgPSBbXG4gICAgICBnZXREQkluZGV4KGRhdGUsIERyYXcuRFJBVzEpLFxuICAgICAgZ2V0REJJbmRleChkYXRlLCBEcmF3LkRSQVcyKSxcbiAgICAgIGdldERCSW5kZXgoZGF0ZSwgRHJhdy5EUkFXMylcbiAgICBdO1xuXG4gICAgY29uc3QgcHJvbWlzZXM6IFByb21pc2U8YW55PltdID0gW107XG5cbiAgICB0b0NsZWFyLmZvckVhY2goaW5kZXggPT4ge1xuXG4gICAgICBsZXQgb2ZmbGluZURCOiBPZmZsaW5lRGF0YWJhc2UgPSBwb29sW2luZGV4XTtcblxuICAgICAgaWYgKCFvZmZsaW5lREIpIHtcbiAgICAgICAgb2ZmbGluZURCID0gbmV3IE9mZmxpbmVEYXRhYmFzZShpbmRleCwgX29mZmxpbmVUYWJsZXMsIE9GRkxJTkVfQ09OU1RBTlRTLklEQl9WRVJTSU9OKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGRlbGV0ZSBwb29sW2luZGV4XTtcbiAgICAgIH1cblxuICAgICAgcHJvbWlzZXMucHVzaChvZmZsaW5lREIuY2xlYXJEYXRhYmFzZSgpKTtcblxuICAgIH0pO1xuXG4gICAgYXdhaXQgUHJvbWlzZS5hbGwocHJvbWlzZXMpO1xuXG4gICAgcmV0dXJuIHRydWU7XG5cbiAgfVxuXG4gIC8vIGFsd2F5cyBhc3N1bWVzIHRoYXQgdGhpcyBpcyBhIHJhdyBkYXRlIGFuIG5vdCBwcm9jZXNzZWQgYnkgZGF0ZU5vVGltZVxuICBleHBvcnQgYXN5bmMgZnVuY3Rpb24gY2xvc2VEcmF3KGRyYXc6IERyYXcsIGRhdGU/OiBEYXRlKTogUHJvbWlzZTxhbnk+IHtcblxuICAgIGRhdGUgPSBkYXRlID8gZGF0ZSA6IG5ldyBEYXRlKCk7XG5cbiAgICBjb25zdCBjYW5kaWRhdGU6IERyYXdEYXRhID0gIG5ldyBEcmF3RGF0YSAoZGF0ZSwgZHJhdyk7XG5cbiAgICAvLyBpZiBjYW5kaWRhdGUgaXMgYWxyZWFkeSBjbG9zZWQgdGhlbiBkb3dubG9hZCBpbW1lZGlhdGVseVxuICAgIGNvbnN0IGNsb3NlZERyYXc6IERyYXdEYXRhID0gZ2V0Q2xvc2VkRHJhdyhjYW5kaWRhdGUpO1xuXG4gICAgaWYgKGNsb3NlZERyYXcpIHtcbiAgICAgIGNvbnN0IGRhdGEgPSBhd2FpdCBkb3dubG9hZERyYXcoY2xvc2VkRHJhdy5kYXRlTm9UaW1lLCBjbG9zZWREcmF3LmRyYXcsIGNsb3NlZERyYXcuY2xvc2VDb2RlKTtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGNjIDogY2xvc2VkRHJhdy5jbG9zZUNvZGUsXG4gICAgICAgIGRhdGEgOiBkYXRhXG4gICAgICB9O1xuICAgIH1cblxuICAgIC8vIGNhbmRpZGF0ZSBpcyBub3QgeWV0IGNsb3NlZDpcblxuICAgIGNhbmRpZGF0ZS5jbG9zZUNvZGUgPSBEYXRlLm5vdygpO1xuXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oYnRvYShjYW5kaWRhdGUuZG9jdW1lbnRJZCksIGJ0b2EoSlNPTi5zdHJpbmdpZnkoY2FuZGlkYXRlKSkpO1xuXG4gICAgY29uc3QgZGF0YSA9IGF3YWl0IGRvd25sb2FkRHJhdyhjYW5kaWRhdGUuZGF0ZU5vVGltZSwgY2FuZGlkYXRlLmRyYXcsIGNhbmRpZGF0ZS5jbG9zZUNvZGUpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIGNjIDogY2FuZGlkYXRlLmNsb3NlQ29kZSxcbiAgICAgIGRhdGEgOiBkYXRhXG4gICAgfTtcblxuICB9XG5cbiAgLy8geW91IGNhbiBvbmx5IGNhbmNlbCB0cmFuc2FjdGlvbnMgb24gdGhlIGN1cnJlbnQgZGF0ZVxuICBleHBvcnQgYXN5bmMgZnVuY3Rpb24gY2FuY2VsVHJhbnNhY3Rpb24oZHJhdzogRHJhdywgdHJhbnNhY3Rpb25JZDogc3RyaW5nKSB7XG5cbiAgICBpZiAoIWRyYXcpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignRHJhdyBub3Qgc3BlY2lmaWVkIHdoZW4gY2FuY2VsbGluZyB0cmFuc2FjdGlvbi4nKTtcbiAgICB9XG5cbiAgICBpZiAodHJhbnNhY3Rpb25JZC5sZW5ndGggPT09IDApIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignSW52YWxpZCB0cmFuc2FjdGlvbiBpZCBzcGVjaWZpZWQgb24gY2FuY2VsbGluZyB0cmFuc2FjdGlvbi4nKTtcbiAgICB9XG5cbiAgICBjb25zdCBjdXJyZW50RGF0ZSA9IG5ldyBEYXRlKCk7XG5cbiAgICAvLyBjaGVjayBpZiB0cmFuc2FjdGlvbidzIGRyYXcgaXMgb3BlbiwgY2FuY2VsIGlmIG5vdFxuICAgIGNvbnN0IGRyYXdDbG9zZWQgPSBnZXRDbG9zZWREcmF3KG5ldyBEcmF3RGF0YShjdXJyZW50RGF0ZSwgZHJhdykpICE9IG51bGw7XG5cbiAgICBpZiAoZHJhd0Nsb3NlZCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdBYm9ydGVkOiBkcmF3IGlzIGFscmVhZHkgY2xvc2VkLicpO1xuICAgIH1cblxuICAgIGNvbnN0IG9mZmxpbmU6IE9mZmxpbmVEYXRhYmFzZSA9IGdldERhdGFiYXNlKGRhdGVOb1RpbWUoY3VycmVudERhdGUpLmdldFRpbWUoKSwgZHJhdyk7XG5cbiAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBvZmZsaW5lXG4gICAgICAuZ2V0RW50cmllcyhPRkZMSU5FX0NPTlNUQU5UUy5UQUJMRVMuVFJBTlNBQ1RJT04sIE9GRkxJTkVfQ09OU1RBTlRTLklOREVYRVMuVFJBTlNBQ1RJT05fSUQsIElEQktleVJhbmdlLm9ubHkodHJhbnNhY3Rpb25JZCkpO1xuXG4gICAgaWYgKHJlc3VsdC5sZW5ndGggPiAxKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0ZvdW5kIGFuIGFibm9ybWFsIG51bWJlciBvZiB0cmFuc2FjdGlvbiBmb3IgYSBnaXZlbiB0cmFuc2FjdGlvbklkJyk7XG4gICAgfVxuXG4gICAgY29uc3QgdHJhbnNhY3Rpb246IFRyYW5zYWN0aW9uID0gcmVzdWx0WzBdO1xuXG4gICAgaWYgKCF0cmFuc2FjdGlvbikge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdUcmFuc2FjdGlvbiBub3QgZm91bmQgb24gY3VycmVudCBkYXRlICh0b2RheSkgYW5kIGdpdmVuIGRyYXcuJyk7XG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIHN1bW1hcmllc1xuICAgIGZvciAobGV0IHggPSAwIDsgeCA8IHRyYW5zYWN0aW9uLmJldHMubGVuZ3RoIDsgeCsrKSB7XG4gICAgICBjb25zdCBiZXQgPSB0cmFuc2FjdGlvbi5iZXRzW3hdO1xuICAgICAgY29uc3Qgc3VtbWFyeVVwZGF0ZWQgPSBhd2FpdCB1cGRhdGVTdW1tYXJ5T25UcmFuc2FjdGlvbkNhbmNlbGxlZChvZmZsaW5lLCB0cmFuc2FjdGlvbiwgYmV0KTtcbiAgICAgIGlmICghc3VtbWFyeVVwZGF0ZWQpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdBYm9ydGVkIGNhbmNlbGxpbmcgdHJhbnNhY3Rpb24uIFN1bW1hcnkgd2FzIG5vdCB1cGRhdGVkLicpO1xuICAgICAgICAvLyBUT0RPOiByZXZlcnRcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyB1cGRhdGUgdHJhbnNhY3Rpb24gY2FuY2VsbGVkID0gdHJ1ZVxuICAgIHRyYW5zYWN0aW9uLmNhbmNlbGxlZCA9IHRydWU7XG4gICAgcmV0dXJuIGF3YWl0IG9mZmxpbmUucHV0KE9GRkxJTkVfQ09OU1RBTlRTLlRBQkxFUy5UUkFOU0FDVElPTiwgdHJhbnNhY3Rpb24pO1xuXG4gIH1cblxuICBleHBvcnQgZnVuY3Rpb24gZ2V0TG9nZ2VkVXNlcigpe1xuXG4gICAgdHJ5e1xuICAgICAgY29uc3QgcmF3ID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oT0ZGTElORV9DT05TVEFOVFMuTE9DQUxfU1RPUkFHRS5VU0VSX0lORk8pO1xuICAgICAgcmV0dXJuIEpTT04ucGFyc2UocmF3KTtcbiAgICB9Y2F0Y2goZXJyKXtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICB9XG5cbiAgZXhwb3J0IGZ1bmN0aW9uIGxvZ2luKCB1c2VyOiBVc2VyKXtcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShPRkZMSU5FX0NPTlNUQU5UUy5MT0NBTF9TVE9SQUdFLk9VVExFVF9JRCwgdXNlci5vdXRsZXRJZCk7XG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oT0ZGTElORV9DT05TVEFOVFMuTE9DQUxfU1RPUkFHRS5CUkFOQ0hfSUQsIHVzZXIuYnJhbmNoSWQpO1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKE9GRkxJTkVfQ09OU1RBTlRTLkxPQ0FMX1NUT1JBR0UuVVNFUl9JRCwgdXNlci5fc3lzdGVtSGVhZGVyLmRvY3VtZW50SWQpO1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKE9GRkxJTkVfQ09OU1RBTlRTLkxPQ0FMX1NUT1JBR0UuVVNFUl9JTkZPLCBKU09OLnN0cmluZ2lmeSh1c2VyKSk7XG4gIH1cblxuICBleHBvcnQgZnVuY3Rpb24gbG9nb3V0KCkge1xuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKE9GRkxJTkVfQ09OU1RBTlRTLkxPQ0FMX1NUT1JBR0UuT1VUTEVUX0lELCAnJyk7XG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oT0ZGTElORV9DT05TVEFOVFMuTE9DQUxfU1RPUkFHRS5CUkFOQ0hfSUQsICcnKTtcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShPRkZMSU5FX0NPTlNUQU5UUy5MT0NBTF9TVE9SQUdFLlVTRVJfSUQsICcnKTtcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShPRkZMSU5FX0NPTlNUQU5UUy5MT0NBTF9TVE9SQUdFLlVTRVJfSU5GTywgJycpO1xuICB9XG5cbn1cblxuXG4iXX0=