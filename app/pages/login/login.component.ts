import { Component, OnInit } from "@angular/core";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {ApiService} from "~/core/services/api.service";
import {User, UserType} from "~/core/models/outlet.model";
@Component({
    selector: "ns-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls:["./login.component.css"]
})
export class LoginComponent implements OnInit {
    userForm: FormGroup;

    status = {
        error : false,
        message : ''
    };


    constructor(private fb: FormBuilder,
                private router: Router,
                private apiService: ApiService) {

        const loggedInUser = apiService.getLoggedUser();

        if(loggedInUser){
            switch (loggedInUser.userType){
                case 'admin':
                case 'branch':
                    router.navigate(['admin']);
                    break;
                case 'outlet':
                    router.navigate(['user']);
                    break;
            }
        }

    }

    ngOnInit(){
        this._fBuild();
    }

    private _fBuild(){
        this.userForm = this.fb.group({
            username: [null],
            password: [null]
        })
    }
    private _submitUser(){
        return new UserForm(this.userForm.get('username').value, this.userForm.get('password').value)
    }

    submitForm(){
        let submitForm = this._submitUser();

        this.apiService.login(submitForm.username, submitForm.password)
            .subscribe((user: User)=>{
                this.apiService._login(user);
                this.status.message = 'login naka';
                if(user.userType === UserType.ADMIN || user.userType === UserType.BRANCH){
                    this.router.navigate(['/admin'])
                } else {
                    this.router.navigate(['/user/bet'])
                }
            }, (err) => {
                this.status.error = true;
                this.status.message= err.error;
                console.log(err)
            });
    }
}

export class UserForm{
    constructor(public username: string, public password: string){}
}