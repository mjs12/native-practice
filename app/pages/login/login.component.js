"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var api_service_1 = require("~/core/services/api.service");
var outlet_model_1 = require("~/core/models/outlet.model");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(fb, router, apiService) {
        this.fb = fb;
        this.router = router;
        this.apiService = apiService;
        this.status = {
            error: false,
            message: ''
        };
        var loggedInUser = apiService.getLoggedUser();
        if (loggedInUser) {
            switch (loggedInUser.userType) {
                case 'admin':
                case 'branch':
                    router.navigate(['admin']);
                    break;
                case 'outlet':
                    router.navigate(['user']);
                    break;
            }
        }
    }
    LoginComponent.prototype.ngOnInit = function () {
        this._fBuild();
    };
    LoginComponent.prototype._fBuild = function () {
        this.userForm = this.fb.group({
            username: [null],
            password: [null]
        });
    };
    LoginComponent.prototype._submitUser = function () {
        return new UserForm(this.userForm.get('username').value, this.userForm.get('password').value);
    };
    LoginComponent.prototype.submitForm = function () {
        var _this = this;
        var submitForm = this._submitUser();
        this.apiService.login(submitForm.username, submitForm.password)
            .subscribe(function (user) {
            _this.apiService._login(user);
            _this.status.message = 'login naka';
            if (user.userType === outlet_model_1.UserType.ADMIN || user.userType === outlet_model_1.UserType.BRANCH) {
                _this.router.navigate(['/admin']);
            }
            else {
                _this.router.navigate(['/user/bet']);
            }
        }, function (err) {
            _this.status.error = true;
            _this.status.message = err.error;
            console.log(err);
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: "ns-login",
            moduleId: module.id,
            templateUrl: "./login.component.html",
            styleUrls: ["./login.component.css"]
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder,
            router_1.Router,
            api_service_1.ApiService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
var UserForm = /** @class */ (function () {
    function UserForm(username, password) {
        this.username = username;
        this.password = password;
    }
    return UserForm;
}());
exports.UserForm = UserForm;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELHdDQUFzRDtBQUN0RCwwQ0FBdUM7QUFDdkMsMkRBQXVEO0FBQ3ZELDJEQUEwRDtBQU8xRDtJQVNJLHdCQUFvQixFQUFlLEVBQ2YsTUFBYyxFQUNkLFVBQXNCO1FBRnRCLE9BQUUsR0FBRixFQUFFLENBQWE7UUFDZixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQVIxQyxXQUFNLEdBQUc7WUFDTCxLQUFLLEVBQUcsS0FBSztZQUNiLE9BQU8sRUFBRyxFQUFFO1NBQ2YsQ0FBQztRQU9FLElBQU0sWUFBWSxHQUFHLFVBQVUsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUVoRCxFQUFFLENBQUEsQ0FBQyxZQUFZLENBQUMsQ0FBQSxDQUFDO1lBQ2IsTUFBTSxDQUFDLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFBLENBQUM7Z0JBQzNCLEtBQUssT0FBTyxDQUFDO2dCQUNiLEtBQUssUUFBUTtvQkFDVCxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztvQkFDM0IsS0FBSyxDQUFDO2dCQUNWLEtBQUssUUFBUTtvQkFDVCxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDMUIsS0FBSyxDQUFDO1lBQ2QsQ0FBQztRQUNMLENBQUM7SUFFTCxDQUFDO0lBRUQsaUNBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRU8sZ0NBQU8sR0FBZjtRQUNJLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDMUIsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDO1lBQ2hCLFFBQVEsRUFBRSxDQUFDLElBQUksQ0FBQztTQUNuQixDQUFDLENBQUE7SUFDTixDQUFDO0lBQ08sb0NBQVcsR0FBbkI7UUFDSSxNQUFNLENBQUMsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFBO0lBQ2pHLENBQUM7SUFFRCxtQ0FBVSxHQUFWO1FBQUEsaUJBaUJDO1FBaEJHLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUVwQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7YUFDMUQsU0FBUyxDQUFDLFVBQUMsSUFBVTtZQUNsQixLQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM3QixLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUM7WUFDbkMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyx1QkFBUSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLHVCQUFRLENBQUMsTUFBTSxDQUFDLENBQUEsQ0FBQztnQkFDdEUsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFBO1lBQ3BDLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUE7WUFDdkMsQ0FBQztRQUNMLENBQUMsRUFBRSxVQUFDLEdBQUc7WUFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7WUFDekIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUUsR0FBRyxDQUFDLEtBQUssQ0FBQztZQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBQ3BCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQTVEUSxjQUFjO1FBTjFCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsVUFBVTtZQUNwQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLHdCQUF3QjtZQUNyQyxTQUFTLEVBQUMsQ0FBQyx1QkFBdUIsQ0FBQztTQUN0QyxDQUFDO3lDQVUwQixtQkFBVztZQUNQLGVBQU07WUFDRix3QkFBVTtPQVhqQyxjQUFjLENBNkQxQjtJQUFELHFCQUFDO0NBQUEsQUE3REQsSUE2REM7QUE3RFksd0NBQWM7QUErRDNCO0lBQ0ksa0JBQW1CLFFBQWdCLEVBQVMsUUFBZ0I7UUFBekMsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUFTLGFBQVEsR0FBUixRQUFRLENBQVE7SUFBRSxDQUFDO0lBQ25FLGVBQUM7QUFBRCxDQUFDLEFBRkQsSUFFQztBQUZZLDRCQUFRIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHtGb3JtQnVpbGRlciwgRm9ybUdyb3VwfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcbmltcG9ydCB7Um91dGVyfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQge0FwaVNlcnZpY2V9IGZyb20gXCJ+L2NvcmUvc2VydmljZXMvYXBpLnNlcnZpY2VcIjtcbmltcG9ydCB7VXNlciwgVXNlclR5cGV9IGZyb20gXCJ+L2NvcmUvbW9kZWxzL291dGxldC5tb2RlbFwiO1xuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwibnMtbG9naW5cIixcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICAgIHRlbXBsYXRlVXJsOiBcIi4vbG9naW4uY29tcG9uZW50Lmh0bWxcIixcbiAgICBzdHlsZVVybHM6W1wiLi9sb2dpbi5jb21wb25lbnQuY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIExvZ2luQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICB1c2VyRm9ybTogRm9ybUdyb3VwO1xuXG4gICAgc3RhdHVzID0ge1xuICAgICAgICBlcnJvciA6IGZhbHNlLFxuICAgICAgICBtZXNzYWdlIDogJydcbiAgICB9O1xuXG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGZiOiBGb3JtQnVpbGRlcixcbiAgICAgICAgICAgICAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxuICAgICAgICAgICAgICAgIHByaXZhdGUgYXBpU2VydmljZTogQXBpU2VydmljZSkge1xuXG4gICAgICAgIGNvbnN0IGxvZ2dlZEluVXNlciA9IGFwaVNlcnZpY2UuZ2V0TG9nZ2VkVXNlcigpO1xuXG4gICAgICAgIGlmKGxvZ2dlZEluVXNlcil7XG4gICAgICAgICAgICBzd2l0Y2ggKGxvZ2dlZEluVXNlci51c2VyVHlwZSl7XG4gICAgICAgICAgICAgICAgY2FzZSAnYWRtaW4nOlxuICAgICAgICAgICAgICAgIGNhc2UgJ2JyYW5jaCc6XG4gICAgICAgICAgICAgICAgICAgIHJvdXRlci5uYXZpZ2F0ZShbJ2FkbWluJ10pO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlICdvdXRsZXQnOlxuICAgICAgICAgICAgICAgICAgICByb3V0ZXIubmF2aWdhdGUoWyd1c2VyJ10pO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgfVxuXG4gICAgbmdPbkluaXQoKXtcbiAgICAgICAgdGhpcy5fZkJ1aWxkKCk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBfZkJ1aWxkKCl7XG4gICAgICAgIHRoaXMudXNlckZvcm0gPSB0aGlzLmZiLmdyb3VwKHtcbiAgICAgICAgICAgIHVzZXJuYW1lOiBbbnVsbF0sXG4gICAgICAgICAgICBwYXNzd29yZDogW251bGxdXG4gICAgICAgIH0pXG4gICAgfVxuICAgIHByaXZhdGUgX3N1Ym1pdFVzZXIoKXtcbiAgICAgICAgcmV0dXJuIG5ldyBVc2VyRm9ybSh0aGlzLnVzZXJGb3JtLmdldCgndXNlcm5hbWUnKS52YWx1ZSwgdGhpcy51c2VyRm9ybS5nZXQoJ3Bhc3N3b3JkJykudmFsdWUpXG4gICAgfVxuXG4gICAgc3VibWl0Rm9ybSgpe1xuICAgICAgICBsZXQgc3VibWl0Rm9ybSA9IHRoaXMuX3N1Ym1pdFVzZXIoKTtcblxuICAgICAgICB0aGlzLmFwaVNlcnZpY2UubG9naW4oc3VibWl0Rm9ybS51c2VybmFtZSwgc3VibWl0Rm9ybS5wYXNzd29yZClcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKHVzZXI6IFVzZXIpPT57XG4gICAgICAgICAgICAgICAgdGhpcy5hcGlTZXJ2aWNlLl9sb2dpbih1c2VyKTtcbiAgICAgICAgICAgICAgICB0aGlzLnN0YXR1cy5tZXNzYWdlID0gJ2xvZ2luIG5ha2EnO1xuICAgICAgICAgICAgICAgIGlmKHVzZXIudXNlclR5cGUgPT09IFVzZXJUeXBlLkFETUlOIHx8IHVzZXIudXNlclR5cGUgPT09IFVzZXJUeXBlLkJSQU5DSCl7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2FkbWluJ10pXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvdXNlci9iZXQnXSlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LCAoZXJyKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5zdGF0dXMuZXJyb3IgPSB0cnVlO1xuICAgICAgICAgICAgICAgIHRoaXMuc3RhdHVzLm1lc3NhZ2U9IGVyci5lcnJvcjtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpXG4gICAgICAgICAgICB9KTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBVc2VyRm9ybXtcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdXNlcm5hbWU6IHN0cmluZywgcHVibGljIHBhc3N3b3JkOiBzdHJpbmcpe31cbn0iXX0=