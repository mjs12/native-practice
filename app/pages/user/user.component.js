"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_service_1 = require("~/core/services/api.service");
var UserComponent = /** @class */ (function () {
    function UserComponent(apiService) {
        this.apiService = apiService;
    }
    UserComponent.prototype.logout = function () {
        this.apiService.logout();
    };
    UserComponent.prototype.ngOnInit = function () {
        this.loggedInUser = this.apiService.getLoggedUser();
        console.log(this.loggedInUser);
    };
    UserComponent = __decorate([
        core_1.Component({
            selector: "ns-user",
            moduleId: module.id,
            templateUrl: "./user.component.html",
        }),
        __metadata("design:paramtypes", [api_service_1.ApiService])
    ], UserComponent);
    return UserComponent;
}());
exports.UserComponent = UserComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ1c2VyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCwyREFBdUQ7QUFPdkQ7SUFHSSx1QkFBb0IsVUFBc0I7UUFBdEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUFJLENBQUM7SUFFL0MsOEJBQU0sR0FBTjtRQUNJLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVELGdDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDcEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQVpRLGFBQWE7UUFMekIsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxTQUFTO1lBQ25CLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsdUJBQXVCO1NBQ3ZDLENBQUM7eUNBSWtDLHdCQUFVO09BSGpDLGFBQWEsQ0FjekI7SUFBRCxvQkFBQztDQUFBLEFBZEQsSUFjQztBQWRZLHNDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHtBcGlTZXJ2aWNlfSBmcm9tIFwifi9jb3JlL3NlcnZpY2VzL2FwaS5zZXJ2aWNlXCI7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiBcIm5zLXVzZXJcIixcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICAgIHRlbXBsYXRlVXJsOiBcIi4vdXNlci5jb21wb25lbnQuaHRtbFwiLFxufSlcbmV4cG9ydCBjbGFzcyBVc2VyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0e1xuICAgIGxvZ2dlZEluVXNlcjogYW55O1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhcGlTZXJ2aWNlOiBBcGlTZXJ2aWNlKSB7IH1cblxuICAgIGxvZ291dCgpe1xuICAgICAgICB0aGlzLmFwaVNlcnZpY2UubG9nb3V0KCk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKXtcbiAgICAgICAgdGhpcy5sb2dnZWRJblVzZXIgPSB0aGlzLmFwaVNlcnZpY2UuZ2V0TG9nZ2VkVXNlcigpO1xuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmxvZ2dlZEluVXNlcik7XG4gICAgfVxuXG59Il19