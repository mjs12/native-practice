import { Component, OnInit } from "@angular/core";
import {ApiService} from "~/core/services/api.service";

@Component({
    selector: "ns-user",
    moduleId: module.id,
    templateUrl: "./user.component.html",
})
export class UserComponent implements OnInit{
    loggedInUser: any;

    constructor(private apiService: ApiService) { }

    logout(){
        this.apiService.logout();
    }

    ngOnInit(){
        this.loggedInUser = this.apiService.getLoggedUser();
        console.log(this.loggedInUser);
    }

}