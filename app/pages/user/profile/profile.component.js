"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_service_1 = require("~/core/services/api.service");
var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(apiService) {
        this.apiService = apiService;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var loggedInUser = this.apiService.getLoggedUser();
        console.log(loggedInUser);
    };
    ProfileComponent = __decorate([
        core_1.Component({
            selector: "ns-profile",
            moduleId: module.id,
            templateUrl: "./profile.component.html",
        }),
        __metadata("design:paramtypes", [api_service_1.ApiService])
    ], ProfileComponent);
    return ProfileComponent;
}());
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZmlsZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJwcm9maWxlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCwyREFBdUQ7QUFPdkQ7SUFDSSwwQkFBb0IsVUFBc0I7UUFBdEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUFJLENBQUM7SUFFL0MsbUNBQVEsR0FBUjtRQUNJLElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQTtJQUM3QixDQUFDO0lBTlEsZ0JBQWdCO1FBTDVCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsWUFBWTtZQUN0QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLDBCQUEwQjtTQUMxQyxDQUFDO3lDQUVrQyx3QkFBVTtPQURqQyxnQkFBZ0IsQ0FPNUI7SUFBRCx1QkFBQztDQUFBLEFBUEQsSUFPQztBQVBZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7QXBpU2VydmljZX0gZnJvbSBcIn4vY29yZS9zZXJ2aWNlcy9hcGkuc2VydmljZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogXCJucy1wcm9maWxlXCIsXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgICB0ZW1wbGF0ZVVybDogXCIuL3Byb2ZpbGUuY29tcG9uZW50Lmh0bWxcIixcbn0pXG5leHBvcnQgY2xhc3MgUHJvZmlsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhcGlTZXJ2aWNlOiBBcGlTZXJ2aWNlKSB7IH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgICBjb25zdCBsb2dnZWRJblVzZXIgPSB0aGlzLmFwaVNlcnZpY2UuZ2V0TG9nZ2VkVXNlcigpO1xuICAgICAgICBjb25zb2xlLmxvZyhsb2dnZWRJblVzZXIpXG4gICAgfVxufSJdfQ==