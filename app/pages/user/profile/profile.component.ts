import { Component, OnInit } from "@angular/core";
import {ApiService} from "~/core/services/api.service";

@Component({
    selector: "ns-profile",
    moduleId: module.id,
    templateUrl: "./profile.component.html",
})
export class ProfileComponent implements OnInit {
    constructor(private apiService: ApiService) { }

    ngOnInit(): void {
        const loggedInUser = this.apiService.getLoggedUser();
        console.log(loggedInUser)
    }
}