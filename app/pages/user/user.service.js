"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var outlet_model_1 = require("../../core/models/outlet.model");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var UserService = /** @class */ (function () {
    function UserService() {
        // get total bet amount
        this.drawSubject = new BehaviorSubject_1.BehaviorSubject(outlet_model_1.Draw.DRAW1);
    }
    UserService.prototype.selectDraw = function (draw) { return this.drawSubject.next(draw); };
    UserService.prototype.getDefaultDraw = function () { return this.drawSubject.getValue(); };
    UserService.prototype.getDraw = function () { return this.drawSubject.asObservable(); };
    UserService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidXNlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLCtEQUFvRDtBQUNwRCx3REFBcUQ7QUFHckQ7SUFFSTtRQUNBLHVCQUF1QjtRQUNmLGdCQUFXLEdBQUcsSUFBSSxpQ0FBZSxDQUFNLG1CQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFGNUMsQ0FBQztJQUdoQixnQ0FBVSxHQUFWLFVBQVcsSUFBVSxJQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUM7SUFDNUQsb0NBQWMsR0FBZCxjQUFpQixNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFBLENBQUM7SUFDckQsNkJBQU8sR0FBUCxjQUFVLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUEsQ0FBQztJQVB6QyxXQUFXO1FBRHZCLGlCQUFVLEVBQUU7O09BQ0EsV0FBVyxDQVN2QjtJQUFELGtCQUFDO0NBQUEsQUFURCxJQVNDO0FBVFksa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0RyYXd9IGZyb20gXCIuLi8uLi9jb3JlL21vZGVscy9vdXRsZXQubW9kZWxcIjtcbmltcG9ydCB7QmVoYXZpb3JTdWJqZWN0fSBmcm9tIFwicnhqcy9CZWhhdmlvclN1YmplY3RcIjtcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFVzZXJTZXJ2aWNlIHtcblxuICAgIGNvbnN0cnVjdG9yKCl7IH1cbiAgICAvLyBnZXQgdG90YWwgYmV0IGFtb3VudFxuICAgIHByaXZhdGUgZHJhd1N1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGFueT4oRHJhdy5EUkFXMSk7XG4gICAgc2VsZWN0RHJhdyhkcmF3OiBEcmF3KSB7cmV0dXJuIHRoaXMuZHJhd1N1YmplY3QubmV4dChkcmF3KTt9XG4gICAgZ2V0RGVmYXVsdERyYXcoKXtyZXR1cm4gdGhpcy5kcmF3U3ViamVjdC5nZXRWYWx1ZSgpO31cbiAgICBnZXREcmF3KCl7cmV0dXJuIHRoaXMuZHJhd1N1YmplY3QuYXNPYnNlcnZhYmxlKCk7fVxuXG59XG4iXX0=