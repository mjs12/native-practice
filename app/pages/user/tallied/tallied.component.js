"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var TalliedComponent = /** @class */ (function () {
    function TalliedComponent() {
    }
    TalliedComponent.prototype.ngOnInit = function () { };
    TalliedComponent = __decorate([
        core_1.Component({
            selector: "ns-tallied",
            moduleId: module.id,
            templateUrl: "./tallied.component.html",
        }),
        __metadata("design:paramtypes", [])
    ], TalliedComponent);
    return TalliedComponent;
}());
exports.TalliedComponent = TalliedComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFsbGllZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0YWxsaWVkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQU9sRDtJQUNJO0lBQWdCLENBQUM7SUFFakIsbUNBQVEsR0FBUixjQUFrQixDQUFDO0lBSFYsZ0JBQWdCO1FBTDVCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsWUFBWTtZQUN0QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLDBCQUEwQjtTQUMxQyxDQUFDOztPQUNXLGdCQUFnQixDQUk1QjtJQUFELHVCQUFDO0NBQUEsQUFKRCxJQUlDO0FBSlksNENBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogXCJucy10YWxsaWVkXCIsXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgICB0ZW1wbGF0ZVVybDogXCIuL3RhbGxpZWQuY29tcG9uZW50Lmh0bWxcIixcbn0pXG5leHBvcnQgY2xhc3MgVGFsbGllZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgY29uc3RydWN0b3IoKSB7IH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge31cbn0iXX0=