"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("nativescript-angular/common");
var router_1 = require("nativescript-angular/router");
var common_2 = require("@angular/common");
var core_modue_1 = require("~/core/core.modue");
var user_component_1 = require("~/pages/user/user.component");
var tallied_component_1 = require("~/pages/user/tallied/tallied.component");
var summary_component_1 = require("~/pages/user/summary/summary.component");
var profile_component_1 = require("~/pages/user/profile/profile.component");
var api_service_1 = require("~/core/services/api.service");
var user_service_1 = require("~/pages/user/user.service");
var routes = [
    {
        path: '',
        component: user_component_1.UserComponent,
        children: [
            {
                path: 'transaction',
                component: tallied_component_1.TalliedComponent
            },
            {
                path: 'summary',
                component: summary_component_1.SummaryComponent
            },
            {
                path: 'profile',
                component: profile_component_1.ProfileComponent
            },
            {
                path: '**',
                redirectTo: 'transaction',
                pathMatch: 'full'
            }
        ]
    }
];
var UserModule = /** @class */ (function () {
    function UserModule() {
    }
    UserModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.NativeScriptCommonModule,
                router_1.NativeScriptRouterModule,
                router_1.NativeScriptRouterModule.forChild(routes),
                core_modue_1.CoreModule,
            ],
            providers: [
                api_service_1.ApiService,
                user_service_1.UserService,
                common_2.DatePipe
            ],
            declarations: [
                user_component_1.UserComponent,
                tallied_component_1.TalliedComponent,
                profile_component_1.ProfileComponent,
                summary_component_1.SummaryComponent
            ]
        })
    ], UserModule);
    return UserModule;
}());
exports.UserModule = UserModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ1c2VyLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF1QztBQUN2QyxzREFBcUU7QUFDckUsc0RBQXVFO0FBQ3ZFLDBDQUF5QztBQUN6QyxnREFBNkM7QUFFN0MsOERBQTBEO0FBQzFELDRFQUF3RTtBQUN4RSw0RUFBd0U7QUFDeEUsNEVBQXdFO0FBRXhFLDJEQUF1RDtBQUN2RCwwREFBc0Q7QUFFdEQsSUFBTSxNQUFNLEdBQVc7SUFDbkI7UUFDSSxJQUFJLEVBQUUsRUFBRTtRQUNSLFNBQVMsRUFBRSw4QkFBYTtRQUN4QixRQUFRLEVBQUU7WUFDTjtnQkFDSSxJQUFJLEVBQUUsYUFBYTtnQkFDbkIsU0FBUyxFQUFFLG9DQUFnQjthQUM5QjtZQUNEO2dCQUNJLElBQUksRUFBRSxTQUFTO2dCQUNmLFNBQVMsRUFBRSxvQ0FBZ0I7YUFDOUI7WUFDRDtnQkFDSSxJQUFJLEVBQUUsU0FBUztnQkFDZixTQUFTLEVBQUUsb0NBQWdCO2FBQzlCO1lBQ0Q7Z0JBQ0ksSUFBSSxFQUFFLElBQUk7Z0JBQ1YsVUFBVSxFQUFFLGFBQWE7Z0JBQ3pCLFNBQVMsRUFBRSxNQUFNO2FBQ3BCO1NBQ0o7S0FDSjtDQUNKLENBQUM7QUFxQkY7SUFBQTtJQUEwQixDQUFDO0lBQWQsVUFBVTtRQW5CdEIsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFO2dCQUNMLGlDQUF3QjtnQkFDeEIsaUNBQXdCO2dCQUN4QixpQ0FBd0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO2dCQUN6Qyx1QkFBVTthQUNiO1lBQ0QsU0FBUyxFQUFFO2dCQUNQLHdCQUFVO2dCQUNWLDBCQUFXO2dCQUNYLGlCQUFRO2FBQ1g7WUFDRCxZQUFZLEVBQUU7Z0JBQ1YsOEJBQWE7Z0JBQ2Isb0NBQWdCO2dCQUNoQixvQ0FBZ0I7Z0JBQ2hCLG9DQUFnQjthQUNuQjtTQUNKLENBQUM7T0FDVyxVQUFVLENBQUk7SUFBRCxpQkFBQztDQUFBLEFBQTNCLElBQTJCO0FBQWQsZ0NBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05nTW9kdWxlfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHtOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGV9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9jb21tb25cIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7RGF0ZVBpcGV9IGZyb20gXCJAYW5ndWxhci9jb21tb25cIjtcbmltcG9ydCB7Q29yZU1vZHVsZX0gZnJvbSBcIn4vY29yZS9jb3JlLm1vZHVlXCI7XG5pbXBvcnQge1JvdXRlc30gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHtVc2VyQ29tcG9uZW50fSBmcm9tIFwifi9wYWdlcy91c2VyL3VzZXIuY29tcG9uZW50XCI7XG5pbXBvcnQge1RhbGxpZWRDb21wb25lbnR9IGZyb20gXCJ+L3BhZ2VzL3VzZXIvdGFsbGllZC90YWxsaWVkLmNvbXBvbmVudFwiO1xuaW1wb3J0IHtTdW1tYXJ5Q29tcG9uZW50fSBmcm9tIFwifi9wYWdlcy91c2VyL3N1bW1hcnkvc3VtbWFyeS5jb21wb25lbnRcIjtcbmltcG9ydCB7UHJvZmlsZUNvbXBvbmVudH0gZnJvbSBcIn4vcGFnZXMvdXNlci9wcm9maWxlL3Byb2ZpbGUuY29tcG9uZW50XCI7XG5cbmltcG9ydCB7QXBpU2VydmljZX0gZnJvbSBcIn4vY29yZS9zZXJ2aWNlcy9hcGkuc2VydmljZVwiO1xuaW1wb3J0IHtVc2VyU2VydmljZX0gZnJvbSBcIn4vcGFnZXMvdXNlci91c2VyLnNlcnZpY2VcIjtcblxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXG4gICAge1xuICAgICAgICBwYXRoOiAnJyxcbiAgICAgICAgY29tcG9uZW50OiBVc2VyQ29tcG9uZW50LFxuICAgICAgICBjaGlsZHJlbjogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHBhdGg6ICd0cmFuc2FjdGlvbicsXG4gICAgICAgICAgICAgICAgY29tcG9uZW50OiBUYWxsaWVkQ29tcG9uZW50XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHBhdGg6ICdzdW1tYXJ5JyxcbiAgICAgICAgICAgICAgICBjb21wb25lbnQ6IFN1bW1hcnlDb21wb25lbnRcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgcGF0aDogJ3Byb2ZpbGUnLFxuICAgICAgICAgICAgICAgIGNvbXBvbmVudDogUHJvZmlsZUNvbXBvbmVudFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBwYXRoOiAnKionLFxuICAgICAgICAgICAgICAgIHJlZGlyZWN0VG86ICd0cmFuc2FjdGlvbicsXG4gICAgICAgICAgICAgICAgcGF0aE1hdGNoOiAnZnVsbCdcbiAgICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgIH1cbl07XG5cbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW1xuICAgICAgICBOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGUsXG4gICAgICAgIE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSxcbiAgICAgICAgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlLmZvckNoaWxkKHJvdXRlcyksXG4gICAgICAgIENvcmVNb2R1bGUsXG4gICAgXSxcbiAgICBwcm92aWRlcnM6IFtcbiAgICAgICAgQXBpU2VydmljZSxcbiAgICAgICAgVXNlclNlcnZpY2UsXG4gICAgICAgIERhdGVQaXBlXG4gICAgXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgVXNlckNvbXBvbmVudCxcbiAgICAgICAgVGFsbGllZENvbXBvbmVudCxcbiAgICAgICAgUHJvZmlsZUNvbXBvbmVudCxcbiAgICAgICAgU3VtbWFyeUNvbXBvbmVudFxuICAgIF1cbn0pXG5leHBvcnQgY2xhc3MgVXNlck1vZHVsZSB7IH0iXX0=