import { Injectable } from '@angular/core';
import {Draw} from "../../core/models/outlet.model";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class UserService {

    constructor(){ }
    // get total bet amount
    private drawSubject = new BehaviorSubject<any>(Draw.DRAW1);
    selectDraw(draw: Draw) {return this.drawSubject.next(draw);}
    getDefaultDraw(){return this.drawSubject.getValue();}
    getDraw(){return this.drawSubject.asObservable();}

}
