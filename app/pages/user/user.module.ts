import {NgModule} from "@angular/core";
import {NativeScriptCommonModule} from "nativescript-angular/common";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import {DatePipe} from "@angular/common";
import {CoreModule} from "~/core/core.modue";
import {Routes} from "@angular/router";
import {UserComponent} from "~/pages/user/user.component";
import {TalliedComponent} from "~/pages/user/tallied/tallied.component";
import {SummaryComponent} from "~/pages/user/summary/summary.component";
import {ProfileComponent} from "~/pages/user/profile/profile.component";

import {ApiService} from "~/core/services/api.service";
import {UserService} from "~/pages/user/user.service";

const routes: Routes = [
    {
        path: '',
        component: UserComponent,
        children: [
            {
                path: 'transaction',
                component: TalliedComponent
            },
            {
                path: 'summary',
                component: SummaryComponent
            },
            {
                path: 'profile',
                component: ProfileComponent
            },
            {
                path: '**',
                redirectTo: 'transaction',
                pathMatch: 'full'
            }
        ]
    }
];

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild(routes),
        CoreModule,
    ],
    providers: [
        ApiService,
        UserService,
        DatePipe
    ],
    declarations: [
        UserComponent,
        TalliedComponent,
        ProfileComponent,
        SummaryComponent
    ]
})
export class UserModule { }