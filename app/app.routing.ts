import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import {Routes} from "@angular/router";

import { ItemsComponent } from "./item/items.component";
import { ItemDetailComponent } from "./item/item-detail.component";
import {LoginComponent} from "~/pages/login/login.component";
import {AuthGuard} from "~/core/services/auth.guard";

const routes: Routes = [
    { path: "", redirectTo: "/login", pathMatch: "full" },
    { path: "items", component: ItemsComponent },
    { path: "item/:id", component: ItemDetailComponent },
    { path: "login", component: LoginComponent },
    {
        path: "user",
        loadChildren: './pages/user/user.module#UserModule',
        data: {userTypes: ['outlet']},
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule],
    providers: [AuthGuard]
})
export class AppRoutingModule { }